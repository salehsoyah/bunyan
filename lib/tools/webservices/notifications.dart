import 'package:bunyan/models/notification.dart';
import 'package:bunyan/tools/res.dart';
import 'package:bunyan/tools/webservices/base.dart';

class NotificationsWebService extends BaseWebService {

    Future<List<NotificationModel>> getNotifications() async {
      final response = (await dio.get('notifications/?id=${Res.USER.id}')).data;
      List<NotificationModel> notifications = [];

      for (final notif in List.of(response))
        notifications.add(NotificationModel.fromJson(notif));
      return notifications;
    }
}