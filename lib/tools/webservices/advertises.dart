import 'dart:convert';

import 'package:bunyan/models/advertise.dart';
import 'package:bunyan/models/banner.dart';
import 'package:bunyan/tools/webservices/base.dart';
import 'package:dio/dio.dart';

class AdvertisesWebService extends BaseWebService {
  Future<List<Advertise>> getAds() async {
    /*try {*/
    print('calledd');
    final response =
        jsonDecode((await dio.get('application/bannier.php')).data);
    print('fetchedd');
    List<Advertise> ads = List();
    List.of(response).forEach((ad) {
      ads.add(Advertise.fromJson(ad));
    });
    return ads;
    /*}  on DioError catch(e) {
      if (e.response != null)
        print(e.response.statusCode);

      print('dio error:   $e');
      return null;
    }*/
  }

  Future<List<BannerModel>> getBanners() async {
    try {
      final response = (await dio.get('banners')).data;
      List<BannerModel> banners = List();

      List.of(response).forEach((banner) {
        banners.add(BannerModel.fromJson(banner));
      });

      return banners;
    } on DioError catch (e) {
      if (e.response != null) print(e.response.statusCode);

      print('dio error:   $e');
      return null;
    }
  }
}
