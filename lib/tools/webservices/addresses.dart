import 'package:bunyan/models/city.dart';
import 'package:bunyan/models/region.dart';
import 'package:bunyan/tools/webservices/base.dart';

class AddressesWebService extends BaseWebService  {


  Future<List<RegionModel>> getRegions() async {
    final response = (await dio.get('/regions')).data;
    return List.of(response).map((e) => RegionModel.fromJson(e)).toList();
  }
  
  Future<List<CityModel>> getCities(int regionId) async {
    final response = (await dio.get('/cities/$regionId')).data;
    return List.of(response).map((e) => CityModel.fromJson(e)).toList();
  }
}