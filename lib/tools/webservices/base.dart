import 'package:bunyan/tools/res.dart';
import 'package:dio/dio.dart';

class BaseWebService {
  Dio dio;

  BaseWebService() {
    dio = Dio(BaseOptions(baseUrl: Res.baseUrl, headers: {'accept': 'application/json'},));
  }
}