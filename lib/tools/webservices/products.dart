import 'dart:async';
import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:bunyan/models/category.dart';
import 'package:bunyan/models/enterprise.dart';
import 'package:bunyan/models/favorite.dart';
import 'package:bunyan/models/news.dart';
import 'package:bunyan/models/product.dart';
import 'package:bunyan/models/product_list.dart';
import 'package:bunyan/models/real_estate.dart';
import 'package:bunyan/models/real_estate_filter.dart';
import 'package:bunyan/models/real_estate_type.dart';
import 'package:bunyan/models/service.dart';
import 'package:bunyan/models/services_filter.dart';
import 'package:bunyan/tools/res.dart';
import 'package:bunyan/tools/webservices/base.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class ProductsWebService extends BaseWebService {
  static const REAL_ESTATE = 1;
  static const SERVICE = 2;

  Future<List<ProductModel>> getHomeProducts(
      {RealEstateFilterModel filter}) async {
    Map<String, dynamic> data = filter == null ? {} : filter.toRequest();
    data['id_session'] = Res.USER != null ? Res.USER.id : null;
    //data['id_session'] = Res.USER.id;

    final response = (await dio.get('filter_ads', queryParameters: data));

    List<ProductModel> products = [];
    List.of(response.data).forEach((product) {
      products.add(ProductModel.fromJson(product));
    });
    return products;
  }

  Future<List<ProductModel>> getTop10() async {
    var idu = Res.USER != null ? Res.USER.id : null;
    final response = (await dio.get('top_10_paid_ads/${idu}'));
    List<ProductModel> products = [];
    List.of(response.data).forEach((product) {
      products.add(ProductModel.fromJson(product));
    });
    return products;
  }

  Future<bool> reportProduct({int id, int type}) async {
    var idu = Res.USER != null ? Res.USER.id : null;
    final response = (await dio.get('add_report/${idu}/$id/$type')).data;
    return response['return'];
  }

  Future<List<CategoryModel>> getCategories(String lang) async {
    try {
      final response = (await dio.get('categories?lng_type=$lang')).data;
      List<CategoryModel> categories = [];
      List.of(response).forEach((category) {
        categories.add(CategoryModel.fromJson(category));
      });
      return categories;
    } on DioError catch (e) {
      if (e.response != null) print(e.response.statusCode);

      print('dio error:   $e');
      return null;
    }
  }

  Future<List<CategoryModel>> getServicesCategories(String lang) async {
    final response = (await dio.get('services_categories?lng_type=$lang')).data;
    return List.of(response).map((e) => CategoryModel.fromJson(e)).toList();
  }

  Future<List<NewsModel>> getNews() async {
    final response = (await dio.get('news')).data;
    List<NewsModel> news = [];
    List.of(response).forEach((element) {
      news.add(NewsModel.fromJson(element));
    });
    return news;
  }

  Future<List<ServiceModel>> getServices({ServicesFilterModel filter}) async {
    //get final user id
    var idu = Res.USER != null ? Res.USER.id : null;
    //response code
    //$idu
    final response =
        (await dio.get('services/', queryParameters: filter.toRequest())).data;
    List<ServiceModel> services = [];
    for (final service in List.of(response)) {
      if (service['status'] == '1')
        services.add(ServiceModel.fromJson(service));
    }
    return services;
  }

  Future<List<ServiceModel>> get({ServicesFilterModel filter}) async {
    //get final user id
    var idu = Res.USER != null ? Res.USER.id : null;
    //response code
    //$idu
    final response =
        (await dio.get('services/', queryParameters: filter.toRequest())).data;
    List<ServiceModel> services = [];
    for (final service in List.of(response)) {
      if (service['status'] == '1')
        services.add(ServiceModel.fromJson(service));
    }
    return services;
  }

  Future<List<EnterpriseModel>> getEnterprises() async {
    final response = (await dio.get('entreprises')).data;
    List<EnterpriseModel> enterprises = [];
    log(response[0].toString());
    for (final enterprise in List.of(response))
      if (enterprise != null)
        enterprises.add(EnterpriseModel.fromJson(enterprise));
    return enterprises;
  }

  Future<bool> likeProd(
      {@required int id,
      @required bool isRealEstate,
      bool dislike = false}) async {
    final response = (await dio.get('${dislike ? 'dis' : ''}like_ads/$id',
            queryParameters: {'type': isRealEstate ? 1 : 2}))
        .data;
    return response['success'];
  }

  Future<void> setFavorit(
      {@required int id, @required bool isRealestate, bool set = true}) async {
    final response = (await dio.post('${!set ? 'un' : ''}set_favorit',
            data: {
              'type': isRealestate ? 'advertisement' : 'service',
              'id_session': Res.USER.id,
              'id_ad': id
            },
            options: Options(contentType: Headers.formUrlEncodedContentType)))
        .data;
    print(response);
  }

  Future<List<RealEstateTypeModel>> getRealEstateTypes() async {
    // /types
    final response = (await dio.get('types')).data;
    return List.of(response)
        .map((e) => RealEstateTypeModel.fromJson(e))
        .toList();
  }

  Future<bool> addProduct(dynamic product, {bool isRealEstate = true}) async {
    List<String> photosPath = List.of(product.photos);
    print(product.toRequest());
    List<MultipartFile> photos = [];
    product.photos.clear();
    for (final photo in photosPath) {
      final img = await MultipartFile.fromFile(photo);
      photos.add(img);
    }

    Map<String, dynamic> map = product.toRequest();
    map['photos[]'] = photos;

    final data = FormData.fromMap(map);
    print(map);
    final response = (await dio.post(
            'add_${isRealEstate ? 'advertisement' : 'service'}',
            data: data))
        .data;
    print(response);
    return Map.of(response).containsKey('id');
  }

  Future<List<ProductModel>> getEnterpriseProducts(
      {RealEstateFilterModel filter, int enterpriseId}) async {
    Map<String, dynamic> data = filter == null ? {} : filter.toRequest();
    data['id_session'] = Res.USER != null ? Res.USER.id : null;
    data['company_id'] = enterpriseId;
    //data['id_session'] = Res.USER.id;

    final response = (await dio.get(
        'http://beta-website.bunyan.qa/Users/entreprise_ads',
        queryParameters: data));
    List<ProductModel> products = [];
    print("dzdzdz" + response.data.toString());
    List.of(response.data).forEach((product) {
      products.add(ProductModel.fromJson(product));
    });
    print(products.length.toString() + "ddddddddd");
    return products;
  }
}
