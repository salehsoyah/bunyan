import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:bunyan/exceptions/signup_failed.dart';
import 'package:bunyan/exceptions/unauthorized.dart';
import 'package:bunyan/models/auth.dart';
import 'package:bunyan/models/chat.dart';
import 'package:bunyan/models/enterprise.dart';
import 'package:bunyan/models/favorite.dart';
import 'package:bunyan/models/person.dart';
import 'package:bunyan/models/product.dart';
import 'package:bunyan/models/service.dart';
import 'package:bunyan/tools/res.dart';
import 'package:bunyan/tools/webservices/base.dart';
import 'package:dio/dio.dart';
import 'package:bunyan/tools/extensions.dart';

class UsersWebService extends BaseWebService {
  Future<PersonModel> login({String mail, String passwd}) async {
    try {
      final dio2 = Dio();
      final data = {'email': mail, 'password': passwd};
      final response = (await dio2.post('http://beta-website.bunyan.qa/users/login',
              data: FormData.fromMap(data)))
          .data;
      if (response == null) throw UnauthorizedException();
      return PersonModel.fromJson(response);
    } on DioError catch (e) {
      print(e);
      throw e;
    }
  }

  Future<PersonModel> signup(AuthModel usr) async {
    Map<String, dynamic> map = usr.toRequest();

    if (map.get('profile_photo') != null) {
      final profilePicture = File(usr.profilePicture.path);
      map['profile_photo'] = await MultipartFile.fromFile(profilePicture.path);
    }
    if (map.get('entreprize_photo') != null) {
      final entrPicture = File(usr.entrPhoto.path);
      map['entreprize_photo'] = await MultipartFile.fromFile(entrPicture.path);
    }
    if (map.get('entreprize_lat') == null) map['entreprize_lat'] = '';
    if (map.get('entreprize_lng') == null) map['entreprize_lng'] = '';
    print('map $map');
    FormData data = FormData.fromMap(map);

    final dio2 = Dio();
    var json;
    final response = (await dio2.post(
      'http://beta-website.bunyan.qa/users/signup',
      data: data,
    ))
        .data;

    try {
      json = jsonDecode(jsonEncode(response));
      print('json $json');
      return PersonModel.fromJson(json['session']);
    } catch (e) {
      if (response.toString().contains('already exists')) {
        throw SignupFailedException(cause: response);
      } else
        throw e;
    }
  }

  Future<void> followUser({int id, bool follow = true}) async {
    final dio2 = Dio();
    final response = (await dio2.post(
            'http://beta-website.bunyan.qa/users/${follow ? '' : 'un'}follow',
            data: {'followed_id': id, 'follower_id': Res.USER.id},
            options: Options(contentType: Headers.formUrlEncodedContentType)))
        .data;
    return response['message'].toString().contains('query saved');
  }

  Future<Map<String, dynamic>> getUserProfile(int id) async {
    final dio2 = Dio();
    final params = {
      'selected_profile': id ?? Res.USER.id,
      'id_session': Res.USER.id,
    };

    print(params);

    final response = (await dio2.get('http://beta-website.bunyan.qa/Users/profile',
            queryParameters: params))
        .data;
    Map<String, dynamic> data = {};
    List<ProductModel> products = [];
    List<ServiceModel> services = [];
    if (response['ads'] != null)
      List.of(response['ads'][0]).forEach((element) {
        products.add(ProductModel.fromJson(element));
      });
    else
      products = [];
    // log("ddfff" + response.toString());
    // List.of(response['services'][0]).forEach((element) {
    //   services.add(ServiceModel.fromJson(element));
    // });

    print(response);

    data['user'] = PersonModel.fromJson(response['profile']);
    data['products'] = products;
    data['services'] = services;
    return data;
  }

  Future<List<ChatModel>> getChats() async {
    final dio2 = Dio();
    final response =
        (await dio2.get('http://beta-website.bunyan.qa/Users/chat/${Res.USER.id}')).data;
    List<ChatModel> chats = [];
    List.of(response).forEach((element) {
      chats.add(ChatModel.fromJson(element));
    });
    return chats;
  }

  Future<List<FavoriteModel>> getFav() async {
    final dio2 = Dio();
    final response =
        (await dio2.get('http://beta-website.bunyan.qa/Users/favs/${Res.USER.id}')).data;
    List<FavoriteModel> favs = [];
    List.of(response).forEach((element) {
      favs.add(FavoriteModel.fromJson(element));
    });
    return favs;
  }

  Future<List<ChatModel>> getConversation(ChatModel chat) async {
    final dio2 = Dio();
    final response = (await dio2.get(
            'http://beta-website.bunyan.qa/Users/chat_message/${chat.sender.id}/${chat.receiver.id}'))
        .data;
    List<ChatModel> chats = [];

    List.of(response).forEach((element) {
      chats.add(ChatModel.fromJson(element));
    });

    return chats;
  }

  Future<EnterpriseModel> checkEnterprise(String crNumber) async {
    final dio2 = Dio();
    final response =
        (await dio2.get('http://beta-website.bunyan.qa/Api/entreprise_id/$crNumber'))
            .data[0];

    return response == null ? null : EnterpriseModel.fromJson(response);
  }

  Future<bool> sendMessage({String text, int receiver}) async {
    final dio2 = Dio();

    final data = {
      'sender': Res.USER.id,
      'receiver': receiver,
      'content': Uri.encodeFull(text),
    };

    final response = (await dio2.get('http://beta-website.bunyan.qa/Users/add_message',
            queryParameters: data))
        .data;
    return response['return'];
  }

  Future<bool> followEnterprise({int id, bool follow = true}) async {
    final dio2 = Dio();
    final response = (await dio2.get(
            'http://beta-website.bunyan.qa/Users/${follow ? '' : 'un'}follow_entreprise/${Res.USER.id}/$id'))
        .data;
    return response['return'];
  }

  Future updatePhotoProfile({int idsession, File profilePhoto}) async {
    final dio2 = Dio();
    // var photo = await MultipartFile.fromFile(profilePhoto);
    // final response = (await dio2.post(
    //         'https://bunyan.qa/Users/update_profile_photo',
    //         data: {'profile_photo': photo, 'idsession': idsession}))
    //     .data;
    //
    // return response['return'];

    FormData formData = FormData.fromMap({
      "profile_photo": await MultipartFile.fromFile(profilePhoto.path),
      'idsession': idsession
    });
    var response = await dio2
        .post("http://beta-website.bunyan.qa/Users/update_profile_photo", data: formData);
    print('response $response');
    return response.data;
  }

  Future<bool> forgetPasswd(String email) async {
    final data = FormData.fromMap({'email': email});
    final dio2 = Dio();
    final response =
        (await dio2.post('http://beta-website.bunyan.qa/Users/forgot_password', data: data))
            .data;
    print(response);
    return response['status'];
  }

  Future<bool> checkForgetPasswdCode({String mail, String code}) async {
    final dio2 = Dio();
    final data = FormData.fromMap({'email': mail, 'code': code});
    final response = (await dio2
            .post('http://beta-website.bunyan.qa/Users/find_current_key_code', data: data))
        .data;
    return response['query'];
  }

  Future<bool> updateForgetPasswdCode(
      {String mail, String code, String passwd}) async {
    final dio2 = Dio();
    final data =
        FormData.fromMap({'email': mail, 'code': code, 'password': passwd});
    final response = (await dio2
            .post('http://beta-website.bunyan.qa/Users/update_new_password', data: data))
        .data;
    return response['status'];
  }
}
