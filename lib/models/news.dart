import 'package:bunyan/tools/extensions.dart';

class NewsModel {
  int id;
  String title;
  String photo;
  String content;
  String url;

  NewsModel({this.id, this.title, this.photo, this.content, this.url});

  factory NewsModel.fromJson(Map<String, dynamic> json) {
    return NewsModel(
      id: int.parse(json.get("id")),
      title: json.get("title"),
      content: json.get("contents"),
      url: json.get("url"),
      photo: json.get('photo_url')
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "title": this.title,
      "photo_url": this.photo,
      "contents": this.content,
      "url": this.url,
    };
  }
}