import 'package:bunyan/models/category.dart';
import 'package:bunyan/models/real_estate_type.dart';
import 'package:bunyan/tools/res.dart';
import 'package:equatable/equatable.dart';
import 'package:bunyan/tools/extensions.dart';

class ServicesFilterModel {
  int page;
  CategoryModel category;
  double minPrice;
  double maxPrice;
  String query;

  ServicesFilterModel(
      {this.page,
      this.category,
      this.minPrice,
      this.maxPrice,
      this.query});

  factory ServicesFilterModel.fromJson(Map<String, dynamic> json) {
    return ServicesFilterModel(
      page: json.get("page") == null
          ? null
          : int.parse(json.get("page").toString()),
      category: json.get("category") == null
          ? null
          : CategoryModel.fromJson(json.get("category")),
      minPrice: json.get("minPrice") == null
          ? null
          : double.parse(json.get("minPrice").toString()),
      maxPrice: json.get("maxPrice") == null
          ? null
          : double.parse(json.get("maxPrice").toString()),
      query: json.get('query') == null ? null : json.get('query'),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "page": this.page,
      "category": this.category?.toJson(),
      "minPrice": this.minPrice,
      "maxPrice": this.maxPrice,
      'query': this.query,
    };
  }

  Map<String, dynamic> _toRequest() {
    return {
      "page": this.page?.toString(),
      "category_id": this.category?.id,
      "min_price": this.minPrice?.toString(),
      "max_price": this.maxPrice?.toString(),
      "query" :this.query,
      "userid": Res.USER.id
    };
  }

  Map<String, dynamic> toRequest() {
    Map<String, dynamic> request = this._toRequest();
    List<String> keys = [];
    request.keys.forEach((key) {
      if (request[key] != null || request[key].toString().isEmpty)
        keys.add(key);
    });
    Map<String, dynamic> response = Map();
    keys.forEach((key) {
      response[key] = request[key];
    });
    return response.isNotEmpty ? response : null;
  }

//

}
