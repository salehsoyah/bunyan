import 'package:bunyan/models/enterprise.dart';
import 'package:bunyan/tools/extensions.dart';
import 'package:equatable/equatable.dart';

class PersonModel extends Equatable {
    int id;
    String name;
    String email;
    int followers;
    bool isFollowing;
    String phone;
    String photo;
    EnterpriseModel enterprise;

    PersonModel({this.id, this.name, this.email, this.followers, this.isFollowing, this.phone, this.photo, this.enterprise});

    factory PersonModel.fromJson(Map<String, dynamic> json) {
    return PersonModel(
      id: int.tryParse(json.get("id").toString()),
      name: json.get("name"),
      email: json.get("email"),
      followers: json.get("followers_number"),
      isFollowing: json.get("is_following"),
      phone: json.get('phone'),
      photo: json.get('photo'),
      enterprise: json.get('entreprise') != null ? EnterpriseModel.fromJson(json['entreprise']) : null,
    );
  }

    Map<String, dynamic> toJson() {
      return {
        "id": this.id.toString(),
        "name": this.name,
        "email": this.email,
        "followers": this.followers,
        "isFollowing": this.isFollowing,
        "phone": this.phone,
        'photo': this.photo,
        "entreprise": this.enterprise == null ? null : this.enterprise.toJson(),
      };
    }

  @override
  // TODO: implement props
  List<Object> get props => [id];

}