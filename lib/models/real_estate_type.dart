import 'package:equatable/equatable.dart';

class RealEstateTypeModel extends Equatable {

  int id;
  String name;


  RealEstateTypeModel({this.id, this.name});


  factory RealEstateTypeModel.fromJson(Map<String, dynamic> json) {
    return RealEstateTypeModel(
      id: int.parse(json["id"].toString()),
      name: json["name"],
    );
  }


  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "name": this.name,
    };
  }

  @override
  // TODO: implement props
  List<Object> get props => [id];



}