import 'package:bunyan/tools/extensions.dart';

class BannerModel {
  int id;
  String photo;

  BannerModel({this.id, this.photo});

  factory BannerModel.fromJson(Map<String, dynamic> json) {
    return BannerModel(
      id: int.parse(json.get("id").toString()),
      photo: json.get("url"),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "url": this.photo,
    };
  }
}