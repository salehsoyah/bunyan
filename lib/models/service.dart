import 'dart:convert';

import 'package:bunyan/models/category.dart';
import 'package:bunyan/models/city.dart';
import 'package:bunyan/models/enterprise.dart';
import 'package:bunyan/models/person.dart';
import 'package:bunyan/models/position.dart';
import 'package:bunyan/models/product_list.dart';
import 'package:bunyan/models/region.dart';
import 'package:bunyan/tools/extensions.dart';
import 'package:bunyan/tools/res.dart';

class ServiceModel extends ProductListModel {
  int id;
  String title;
  List<dynamic> photos;
  double price;
  String description;
  PersonModel owner;
  EnterpriseModel enterprise;
  DateTime createdAt;
  bool favorite;
  bool liked;
  PositionModel position;
  CategoryModel category;
  CityModel city;
  int likes;
  RegionModel region;
  dynamic adr;

  ServiceModel(
      {this.id,
      this.title,
      this.photos,
      this.price,
      this.description,
      this.owner,
      this.enterprise,
      this.createdAt,
      this.favorite,
      this.position,
      this.category,
      this.city,
      this.likes,
      this.region,
      this.liked,
      this.adr});

  factory ServiceModel.fromJson(Map<String, dynamic> json) {
    return ServiceModel(
      id: int.parse(json["id"]),
      title: json["title"],
      photos: json["photos"] is String ? [json["photos"]] : json["photos"],
      price: double.parse(json["price"]),
      description: json["description"],
      owner: PersonModel.fromJson(json["owner"]),
      enterprise: json.get('enterprise') != null
          ? EnterpriseModel.fromJson(json["enterprise"])
          : PersonModel.fromJson(json.get('owner')).enterprise != null
              ? PersonModel.fromJson(json.get('owner')).enterprise
              : null,
      createdAt: json.get('add_date') != null
          ? DateTime.parse(json.get('add_date'))
          : null,
      favorite: json.get('is_favorit') ?? false,
      liked: json.get('is_liked')??false,
      likes: int.tryParse(json.get('likes').toString()),
      region: json.get('region') != null
          ? Res.regions
              .where((element) => element.id == json['region'].id)
              .first
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "title": this.title,
      "photos": jsonEncode(this.photos),
      "price": this.price,
      "description": this.description,
      "owner": this.owner,
      "enterprise": this.enterprise,
      'add_date': this.createdAt.toIso8601String(),
      'is_favorit': this.favorite,
      'position': this.position?.toJson(),
      'likes': this.likes
    };
  }

  Map<String, dynamic> toRequest() {
    return {
      "name": this.title,
      'category_id': this.category.id,
      "photos": this.photos,
      "price": this.price,
      'payement_type': null,
      "description": this.description,
      "userid": Res.USER.id,
      'address': this.adr,
      'city': this.city?.id,
      'region': this.region.id,
      'language': this,
      'phone': Res.USER.phone,
      'lat': this.position?.lat,
      'lng': this.position?.lng,
    };
  }
}
