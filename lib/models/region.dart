import 'package:bunyan/models/city.dart';
import 'package:equatable/equatable.dart';
import 'package:bunyan/tools/extensions.dart';

class RegionModel extends Equatable {
  int id;
  String name;
  String arabicName;
  List<CityModel> cities;

  RegionModel({this.id, this.name, this.arabicName, this.cities});

  factory RegionModel.fromJson(Map<String, dynamic> json) {
    return RegionModel(
      id: int.parse(json["id"].toString()),
      name: json["name"],
      arabicName: json["arabic_name"],
      cities: json.get('cities') != null
          ? List.of(json['cities'])
              .map((e) => CityModel.fromJson(json['cities'])).toList()
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "name": this.name,
      "arabic_name": this.arabicName,
      'cities': this.cities != null ? cities.map((e) => e.toJson()).toList() : null,
    };
  }

  @override
  // TODO: implement props
  List<Object> get props => [id];
}
