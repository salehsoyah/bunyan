import 'package:equatable/equatable.dart';
import 'package:bunyan/tools/extensions.dart';

class EnterpriseModel extends Equatable {

  int id;
  String name;
  String photo;
  String crNumber;
  String email;
  String description;
  String phone;
  int followers;
  bool isFollowing;
  double lat;
  double lng;



  EnterpriseModel({this.id, this.name, this.photo, this.crNumber, this.email,
      this.description, this.phone, this.followers, this.isFollowing, this.lat, this.lng});


  factory EnterpriseModel.fromJson(Map<String, dynamic> json) {
    return json == null ? null : EnterpriseModel(
      id: int.parse(json["id"].toString()),
      name: json["name"],
      photo: json["photo"],
      crNumber: json["cr_number"],
      email: json["email"],
      description: json["description"],
      phone: json.get('phone'),
      followers: json.get("followers")??0,
      isFollowing: json.get('is_following')??false,
      lat: json['lat'] != null ? double.parse(json['lat']) : 0,
      lng: json['lng'] != null ? double.parse(json['lng']) : 0,

    );
  }


  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "name": this.name,
      "photo": this.photo,
      "crNumber": this.crNumber,
      "email": this.email,
      "description": this.description,
      'phone': this.phone,
      'followers': this.followers,
      'is_following': this.isFollowing
    };
  }

  @override
  List<Object> get props => [id];

}