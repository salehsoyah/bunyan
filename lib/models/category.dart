import 'package:bunyan/tools/extensions.dart';

class CategoryModel {
  String id;
  String name;
  String arabicName;
  String photo;

  CategoryModel({this.id, this.name, this.photo, this.arabicName});

  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "name": this.name,
      "photo": this.photo,
      'arabic_name': this.arabicName,
    };
  }

  factory CategoryModel.fromJson(Map<String, dynamic> json) {
    return CategoryModel(
      id: json.get("id"),
      name: json.get("name"),
      photo: json.get("photo"),
      arabicName: json.get('arabic_name'),
    );
  }

}