import 'dart:convert';

import 'package:equatable/equatable.dart';

class FavoriteModel extends Equatable {
  int id;
  String name;
  String reference;
  List<String> photos;

  FavoriteModel({this.id, this.name, this.reference, this.photos});

  factory FavoriteModel.fromJson(Map<String, dynamic> json) {
    return FavoriteModel(
      id: int.parse(json["id"]),
      name: json["name"],
      reference: json["reference"],
      photos: List<String>.from(json["photos"]).map<String>((i) => i).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "name": this.name,
      "reference": this.reference,
      "photos": jsonEncode(this.photos),
    };
  }

  @override
  // TODO: implement props
  List<Object> get props => [id];



//

}