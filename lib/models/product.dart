import 'dart:convert';

import 'package:bunyan/models/address.dart';
import 'package:bunyan/models/category.dart';
import 'package:bunyan/models/city.dart';
import 'package:bunyan/models/enterprise.dart';
import 'package:bunyan/models/person.dart';
import 'package:bunyan/models/position.dart';
import 'package:bunyan/models/product_list.dart';
import 'package:bunyan/models/real_estate_type.dart';
import 'package:bunyan/models/region.dart';
import 'package:bunyan/tools/extensions.dart';
import 'package:bunyan/tools/res.dart';
import 'package:dio/dio.dart';

class ProductModel extends ProductListModel {
  int id;
    String title;
  double price;
  String currency;
  String description;
  bool forSell;
  bool furnished;
  String reference;
  DateTime createdAt;
  List<dynamic> photos;
  CategoryModel category;
  EnterpriseModel enterprise;
  PersonModel owner;
  dynamic adr;
  bool favorite;
  int bathrooms;
  String landSize;
  int rooms;
  bool isLiked;
  PositionModel position;
  RegionModel region;
  CityModel city;
  bool swimmingPool;
  RealEstateTypeModel type;
  String views;

  ProductModel(
      {this.id,
      this.title,
      this.price,
      this.currency,
      this.description,
      this.forSell,
      this.reference,
      this.createdAt,
      this.photos,
      this.category,
      this.owner,
      this.adr,
      this.furnished,
      this.enterprise,
      this.favorite,
      this.bathrooms,
      this.landSize,
      this.rooms,
      this.isLiked,
      this.position,
      this.region,
      this.city,
      this.swimmingPool,
      this.type,
      this.views});

  factory ProductModel.fromJson(Map<String, dynamic> json) {
    return ProductModel(
      id: int.parse(json.get("id")),
      title: json.get("name"),
      price: double.parse(json.get("price")),
      currency: json.get("currency"),
      description: json.get("description"),
      forSell: json.get("for_sell"),
      reference: json.get("reference"),
      createdAt: DateTime.parse(json.get("created_at")),
      photos: List.of(json.get("photos")).map<String>((i) => i).toList(),
      category: CategoryModel.fromJson(json.get("category_id")),
      owner: json.get('owner') == null
          ? null
          : PersonModel.fromJson(json.get("owner")),
      adr: AddressModel.fromJson(json.get("adresse")),
      furnished: int.parse((json.get('furnished') ?? 0).toString()) != 0,
      enterprise: json.get('entreprise') != null
          ? EnterpriseModel.fromJson(json['entreprise'])
          : PersonModel.fromJson(json.get("owner")).enterprise != null
              ? PersonModel.fromJson(json.get("owner")).enterprise
              : null,
      favorite: json['is_favorit'],
      bathrooms: int.parse(json.get('bathrooms') ?? 0.toString()),
      landSize: json.get('land'),
      rooms: int.tryParse(json.get('rooms').toString()) ?? 0,
      isLiked: json.get('is_liked') ?? false,
      position: PositionModel.fromJson(json.get('position')),
      views: json.get("views"),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "name": this.title,
      "price": this.price,
      "currency": this.currency,
      "description": this.description,
      "for_sell": this.forSell,
      "reference": this.reference,
      "create_at": this.createdAt.toIso8601String(),
      "photos": jsonEncode(this.photos),
      "category_id": this.category,
      "owner": this.owner?.toJson(),
      "adresse": this.adr,
      "furnished": this.furnished ? "1" : "0",
      'enterprise': this.enterprise != null
          ? this.enterprise.toJson()
          : this.owner?.enterprise != null
              ? this.owner.enterprise?.toJson()
              : null,
      'position': this.position?.toJson(),
      'favorite': this.favorite,
      'bathrooms': this.bathrooms.toString(),
      'land': this.landSize,
      'rooms': this.rooms.toString(),
      'is_liked': this.isLiked,
      'views': this.views,
    };
  }

  Map<String, dynamic> toRequest() {
    return {
      "title": this.title,
      "price": this.price,
      "description": this.description,
      "habitate_size": this.landSize,
      "for_sell": this.forSell,
      "reference": this.reference,
      "photos": this.photos,
      "category_id": this.category.id,
      "userid": Res.USER.id,
      "adresse": this.adr,
      "furnished": this.furnished ? "1" : "0",
      'lat': this.position?.lat,
      'lng': this.position?.lng,
      'bathrooms': this.bathrooms.toString(),
      'land_size': this.landSize,
      'bedrooms': this.rooms.toString(),
      'city': this.city.id,
      'region': this.region.id,
      'payement_type': null,
      'type': type.id,
      'swimming_pool': this.swimmingPool ? '1' : '0',
      'views': this.views,
    };
  }
}
