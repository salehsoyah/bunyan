import 'package:bunyan/models/person.dart';
import 'package:bunyan/models/product.dart';
import 'package:equatable/equatable.dart';

class NotificationModel with EquatableMixin {

  int id;
  String title;
  String text;
  ProductModel product;
  PersonModel owner;


  NotificationModel({this.id, this.title, this.text, this.product, this.owner});


  factory NotificationModel.fromJson(Map<String, dynamic> json) {
    return NotificationModel(
      id: int.parse(json["id"].toString()),
      title: json["title"],
      text: json["text"],
      product: ProductModel.fromJson(json["product"]),
      owner: PersonModel.fromJson(json["owner"]),
    );
  }


  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "title": this.title,
      "text": this.text,
      "product": this.product.toJson(),
      "owner": this.owner.toJson(),
    };
  }

  @override
  List<Object> get props => [id];

}