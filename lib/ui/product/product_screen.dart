import 'dart:async';

import 'package:bunyan/localization/language/languages.dart';
import 'package:bunyan/localization/locale_constant.dart';
import 'package:bunyan/models/chat.dart';
import 'package:bunyan/models/product.dart';
import 'package:bunyan/models/service.dart';
import 'package:bunyan/tools/res.dart';
import 'package:bunyan/tools/webservices/products.dart';
import 'package:bunyan/tools/webservices/users.dart';
import 'package:bunyan/ui/chat/conversation_screen.dart';
import 'package:bunyan/ui/common/location_picker.dart';
import 'package:bunyan/ui/main/main_screen.dart';
import 'package:bunyan/ui/notifications/notifications_screen.dart';
import 'package:bunyan/ui/profile/profile_agent.dart';
import 'package:bunyan/ui/profile/profile_screen.dart';
import 'package:bunyan/ui/profile/user_profile.dart';
import 'package:bunyan/ui/profileInfo.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:route_observer_mixin/route_observer_mixin.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';

class ProductScreen extends StatefulWidget {
  ProductScreen({Key key, @required this.product}) : super(key: key);
  final ProductModel product;

  @override
  _ProductScreenState createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen>
    with TickerProviderStateMixin, RouteAware, RouteObserverMixin {
  bool _seeAllDesc = false;
  ScrollController _scrollController = ScrollController();
  StreamController<int> _carouselStreamController =
      StreamController<int>.broadcast();
  ProductModel _product;
  bool _isFavoriting = false;
  bool _isLiking = false;
  bool _isFollowingEnterprise = false;

  String _phone;
  String address1;
  String address2;
  String address3;

  Completer<GoogleMapController> _controller = Completer();

  int _currentIndex = 0;
  bool favorite = false;
  bool isLiked = false;

  @override
  void initState() {
    super.initState();
    if (widget.product.favorite == true) {
      setState(() {
        favorite = true;
      });
    }
    if (widget.product.isLiked == true) {
      setState(() {
        isLiked = true;
      });
    }

    if (widget.product.position != null) {
      getUserLocation(widget.product.position.lat, widget.product.position.lng);
    }
    _product = widget.product;
    _scrollController.addListener(_scrollListener);
    _phone = (widget.product.owner?.phone ?? '').replaceAll(' ', '');
    _phone = _phone.replaceAll('+', '');
    if (_phone.startsWith('00')) _phone = _phone.replaceFirst('00', '');
  }

  _scrollListener() {
    if (_scrollController.position.userScrollDirection ==
        ScrollDirection.reverse)
      Res.bottomNavBarAnimStream.add(false);
    else
      Res.bottomNavBarAnimStream.add(true);
  }

  @override
  void didPop() {
    Res.bottomNavBarAnimStream.add(true);
    super.didPop();
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.removeListener(_scrollListener);
  }

  @override
  void didPopNext() {
    super.didPopNext();
    Res.titleStream.add('عقارات');
  }

  void openMaps(LatLng ltn) async {
    String googleUrl =
        'https://www.google.com/maps/search/?api=1&query=${ltn.latitude},${ltn.longitude}';
    if (await canLaunch(googleUrl) == null) {
      throw 'Could not open the map.';
    } else {
      await launch(googleUrl);
    }
  }

  @override
  void didPush() {
    super.didPush();
    Res.titleStream.add('عقارات');
  }

  double scale = 1.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            icon: const Icon(Icons.notifications_outlined),
            tooltip: 'Show Snackbar',
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => NotificationsScreen()));
            },
          ),
          PopupMenuButton<String>(
            icon: Icon(Icons.language_outlined),
            onSelected: (String result) {
              changeLanguage(context, result);
            },
            itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
              PopupMenuItem<String>(
                value: 'ar',
                child: Text('اَلْعَرَبِيَّةُ‎',
                    style: GoogleFonts.cairo(fontWeight: FontWeight.w700)),
              ),
              PopupMenuItem<String>(
                value: 'en',
                child: Text('English',
                    style: GoogleFonts.cairo(fontWeight: FontWeight.w700)),
              ),
            ],
          ),
        ],
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        elevation: 0,
        leading: InkWell(
          onTap: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => MainScreen(
                          menu_index: 0,
                        )));
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ClipOval(
              child: Image.asset(
                'assets/logo.min.png',
                height: 40,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        title: Text(
          "Real Estate",
          style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: SafeArea(
        child: Container(
          width: 1.sw,
          child: SingleChildScrollView(
            controller: _scrollController,
            primary: false,
            child: Column(
              children: [
                _slider(),
                _preview(),
                Padding(
                  padding: EdgeInsets.only(
                      left: 0.w, right: 0.w, top: 20.h, bottom: 30.h),
                  child: _descriptionCard(),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 30.w),
                  child: _moreDescCard(),
                ),
                widget.product.position != null &&
                        widget.product.position.lng != null
                    ? Padding(
                        padding: EdgeInsets.fromLTRB(30.w, 10, 30.w, 0),
                        child: Card(
                          elevation: 3.0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Location",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 15),
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Text("$address2\n$address1\n$address3")
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    height: 150,
                                    width: 150,
                                    child: GoogleMap(
                                      mapType: MapType.hybrid,
                                      initialCameraPosition: CameraPosition(
                                        target: LatLng(
                                            widget.product.position.lat,
                                            widget.product.position.lng),
                                        zoom: 14.4746,
                                      ),
                                      onMapCreated:
                                          (GoogleMapController controller) {
                                        _controller.complete(controller);
                                      },
                                      onTap: openMaps,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      )
                    : Container(),
                Padding(
                  padding: EdgeInsets.only(
                      left: 30.w, right: 30.w, bottom: 30.h, top: 30.h),
                  child: _userCard(),
                ),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 3.0,
        clipBehavior: Clip.antiAlias,
        child: Container(
          height: 65,
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: BottomNavigationBar(
                selectedLabelStyle:
                    GoogleFonts.cairo(fontWeight: FontWeight.w700),
                unselectedLabelStyle: GoogleFonts.cairo(),
                currentIndex: _currentIndex,
                backgroundColor: Colors.black,
                selectedItemColor: Colors.black,
                unselectedItemColor: Colors.grey,
                onTap: (index) {
                  print(index);
                  setState(() {
                    _currentIndex = index;
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MainScreen(
                                  menu_index: _currentIndex,
                                )));
                  });
                },
                items: [
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.home_outlined,
                    ),
                    activeIcon: Icon(Icons.home),
                    label: Languages.of(context).menuHome,
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.favorite_outline,
                    ),
                    activeIcon: Icon(Icons.favorite),
                    label: Languages.of(context).menuFavorite,
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      FontAwesome.comments,
                      color: Colors.transparent,
                    ),
                    label: '',
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.chat_outlined,
                    ),
                    activeIcon: Icon(Icons.chat),
                    label: Languages.of(context).productDetailsCallChat,
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.person_outline,
                    ),
                    activeIcon: Icon(Icons.person),
                    label: Languages.of(context).menuProfile,
                  ),
                ]),
          ),
        ),
      ),
      floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterDocked,
      floatingActionButton: MediaQuery.of(context).viewInsets.bottom == 0
          ? FloatingActionButton(
              mini: true,
              backgroundColor: Colors.black,
              child: Icon(FontAwesome.plus),
              onPressed: () => setState(() {}),
            )
          : Container(),
    );
  }

  Widget _textPair({String key, String value, IconData icon}) {
    return Row(
      children: [
        icon != null
            ? Icon(
                icon,
                size: 25.sp,
              )
            : Container(),
        icon != null
            ? SizedBox(
                width: 5.w,
              )
            : Container(),
        key != null
            ? Text(
                '$key:',
                style: GoogleFonts.cairo(
                    fontSize: 20.sp,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              )
            : Container(),
        SizedBox(
          width: key != null ? 10.w : .0,
        ),
        Text(
          value,
          style: GoogleFonts.cairo(fontSize: 23.sp, color: Colors.black),
        ),
      ],
    );
  }

  Widget _slider() {
    return Stack(
      children: [
        CarouselSlider.builder(
            options: CarouselOptions(
              height: .48.sh,
              onPageChanged: (pos, reson) {
                _carouselStreamController.add(pos);
              },
              viewportFraction: 1,
              autoPlay: widget.product.photos.length > 1,
              autoPlayInterval: Duration(seconds: 2, milliseconds: 500),
              enableInfiniteScroll: widget.product.photos.length > 1,
            ),
            itemCount: widget.product.photos.length,
            itemBuilder: (context, index, realIndex) => TransitionToImage(
                  image: AdvancedNetworkImage(widget.product.photos[index],
                      useDiskCache: true),
                  width: 1.sw,
                  fit: BoxFit.cover,
                  loadingWidget: _shimmer(),
                  placeholder: Icon(Icons.broken_image_outlined),
                  repeat: ImageRepeat.noRepeat,
                )),
        Positioned(
          top: .0,
          left: .0,
          child: Container(
            decoration: BoxDecoration(
                color: Colors.red,
                borderRadius:
                    BorderRadius.only(bottomRight: Radius.circular(20.0))),
            padding: EdgeInsets.symmetric(vertical: 5.w, horizontal: 25.w),
            child: Text(
              '${formatDecimal(widget.product.price)} QAR',
              style: GoogleFonts.cairo(
                  fontSize: 20.0.sp,
                  color: Colors.white.withOpacity(.9),
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        /*Positioned.fill(
          bottom: .0,
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(bottom: 20.h),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.black54,
                    borderRadius: BorderRadius.circular(5.0)),
                padding: EdgeInsets.all(10.w),
                child: StreamBuilder<int>(
                  stream: _carouselStreamController.stream,
                  builder: (context, snapshot) {
                    return Row(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: widget.product.photos
                          .map((e) =>
                          Icon(
                            widget.product.photos.indexOf(e) == snapshot.data
                                ? FontAwesome.circle
                                : FontAwesome.circle_o,
                            color: Colors.white,
                            size: 12.sp,
                          ))
                          .toList(),
                    );
                  }
                ),
              ),
            ),
          ),
        )*/
      ],
    );
  }

  Widget _descriptionCard() {
    return SizedBox(
      height: .20.sw,
      child: Row(
        //shrinkWrap: true,
        //scrollDirection: Axis.horizontal,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          //_descriptionItem(text: 'فيلا', icon: Icons.apartment),
          _descriptionItem(
              text: widget.product.rooms.toString() +
                  ' ' +
                  Languages.of(context).productDetailsRoom,
              icon: Icons.hotel),
          _descriptionItem(
              text: widget.product.bathrooms.toString() +
                  ' ' +
                  Languages.of(context).productDetailsBathRoom,
              icon: Icons.bathtub_sharp),
          _descriptionItem(
              text: widget.product.landSize ?? '',
              icon: Icons.zoom_out_map_rounded),
          _descriptionItem(
              text: widget.product.furnished
                  ? Languages.of(context).productDetailsFurniched
                  : Languages.of(context).productDetailsUnFurniched,
              icon: Icons.weekend),
        ],
      ),
    );
  }

  Widget _preview() {
    Size size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.only(top: 10.h, left: 25.w, right: 25.w),
      width: 1.sw,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text.rich(
                TextSpan(
                  children: [
                    WidgetSpan(child: Icon(Icons.remove_red_eye_sharp)),
                    TextSpan(text: '  ${widget.product.views}'),
                  ],
                ),
              ),
              Text.rich(
                TextSpan(
                  children: [
                    TextSpan(
                        text: 'Reference : ',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    TextSpan(text: '${widget.product.reference}'),
                  ],
                ),
              ),
            ],
          ),
          _textIcon(
            icon: Icons.date_range,
            text: DateTime.now().difference(widget.product.createdAt).inDays ==
                    0
                ? Languages.of(context).today
                : DateTime.now().difference(widget.product.createdAt).inDays <=
                        7
                    ? Languages.of(context).before +
                        ' ' +
                        DateTime.now()
                            .difference(widget.product.createdAt)
                            .inDays
                            .toString() +
                        ' ' +
                        Languages.of(context).days
                    : '${widget.product.createdAt.day}/${widget.product.createdAt.month}/${widget.product.createdAt.year}',
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ConstrainedBox(
                constraints: BoxConstraints(
                  maxWidth: size.width - 32,
                ),
                child: Text(
                  (widget.product.title ?? 'N/A') +
                      " " +
                      widget.product.id.toString(),
                  style: GoogleFonts.cairo(
                      color: Colors.black,
                      fontSize: 30.sp,
                      fontWeight: FontWeight.bold),
                ),
              ),
              // _textIcon(
              //     text: widget.product.adr.name ?? 'N/A',
              //     icon: Icons.location_pin),
            ],
          ),
          SizedBox(
            height: 35.w,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _productActionBtn(
                color: Colors.green,
                icon: Icons.share,
                text: Languages.of(context).productDetailsShare,
                onTap: () => _showErrorDialog(
                    text: 'غير متاح') /*Share.share('https://google.com')*/,
              ),
              SizedBox(
                width: 20.w,
              ),
              _productActionBtn(
                  color: Colors.red,
                  icon: Icons.favorite_border,
                  text: Languages.of(context).productDetailsFavorite,
                  onTap: _isFavoriting ? null : _updateFavorite,
                  done: favorite ? true : false),
              SizedBox(
                width: 20.w,
              ),
              _productActionBtn(
                  color: Colors.blue,
                  icon: FontAwesome.thumbs_up,
                  text: '223',
                  onTap: _likeProd,
                  done: isLiked ? true : false),
            ],
          )
        ],
      ),
    );
  }

  Widget _textIcon({String text, IconData icon}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Icon(
          icon,
          color: Colors.grey,
          size: 25.sp,
        ),
        SizedBox(
          width: 10.w,
        ),
        Text(
          text,
          style: GoogleFonts.cairo(
              color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 20.sp),
        ),
      ],
    );
  }

  Widget _userCard() {
    Size size = MediaQuery.of(context).size;
    return Stack(
      children: [
        Image.asset("assets/Image 280.jpg"),
        Padding(
          padding: EdgeInsets.only(top: 200.w),
          child: Card(
            elevation: 5.0,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 40.h, horizontal: 20),
              child: Column(
                children: [
                  SizedBox(
                    height: 40.w,
                  ),
                  SizedBox(
                    height: 20.h,
                  ),
                  SizedBox(
                    height: 20.h,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Expanded(
                        child: _contactBtn(
                            text: Languages.of(context).productDetailsCallWhats,
                            icon: FontAwesome.whatsapp,
                            onTap: () {
                              launch('https://wa.me/$_phone}');
                            },
                            color: Colors.green),
                      ),
                      SizedBox(
                        width: 5.sp,
                      ),
                      Expanded(
                        child: _contactBtn(
                            text: Languages.of(context).productDetailsCallPhone,
                            icon: FontAwesome.phone,
                            onTap: () {
                              launch('tel:${_phone}');
                            },
                            color: Colors.blue),
                      ),
                      SizedBox(
                        width: 5.sp,
                      ),
                      Expanded(
                        child: _contactBtn(
                            text: Languages.of(context).productDetailsCallChat,
                            icon: FontAwesome.comments,
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ConversationScreen(
                                            chat: ChatModel(
                                              receiver: widget.product.owner,
                                              sender: Res.USER,
                                            ),
                                            senderName:
                                                widget.product.owner.name,
                                          )));
                            },
                            color: Colors.red),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 40.h,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30),
                    child: MaterialButton(
                      onPressed: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ProfileAgent(
                                    profile: widget.product.owner,
                                    enterprise: widget.product.enterprise,
                                  ))),
                      child: Text(
                        Languages.of(context).productDetailsMoreAds,
                        style: GoogleFonts.cairo(
                            fontSize: 14, fontWeight: FontWeight.w700),
                      ),
                      textColor: Colors.black,
                      minWidth: double.infinity,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                          side: BorderSide(color: Colors.black)),
                      visualDensity: VisualDensity.comfortable,
                      padding: EdgeInsets.symmetric(vertical: 10),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        Positioned(
          top: 150.w,
          right: .0,
          left: .0,
          child: InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ProfileAgent(
                            profile: widget.product.owner,
                            enterprise: widget.product.enterprise,
                          )));
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Card(
                  color: Colors.grey,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5000.0)),
                  child: Container(
                    padding: EdgeInsets.all(1.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(5000.0),
                      child: Hero(
                        tag: 'profile_picture',
                        child: TransitionToImage(
                          image: AdvancedNetworkImage(
                              widget.product.owner?.photo ?? ''),
                          loadingWidget: _shimmer(width: 100.w, height: 100.w),
                          placeholder: Container(
                            width: 100.w,
                            height: 100.w,
                            child: Icon(Icons.broken_image_outlined),
                            color: Colors.white,
                          ),
                          width: 100.w,
                          height: 100.w,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
                Text(
                  widget.product.owner?.name ?? '',
                  style: GoogleFonts.cairo(fontSize: 15.sp),
                )
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8.0, right: 8.0 ,top: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [

              Positioned(
                  right: 10,
                  top: 10,
                  child: InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ProfileScreen(
                                  profile: widget.product.owner,
                                  enterprise: widget.product.enterprise,
                                )),
                      );
                    },
                    child: Container(
                      width: size.width * 0.25,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      padding:
                          EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                      alignment: Alignment.center,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "Go to profile",
                            style: GoogleFonts.cairo(
                                color: Colors.black,
                                fontSize: 13,
                                fontWeight: FontWeight.w700),
                          ),
                        ],
                      ),
                    ),
                  )),
            ],
          ),
        ),
      ],
    );
  }

  Widget _descriptionItem({String text, IconData icon}) {
    Size size = MediaQuery.of(context).size;
    return Card(
      elevation: 3.0,
      margin: EdgeInsets.symmetric(vertical: 5.h, horizontal: 3.w),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      child: Container(
        width: size.width * 0.22,
        height: .17.sw,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              icon,
              color: Colors.blue,
              size: 30.w,
            ),
            Text(
              text,
              style: GoogleFonts.cairo(
                  color: Colors.black54,
                  fontSize: 20.sp,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    );
  }

  Widget _contactBtn(
      {String text, IconData icon, Color color, Function() onTap}) {
    Size size = MediaQuery.of(context).size;
    return InkWell(
      onTap: onTap,
      child: Container(
        width: size.width * 0.25,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(5.0),
        ),
        padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        alignment: Alignment.center,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              icon,
              size: 27.sp,
              color: Colors.white,
            ),
            SizedBox(
              width: 5,
            ),
            Text(
              text,
              style: GoogleFonts.cairo(
                  color: Colors.white,
                  fontSize: 13,
                  fontWeight: FontWeight.w700),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> updateEntrFollow() async {
    setState(() {
      _isFollowingEnterprise = true;
    });
    final willFollow = !_product.enterprise.isFollowing;
    try {
      final success = await UsersWebService()
          .followEnterprise(id: _product.enterprise.id, follow: willFollow);
      if (!success)
        _showErrorDialog();
      else
        setState(() {
          _product.enterprise.isFollowing = willFollow;
          _product.enterprise.followers += willFollow ? 1 : -1;
          widget.product.enterprise.isFollowing = willFollow;
        });
    } catch (e) {
      _showErrorDialog();
    }
    setState(() {
      _isFollowingEnterprise = false;
    });
  }

  Widget _moreDescCard() {
    return Card(
      elevation: 5.0,
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: double.infinity,
              child: Text(
                Languages.of(context).productDetailsDescrption,
                style: GoogleFonts.cairo(
                    fontSize: 35.sp, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.w),
              child: AnimatedSize(
                duration: Duration(milliseconds: 300),
                vsync: this,
                child: Text(
                  _removeAllHtmlTags(widget.product.description),
                  maxLines: _seeAllDesc ? null : 5,
                  style:
                      GoogleFonts.cairo(fontSize: 20.sp, color: Colors.black),
                  overflow: _seeAllDesc ? null : TextOverflow.ellipsis,
                  //softWrap: true,
                ),
              ),
            ),
            InkWell(
              onTap: () {
                setState(() {
                  _seeAllDesc = !_seeAllDesc;
                });
              },
              child: Padding(
                padding: EdgeInsets.all(20.w),
                child: Text(
                  _seeAllDesc
                      ? Languages.of(context).showLess
                      : Languages.of(context).showMore,
                  style: GoogleFonts.cairo(
                      color: Colors.blue,
                      decoration: TextDecoration.underline,
                      fontSize: 18.sp),
                ),
              ),
            ),
            _textPair(
                key: Languages.of(context).productDetailsID, value: '2215441'),
            SizedBox(
              height: 40.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                _button(
                    text: Languages.of(context).productDetailsReport,
                    icon: Icons.assistant_photo,
                    borderColor: Colors.red,
                    onTap: _reportAd,
                    textColor: Colors.red),
                _button(
                    text: Languages.of(context).productDetailsShowOnMap,
                    icon: Icons.map_sharp,
                    borderColor: Colors.blue,
                    onTap: _showOnMap,
                    textColor: Colors.blue),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _reportAd() async {
    final success = await ProductsWebService().reportProduct(
        id: widget.product.id,
        type: widget.product is ServiceModel
            ? ProductsWebService.SERVICE
            : ProductsWebService.REAL_ESTATE);
    if (success)
      showDialog(
          context: context,
          builder: (context) => AlertDialog(
                content: Text(
                  'شكرا لقيامك بالتبليغ عن الإعلان.',
                  style: GoogleFonts.cairo(),
                ),
                actions: [
                  TextButton(
                      onPressed: () => Navigator.pop(context),
                      child: Text(
                        'موافق',
                        style: GoogleFonts.cairo(color: Colors.teal),
                      ))
                ],
              ));

    _showErrorDialog();
  }

  void _showErrorDialog({String text}) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              content: SingleChildScrollView(
                child: Text(
                  text ?? Languages.of(context).ServererrorPleasetryagainlater,
                  style: GoogleFonts.cairo(),
                ),
              ),
              actions: [
                TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: Text(
                      Languages.of(context).agreeon,
                      style: GoogleFonts.cairo(color: Colors.teal),
                    ))
              ],
            ));
  }

  Widget _button(
      {String text,
      Color color = Colors.transparent,
      Color borderColor = Colors.transparent,
      Color textColor = Colors.black,
      VoidCallback onTap,
      IconData icon}) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(5.0),
      child: InkWell(
        onTap: () {
          if (onTap != null) onTap();
        },
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: borderColor),
            color: color,
            borderRadius: BorderRadius.circular(5.0),
          ),
          padding: EdgeInsets.symmetric(vertical: 3.h, horizontal: 5.w),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Icon(
                icon,
                size: 17.sp,
                color: textColor,
              ),
              SizedBox(
                width: 8.w,
              ),
              Text(
                text,
                style: GoogleFonts.cairo(color: textColor, fontSize: 16.sp),
              )
            ],
          ),
        ),
      ),
    );
  }

  String formatDecimal(double n) {
    return n.toStringAsFixed(n.truncateToDouble() == n ? 0 : 1);
  }

  Widget _shimmer({double width, double height}) {
    return Shimmer.fromColors(
        child: Container(
          width: width,
          height: height,
          color: Colors.grey,
        ),
        baseColor: Colors.grey.withOpacity(.5),
        highlightColor: Colors.white);
  }

  String _removeAllHtmlTags(String htmlText) {
    if (htmlText == null) return 'N/A';
    RegExp exp = RegExp(r"<[^>]*>", multiLine: true, caseSensitive: true);

    return htmlText.replaceAll(exp, '');
  }

  Widget _enterpriseActionBtn(
      {Color color,
      IconData icon,
      String text,
      VoidCallback onTap,
      bool done = false}) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: 60,
        decoration: BoxDecoration(
            border: Border.all(color: color),
            borderRadius: BorderRadius.circular(5.0),
            color: done ? color : null),
        padding: EdgeInsets.symmetric(vertical: 3.h, horizontal: 5.w),
        alignment: Alignment.center,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              icon,
              size: 17.sp,
              color: !done ? color : Colors.white,
            ),
            SizedBox(
              width: 8.w,
            ),
            Text(
              text,
              style: GoogleFonts.cairo(
                  color: !done ? color : Colors.white, fontSize: 16.sp),
            )
          ],
        ),
      ),
    );
  }

  Widget _productActionBtn(
      {Color color,
      String text,
      IconData icon,
      bool done = false,
      Function() onTap}) {
    return InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: color),
          color: done ? color : null,
          borderRadius: BorderRadius.circular(5.0),
        ),
        padding: EdgeInsets.symmetric(vertical: 3.h, horizontal: 8.w),
        child: Center(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                icon,
                size: 17.sp,
                color: done ? Colors.white : color,
              ),
              SizedBox(
                width: 8.w,
              ),
              Text(
                text,
                style: GoogleFonts.cairo(
                    color: done ? Colors.white : color, fontSize: 16.sp),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _updateFavorite() async {
    setState(() {
      _isFavoriting = true;
    });
    try {
      await ProductsWebService().setFavorit(
          id: _product.id,
          isRealestate: !(_product is ServiceModel),
          set: !_product.favorite);
      setState(() {
        _product.favorite = !_product.favorite;
        widget.product.favorite = _product.favorite;
        favorite = true;
      });
    } catch (e) {
      _showErrorDialog();
    }
    setState(() {
      _isFavoriting = false;
    });
  }

  void _likeProd() {
    if (!_isLiking) {
      setState(() {
        _isLiking = true;
      });
      ProductsWebService()
          .likeProd(
              id: widget.product.id,
              isRealEstate: true,
              dislike: !widget.product.isLiked)
          .then((value) {
        setState(() {
          if (value) {
            widget.product.isLiked = !widget.product.isLiked;
            isLiked = true;
          } else
            _showErrorDialog();
          _isLiking = false;
        });
      });
    }
  }

  _showOnMap() {
    final position = _product.position;
    if (position != null && position.lat != null && position.lng != null)
      Navigator.push(
          Res.mainContext,
          MaterialPageRoute(
              builder: (context) => LocationPicker(
                    position: position,
                  )));
    else
      ScaffoldMessenger.of(Res.scaffoldKey.currentContext)
          .showSnackBar(SnackBar(content: Text('Lيس لهذا الإعلان موقع محدد')));
  }

  getUserLocation(double lt, lg) async {
    LocationData myLocation;
    String error;
    Location location = new Location();
    try {
      myLocation = await location.getLocation();
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        error = 'please grant permission';
        print(error);
      }
      if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        error = 'permission denied- please enable it from app settings';
        print(error);
      }
      myLocation = null;
    }
    final coordinates = new Coordinates(lt, lg);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    print(
        ' ${first.locality},, ${first.adminArea},,${first.subLocality},, ${first.subAdminArea},,${first.addressLine},, ${first.featureName},,${first.thoroughfare},, ${first.subThoroughfare}');
    setState(() {
      address1 = first.locality;
      address2 = first.subLocality;
      address3 = first.addressLine;
    });
    return first;
  }
}
