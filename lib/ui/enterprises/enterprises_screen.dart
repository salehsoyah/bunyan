import 'package:bunyan/localization/language/languages.dart';
import 'package:bunyan/localization/locale_constant.dart';
import 'package:bunyan/models/enterprise.dart';
import 'package:bunyan/tools/res.dart';
import 'package:bunyan/tools/webservices/products.dart';
import 'package:bunyan/ui/enterprises/enterprise.dart';
import 'package:bunyan/ui/main/main_screen.dart';
import 'package:bunyan/ui/notifications/notifications_screen.dart';
import 'package:bunyan/ui/profile/profile_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:route_observer_mixin/route_observer_mixin.dart';
import 'package:url_launcher/url_launcher.dart';

class EnterprisesScreen extends StatefulWidget {
  EnterprisesScreen({Key key}) : super(key: key);

  @override
  _EnterprisesScreenState createState() => _EnterprisesScreenState();
}

class _EnterprisesScreenState extends State<EnterprisesScreen>
    with RouteAware, RouteObserverMixin {
  List<EnterpriseModel> _enterprises = [];
  List<EnterpriseModel> _enterprisesSearch = [];

  ScrollController _scrollController = ScrollController();
  bool isLoadingSearch = false;
  Locale _locale;
  String query;
  int _currentIndex = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Res.titleStream.add('شركات');
    _scrollController.addListener(_scrollListener);
    ProductsWebService().getEnterprises().then((entrs) {
      print(entrs);
      setState(() {
        _enterprises = entrs;
      });
    });
  }

  _searchEnterprises() {
    _enterprisesSearch.clear();
    setState(() {
      _enterprisesSearch = _enterprises.where((enterprise) {
        final titleLower = enterprise.name.toLowerCase();

        return titleLower.contains(this.query.toLowerCase());
      }).toList();
    });
  }

  _scrollListener() {
    if (_scrollController.position.userScrollDirection ==
        ScrollDirection.reverse)
      Res.bottomNavBarAnimStream.add(false);
    else
      Res.bottomNavBarAnimStream.add(true);
  }

  @override
  void didPop() {
    Res.bottomNavBarAnimStream.add(true);
    super.didPop();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _scrollController.removeListener(_scrollListener);
  }

  @override
  void didPopNext() {
    Res.titleStream.add('شركات');
    super.didPopNext();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            icon: const Icon(Icons.notifications_outlined),
            tooltip: 'Show Snackbar',
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => NotificationsScreen()));
            },
          ),
          PopupMenuButton<String>(
            icon: Icon(Icons.language_outlined),
            onSelected: (String result) {
              changeLanguage(context, result);
            },
            itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
              PopupMenuItem<String>(
                value: 'ar',
                child: Text('اَلْعَرَبِيَّةُ‎',
                    style: GoogleFonts.cairo(fontWeight: FontWeight.w700)),
              ),
              PopupMenuItem<String>(
                value: 'en',
                child: Text('English',
                    style: GoogleFonts.cairo(fontWeight: FontWeight.w700)),
              ),
            ],
          ),
        ],
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        elevation: 0,
        leading: InkWell(
          onTap: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => MainScreen(
                      menu_index: 0,
                    )));
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ClipOval(
              child: Image.asset(
                'assets/logo.min.png',
                height: 40,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        title: Text(
          Languages.of(context).agencies,
          style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(2.0),
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: Image.asset("assets/Artboard – 3.jpg")),
            ),
            _searchWidget(),
            Expanded(
              child: ListView.builder(
                  shrinkWrap: true,
                  controller: _scrollController,
                  itemCount: _enterprisesSearch.isNotEmpty
                      ? _enterprisesSearch.length
                      : _enterprises.length,
                  itemBuilder: (context, index) => InkWell(
                        onTap: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => EnterpriseScreen(
                                      enterprise: _enterprisesSearch.isNotEmpty
                                          ? _enterprisesSearch[index]
                                          : _enterprises[index],
                                    ))),
                        child: Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          elevation: 5.0,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Flexible(
                                  child: Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 12.w),
                                    child: Column(
                                      children: [
                                        Text(
                                          _enterprisesSearch.isNotEmpty
                                              ? _enterprisesSearch[index].name
                                              : _enterprises[index].name,
                                          style: GoogleFonts.cairo(
                                              fontSize: 24.sp,
                                              fontWeight: FontWeight.bold),
                                          maxLines: 1,
                                          textDirection: TextDirection.ltr,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        SizedBox(
                                          height: 12.h,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 15.0, right: 15.0),
                                          child: Text(
                                            _enterprisesSearch.isNotEmpty
                                                ? (_enterprisesSearch[index]
                                                            .description !=
                                                        ''
                                                    ? _enterprisesSearch[index]
                                                        .description
                                                    : '')
                                                : (_enterprises[index]
                                                            .description !=
                                                        ''
                                                    ? _enterprises[index]
                                                        .description
                                                    : ''),
                                            style: GoogleFonts.cairo(
                                                fontSize: 20.sp),
                                            maxLines: 3,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 12.h,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            _enterprisesSearch.isNotEmpty
                                                ? (_enterprisesSearch[index]
                                                                .phone !=
                                                            null &&
                                                        _enterprisesSearch[
                                                                index]
                                                            .phone
                                                            .isNotEmpty
                                                    ? InkWell(
                                                        onTap: () => _call(
                                                            _enterprisesSearch[
                                                                    index]
                                                                .phone),
                                                        child: Padding(
                                                          padding:
                                                              EdgeInsets.all(
                                                                  10.w),
                                                          child: Icon(
                                                            Icons.phone,
                                                            size: 30.sp,
                                                          ),
                                                        ),
                                                      )
                                                    : Container())
                                                : _enterprises[index].phone !=
                                                            null &&
                                                        _enterprises[index]
                                                            .phone
                                                            .isNotEmpty
                                                    ? InkWell(
                                                        onTap: () => _call(
                                                            _enterprises[index]
                                                                .phone),
                                                        child: Padding(
                                                          padding:
                                                              EdgeInsets.all(
                                                                  10.w),
                                                          child: Icon(
                                                            Icons.phone,
                                                            size: 30.sp,
                                                          ),
                                                        ),
                                                      )
                                                    : Container(),
                                            _enterprisesSearch.isNotEmpty
                                                ? (_enterprisesSearch[index]
                                                                .email !=
                                                            null &&
                                                        _enterprisesSearch[
                                                                index]
                                                            .email
                                                            .isNotEmpty
                                                    ? InkWell(
                                                        onTap: () => _sendMail(
                                                            _enterprises[index]
                                                                .email),
                                                        child: Padding(
                                                          padding:
                                                              EdgeInsets.all(
                                                                  10.w),
                                                          child: Icon(
                                                            Icons.email,
                                                            size: 30.sp,
                                                          ),
                                                        ),
                                                      )
                                                    : Container())
                                                : _enterprises[index].email !=
                                                            null &&
                                                        _enterprises[index]
                                                            .email
                                                            .isNotEmpty
                                                    ? InkWell(
                                                        onTap: () => _sendMail(
                                                            _enterprises[index]
                                                                .email),
                                                        child: Padding(
                                                          padding:
                                                              EdgeInsets.all(
                                                                  10.w),
                                                          child: Icon(
                                                            Icons.email,
                                                            size: 30.sp,
                                                          ),
                                                        ),
                                                      )
                                                    : Container(),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 10.h,
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(10.0),
                                  child: SizedBox(
                                    width: .3.sw,
                                    height: .3.sw,
                                    child: TransitionToImage(
                                      image: AdvancedNetworkImage(
                                          _enterprisesSearch.isNotEmpty
                                              ? (_enterprisesSearch[index]
                                                          .photo !=
                                                      ''
                                                  ? _enterprisesSearch[index]
                                                      .photo
                                                  : '')
                                              : (_enterprises[index].photo != ''
                                                  ? _enterprises[index].photo
                                                  : '')),
                                      width: .3.sw,
                                      fit: BoxFit.cover,
                                      height: .3.sw,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                )
                              ],
                            ),
                          ),
                        ),
                      )),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 3.0,
        clipBehavior: Clip.antiAlias,
        child: Container(
          height: 65,
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: BottomNavigationBar(
                selectedLabelStyle:
                    GoogleFonts.cairo(fontWeight: FontWeight.w700),
                unselectedLabelStyle: GoogleFonts.cairo(),
                currentIndex: _currentIndex,
                backgroundColor: Colors.black,
                selectedItemColor: Colors.black,
                unselectedItemColor: Colors.grey,
                onTap: (index) {
                  print(index);
                  setState(() {
                    _currentIndex = index;
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MainScreen(
                                  menu_index: _currentIndex,
                                )));
                  });
                },
                items: [
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.home_outlined,
                    ),
                    activeIcon: Icon(Icons.home),
                    label: Languages.of(context).menuHome,
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.favorite_outline,
                    ),
                    activeIcon: Icon(Icons.favorite),
                    label: Languages.of(context).menuFavorite,
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      FontAwesome.comments,
                      color: Colors.transparent,
                    ),
                    label: '',
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.chat_outlined,
                    ),
                    activeIcon: Icon(Icons.chat),
                    label: Languages.of(context).productDetailsCallChat,
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.person_outline,
                    ),
                    activeIcon: Icon(Icons.person),
                    label: Languages.of(context).menuProfile,
                  ),
                ]),
          ),
        ),
      ),
      floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterDocked,
      floatingActionButton: MediaQuery.of(context).viewInsets.bottom == 0
          ? FloatingActionButton(
              mini: true,
              backgroundColor: Colors.black,
              child: Icon(FontAwesome.plus),
              onPressed: () => setState(() {}),
            )
          : Container(),
    );
  }

  Widget _searchWidget() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 15, horizontal: 4),
      child: SizedBox(
        height: 40,
        child: Row(
          children: [
            Expanded(
              child: TextField(
                onChanged: (value) {
                  setState(() {
                    query = value;
                    if (query == '') {
                      _enterprisesSearch.clear();
                    }
                  });
                },
                cursorHeight: 26,
                cursorColor: Colors.black,
                enabled: true,
                style: GoogleFonts.cairo(fontSize: 26.sp),
                decoration: InputDecoration(
                  border: InputBorder.none,
                  focusedErrorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(color: Colors.black)),
                  disabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(color: Colors.black)),
                  errorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(color: Colors.red)),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(color: Colors.black)),
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(color: Colors.black)),
                  contentPadding:
                      EdgeInsets.only(top: 0, left: 20.w, right: 20),
                  hintStyle:
                      GoogleFonts.cairo(color: Colors.black38, fontSize: 30.sp),
                  hintText: Languages.of(context).searchagence,
                  suffixIcon: GestureDetector(
                    onTap: (){
                      _searchEnterprises();
                    },
                    child: Container(
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: (Radius.circular(
                              _locale.toString() == 'ar' ? 10 : 0)),
                          bottomLeft: (Radius.circular(
                              _locale.toString() == 'ar' ? 10 : 0)),
                          topRight: (Radius.circular(
                              _locale.toString() == 'ar' ? 0 : 10)),
                          bottomRight: (Radius.circular(
                              _locale.toString() == 'ar' ? 0 : 10)),
                        ),
                        color: Colors.black,
                      ),
                      child: !isLoadingSearch
                          ? Icon(
                              Icons.search,
                              color: Colors.white,
                              size: 24,
                            )
                          : const SizedBox(
                              height: 20,
                              width: 20,
                              child: CircularProgressIndicator(
                                strokeWidth: 2,
                                color: Colors.white,
                              ),
                            ),
                    ),
                  ),
                  suffixIconConstraints:
                      BoxConstraints(minWidth: 60, minHeight: 40, maxWidth: 60),
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }

  void _sendMail(String email) {
    launch('mailto:$email');
  }

  void _call(String phone) {
    launch('tel:$phone');
  }
}
