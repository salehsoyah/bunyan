import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:bunyan/localization/language/languages.dart';
import 'package:bunyan/localization/locale_constant.dart';
import 'package:bunyan/models/chat.dart';
import 'package:bunyan/models/enterprise.dart';
import 'package:bunyan/models/person.dart';
import 'package:bunyan/models/product.dart';
import 'package:bunyan/models/product_list.dart';
import 'package:bunyan/tools/res.dart';
import 'package:bunyan/tools/webservices/advertises.dart';
import 'package:bunyan/tools/webservices/products.dart';
import 'package:bunyan/tools/webservices/users.dart';
import 'package:bunyan/ui/chat/conversation_screen.dart';
import 'package:bunyan/ui/common/card_item.dart';
import 'package:bunyan/ui/main/main_screen.dart';
import 'package:bunyan/ui/notifications/notifications_screen.dart';
import 'package:bunyan/ui/onBoardingScreen.dart';
import 'package:bunyan/ui/product/product_screen.dart';
import 'package:clay_containers/widgets/clay_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:route_observer_mixin/route_observer_mixin.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:image_picker/image_picker.dart';

import '../actionButton.dart';
import '../profileInfo.dart';
import '../redirect_to_auth.dart';

class EnterpriseScreen extends StatefulWidget {
  EnterpriseScreen({Key key, this.enterprise}) : super(key: key);

  final EnterpriseModel enterprise;

  @override
  _EnterpriseScreenState createState() => _EnterpriseScreenState();
}

class _EnterpriseScreenState extends State<EnterpriseScreen>
    with RouteAware, RouteObserverMixin {
  List<ProductModel> _products;
  bool isLoading;
  PersonModel _profile;
  bool _requestingFollowing = false;
  String _phone;
  String photoProfile;
  Locale currentLang;
  int _currentIndex = 0;
  bool details = false;

  double long = 49.5;
  double lat = -0.09;
  var location = [];

  bool _isFetching = true;

  String address1;
  String address2;
  String address3;

  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 30.4746,
  );

  @override
  void initState() {
    getUserLocation(widget.enterprise.lat, widget.enterprise.lng);

    isLoading = true;
    getCurrentLang();
    getData();
    super.initState();
    if (_profile != null) {
      _phone = _profile.phone?.replaceAll(' ', '') ?? '';
      _phone.replaceAll('+', '');
      if (_phone.startsWith('00')) _phone = _phone.replaceFirst('00', '');

      // UsersWebService().getUserProfile(_profile.id).then((response) {
      //   setState(() {
      //     final oldProfile = PersonModel.fromJson(_profile.toJson());
      //     _profile = response['user'];
      //     photoProfile = _profile.photo;
      //     _products = [];
      //     _products.addAll(response['products']);
      //     _products.addAll(response['services']);
      //
      //     if (oldProfile == Res.USER) {
      //       Res.USER = _profile;
      //       SharedPreferences.getInstance().then((prefs) {
      //         prefs.setString('user', jsonEncode(Res.USER.toJson()));
      //       });
      //     }
      //     isLoading = false;
      //   });
      // });
    }
  }

  getData() async {
    final futures = await Future.wait([
      ProductsWebService().getHomeProducts(),
      AdvertisesWebService().getBanners(),
    ]);
    setState(() {
      _products = futures[0];
      _isFetching = false;
    });
  }

  getCurrentLang() async {
    getLocale().then((locale) {
      setState(() {
        currentLang = locale;
      });
    });
  }

  void openMaps(LatLng ltn) async {
    String googleUrl =
        'https://www.google.com/maps/search/?api=1&query=${ltn.latitude},${ltn.longitude}';
    if (await canLaunch(googleUrl) == null) {
      throw 'Could not open the map.';
    } else {
      await launch(googleUrl);
    }
  }

  logOut() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
    setState(() {
      Res.USER = null;
    });
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => OnBoardingScreen()));
  }

  @override
  void didPopNext() {
    Res.titleStream.add('الملف الشخصي');
    super.didPopNext();
  }

  @override
  void didPush() {
    super.didPush();
    Res.titleStream.add('الملف الشخصي');
  }

  File personalPhoto;

  _getFromGallery() async {
    var pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);
    setState(() {
      personalPhoto = File(pickedFile.path);
      uploadPhotoProfile(personalPhoto);
      pickedFile = null;
    });
  }

  _getFromCamera() async {
    var pickedFile = await ImagePicker().getImage(source: ImageSource.camera);
    setState(() {
      personalPhoto = File(pickedFile.path);
      uploadPhotoProfile(personalPhoto);
      pickedFile = null;
    });
  }

  uploadPhotoProfile(File photo) async {
    setState(() {
      isLoading = true;
    });
    UsersWebService()
        .updatePhotoProfile(idsession: _profile.id, profilePhoto: photo)
        .then((data) {
      setState(() {
        photoProfile = data['photo_url'];
        print('photoProfile $photoProfile');
        isLoading = false;
        if (data != null) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              elevation: 0,
              backgroundColor: Colors.black,
              behavior: SnackBarBehavior.floating,
              padding: EdgeInsets.all(0),
              content: Container(
                  padding: EdgeInsets.all(15),
                  child: Row(
                    children: [
                      Text(
                        Languages.of(context).editPhotoProfileSuccess,
                        style: GoogleFonts.cairo(
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                        ),
                      )
                    ],
                  )),
            ),
          );
          setState(() {
            initState();
            isLoading = false;
          });
        }
      });
    }).onError((error, stackTrace) {
      setState(() {
        isLoading = false;
      });
    });
  }

  void _showPicker(context) {
    showModalBottomSheet(
        backgroundColor: Colors.white,
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(35.0)),
        ),
        builder: (BuildContext bc) {
          Size size = MediaQuery.of(context).size;
          return SafeArea(
            child: Container(
              height: 140,
              padding: const EdgeInsets.symmetric(vertical: 20),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.vertical(top: Radius.circular(34.0)),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      _getFromGallery();
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      padding: const EdgeInsets.all(20),
                      width: size.width * 0.4,
                      decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(15)),
                      child: Column(
                        children: [
                          const Icon(
                            Icons.photo_library,
                            color: Colors.white,
                          ),
                          const SizedBox(height: 5),
                          Text(
                            Languages.of(context).modalBottomSheetPhotoLibrary,
                            style: const TextStyle(
                              fontWeight: FontWeight.w700,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      _getFromCamera();
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      padding: const EdgeInsets.all(20),
                      width: size.width * 0.4,
                      decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(15)),
                      child: Column(
                        children: [
                          const Icon(
                            Icons.photo_camera,
                            color: Colors.white,
                          ),
                          const SizedBox(height: 5),
                          Text(
                            Languages.of(context).modalBottomSheetCamera,
                            style: const TextStyle(
                              fontWeight: FontWeight.w700,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  Widget buttonIcon() {
    return Icon(
      Icons.chevron_right,
      color: Colors.grey,
      size: 30,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            icon: const Icon(Icons.notifications_outlined),
            tooltip: 'Show Snackbar',
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => NotificationsScreen()));
            },
          ),
          PopupMenuButton<String>(
            icon: Icon(Icons.language_outlined),
            onSelected: (String result) {
              changeLanguage(context, result);
            },
            itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
              PopupMenuItem<String>(
                value: 'ar',
                child: Text('اَلْعَرَبِيَّةُ‎',
                    style: GoogleFonts.cairo(fontWeight: FontWeight.w700)),
              ),
              PopupMenuItem<String>(
                value: 'en',
                child: Text('English',
                    style: GoogleFonts.cairo(fontWeight: FontWeight.w700)),
              ),
            ],
          ),
        ],
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        elevation: 0,
        leading: InkWell(
          onTap: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => MainScreen(
                      menu_index: 0,
                    )));
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ClipOval(
              child: Image.asset(
                'assets/logo.min.png',
                height: 40,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        title: Text(
          "Agencies",
          style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: Container(
        color: Color(0XFFeeeeee),
        child: NestedScrollView(
            headerSliverBuilder: (context, scrolled) {
              return [
                SliverAppBar(
                  backgroundColor: Color(0XFFeeeeee),
                  expandedHeight: .55.sh,
                  //collapsedHeight: kToolbarHeight,
                  automaticallyImplyLeading: false,
                  flexibleSpace: LayoutBuilder(builder: (context, constraints) {
                    double scale = 1.0;
                    if (constraints.biggest.height <= 290)
                      scale = (constraints.biggest.height - 56) / (290 - 56.0);
                    return Stack(
                      children: [
                        FlexibleSpaceBar(
                          collapseMode: CollapseMode.parallax,
                          background: Padding(
                            padding: EdgeInsets.only(
                                right: 4, left: 4, bottom: .26.sh),
                            child: ClipRRect(
                                child: Hero(
                              tag: 'profile_picture',
                              child: widget.enterprise.photo != null
                                  ? TransitionToImage(
                                      image: AssetImage('assets/Image 280.jpg'),
                                      loadingWidget:
                                          _shimmer(width: 1.sw, height: 1.sw),
                                      placeholder: Container(
                                        width: 1.sw,
                                        height: 1.sw,
                                        child:
                                            Icon(Icons.broken_image_outlined),
                                        color: Colors.white,
                                      ),
                                      width: 1.sw,
                                      fit: BoxFit.cover,
                                    )
                                  : Image(
                                      image: AssetImage('assets/Image 280.jpg'),
                                      fit: BoxFit.cover,
                                    ),
                            )),
                          ),
                        ),
                        Positioned(
                            right: 10,
                            top: 10,
                            child: Container(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Icon(
                                  Icons.share,
                                  size: 20,
                                ),
                              ),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                            )),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: AnimatedOpacity(
                            opacity: scale,
                            duration: Duration(seconds: 0),
                            child: Transform.scale(
                              scale: scale,
                              origin: Offset(.0, .2.sh),
                              child: Card(
                                elevation: 3.0,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0)),
                                child: Stack(
                                  clipBehavior: Clip.none,
                                  children: [
                                    Container(
                                        height: .35.sh,
                                        width: .85.sw,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                        ),
                                        padding: EdgeInsets.symmetric(
                                            vertical: 25.h, horizontal: 30.w),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            SizedBox(height: 60),
                                            Text(
                                              widget.enterprise.name,
                                              style: GoogleFonts.cairo(
                                                  fontWeight: FontWeight.w900,
                                                  fontSize: 32.sp),
                                              textAlign: TextAlign.center,
                                            ),
                                            Text("Villas and Apartments"),
                                            Divider(color: Colors.black),
                                            Text(
                                              widget.enterprise.followers
                                                  .toString(),
                                              style: GoogleFonts.cairo(
                                                  fontWeight: FontWeight.w900,
                                                  fontSize: 32.sp),
                                            ),
                                            Text("Followers"),
                                          ],
                                        )),
                                    Positioned(
                                      bottom: -5,
                                      right: 0,
                                      left: 0,
                                      child: MaterialButton(
                                        onPressed: () {},
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            "FOLLOW",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 17,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        color: Colors.lightBlue,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                      ),
                                    ),
                                    Positioned(
                                        top: -70,
                                        left:
                                            MediaQuery.of(context).size.width *
                                                    0.5 -
                                                .075.sw -
                                                60,
                                        child: GestureDetector(
                                          onTap: () {
                                            _showPicker(context);
                                          },
                                          child: ProfileInfo(
                                              widget.enterprise.photo != null
                                                  ? widget.enterprise.photo
                                                  : 'http://sanjaymotels.com/wp-content/uploads/2019/01/testimony.png',
                                              false),
                                        )),
                                    Positioned(
                                      top: -20,
                                      right: currentLang.toString() == 'en'
                                          ? 0
                                          : .85.sw - 40,
                                      child: (widget.enterprise == null &&
                                              _profile == Res.USER)
                                          ? GestureDetector(
                                              child: Container(
                                                height: 45,
                                                width: 45,
                                                decoration: BoxDecoration(
                                                    color: Colors.black,
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                40))),
                                                child: Icon(
                                                  Icons.edit,
                                                  color: Colors.white,
                                                  size: 28,
                                                ),
                                              ),
                                            )
                                          : Container(),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    );
                  }),
                )
              ];
            },
            body: Scaffold(
              bottomNavigationBar: BottomAppBar(
                shape: CircularNotchedRectangle(),
                notchMargin: 3.0,
                clipBehavior: Clip.antiAlias,
                child: Container(
                  height: 65,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                    child: BottomNavigationBar(
                        selectedLabelStyle:
                            GoogleFonts.cairo(fontWeight: FontWeight.w700),
                        unselectedLabelStyle: GoogleFonts.cairo(),
                        currentIndex: _currentIndex,
                        backgroundColor: Colors.black,
                        selectedItemColor: Colors.black,
                        unselectedItemColor: Colors.grey,
                        onTap: (index) {
                          print(index);
                          setState(() {
                            _currentIndex = index;
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => MainScreen(
                                          menu_index: _currentIndex,
                                        )));
                          });
                        },
                        items: [
                          BottomNavigationBarItem(
                            icon: Icon(
                              Icons.home_outlined,
                            ),
                            activeIcon: Icon(Icons.home),
                            label: Languages.of(context).menuHome,
                          ),
                          BottomNavigationBarItem(
                            icon: Icon(
                              Icons.favorite_outline,
                            ),
                            activeIcon: Icon(Icons.favorite),
                            label: Languages.of(context).menuFavorite,
                          ),
                          BottomNavigationBarItem(
                            icon: Icon(
                              FontAwesome.comments,
                              color: Colors.transparent,
                            ),
                            label: '',
                          ),
                          BottomNavigationBarItem(
                            icon: Icon(
                              Icons.chat_outlined,
                            ),
                            activeIcon: Icon(Icons.chat),
                            label: Languages.of(context).productDetailsCallChat,
                          ),
                          BottomNavigationBarItem(
                            icon: Icon(
                              Icons.person_outline,
                            ),
                            activeIcon: Icon(Icons.person),
                            label: Languages.of(context).menuProfile,
                          ),
                        ]),
                  ),
                ),
              ),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.miniCenterDocked,
              floatingActionButton:
                  MediaQuery.of(context).viewInsets.bottom == 0
                      ? FloatingActionButton(
                          mini: true,
                          backgroundColor: Colors.black,
                          child: Icon(FontAwesome.plus),
                          onPressed: () => setState(() {}),
                        )
                      : Container(),
              body: Align(
                  alignment: Alignment.topCenter,
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        details == false
                            ? Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: TextButton(
                                  onPressed: () {
                                    setState(() {
                                      details = true;
                                    });
                                  },
                                  child: Text(
                                    "Company Details >>",
                                    style: TextStyle(
                                        color: Colors.lightBlue,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              )
                            : Container(),
                        details == true
                            ? Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(25, 10, 25, 0),
                                child: Card(
                                  elevation: 3.0,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(10.0)),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(widget.enterprise.description),
                                  ),
                                ),
                              )
                            : Container(),
                        details == true && widget.enterprise.lng != 0
                            ? Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(25, 10, 25, 0),
                                child: Card(
                                  elevation: 3.0,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(10.0)),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "Location",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 15),
                                                ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Text(
                                                    "$address2\n$address1\n$address3")
                                              ],
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Container(
                                            height: 150,
                                            width: 150,
                                            child: GoogleMap(
                                              mapType: MapType.hybrid,
                                              initialCameraPosition:
                                                  CameraPosition(
                                                target: LatLng(
                                                    widget.enterprise.lat,
                                                    widget.enterprise.lng),
                                                zoom: 14.4746,
                                              ),
                                              onMapCreated: (GoogleMapController
                                                  controller) {
                                                _controller
                                                    .complete(controller);
                                              },
                                              onTap: openMaps,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              )
                            : Container(),
                        details == true
                            ? Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(25, 10, 25, 0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: MaterialButton(
                                        onPressed: () {
                                          launch(
                                              'mailto:${widget.enterprise.email}');
                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              2, 7, 2, 7),
                                          child: Text(
                                            "Email",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 17,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        color: Colors.red,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Expanded(
                                      child: MaterialButton(
                                        onPressed: () {
                                          launch(
                                              'tel:${widget.enterprise.phone}');
                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              2, 7, 2, 7),
                                          child: Text(
                                            "Call",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 17,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        color: Colors.blue,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Expanded(
                                      child: MaterialButton(
                                        onPressed: () {
                                          launch(
                                              'https://wa.me/${widget.enterprise.phone}');
                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              2, 7, 2, 7),
                                          child: Text(
                                            "Whatsapp",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 17,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        color: Colors.green,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            : Container(),
                        SizedBox(
                          height: 20,
                        ),
                        CustomScrollView(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(), //
                          slivers: [
                            SliverPadding(
                              padding: EdgeInsets.only(
                                  top: 0,
                                  right: .02.sw,
                                  left: .02.sw,
                                  bottom: 0.1.sh),
                              sliver: SliverStaggeredGrid.countBuilder(
                                crossAxisCount: 2,
                                itemCount: _isFetching ? 4 : _products.length,
                                staggeredTileBuilder: (index) =>
                                    StaggeredTile.fit(1),
                                crossAxisSpacing: .0,
                                itemBuilder: (context, index) {
                                  // print(' _products[index] ${_products[index].toJson()}');
                                  return _isFetching
                                      ? _shimmerItem(index)
                                      : _products.length > 0
                                          ? InkWell(
                                              onTap: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            ProductScreen(
                                                              product:
                                                                  _products[
                                                                      index],
                                                            )));
                                                Res.titleStream.add('عقارات');
                                              },
                                              child: CardItem(
                                                  product: _products[index]),
                                            )
                                          : Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 20),
                                              child: Text(
                                                Languages.of(context)
                                                    .redirectToAuthMessage,
                                                style: GoogleFonts.cairo(
                                                  fontSize: 20,
                                                  fontWeight: FontWeight.w700,
                                                  color: Colors.black,
                                                ),
                                              ),
                                            );
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  )),
            )),
      ),
    );
  }

  Widget _contactBtn({String text, IconData icon, Color color, dynamic onTap}) {
    return InkWell(
      onTap: () {
        if (onTap != null) onTap();
      },
      child: Container(
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(5.0),
        ),
        padding: EdgeInsets.symmetric(vertical: 3.h, horizontal: 15.w),
        alignment: Alignment.center,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              text,
              style: GoogleFonts.cairo(color: Colors.white, fontSize: 23.sp),
            ),
            SizedBox(
              width: 12.w,
            ),
            Icon(
              icon,
              size: 27.sp,
              color: Colors.white,
            )
          ],
        ),
      ),
    );
  }

  _shimmerItem(index) {
    return Card(
      elevation: 2.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        textDirection: TextDirection.rtl,
        mainAxisSize: MainAxisSize.min,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: Stack(
              children: [
                Shimmer.fromColors(
                  baseColor: const Color(0xFFf3f3f3),
                  highlightColor: const Color(0xFFE8E8E8),
                  child: Container(
                    height: index.isOdd ? 380.h : 280.h,
                    width: double.infinity,
                    color: Colors.grey,
                  ),
                ),
                Positioned(
                  top: .0,
                  left: .0,
                  child: Shimmer.fromColors(
                    baseColor: Color(0xffbfbdbd),
                    highlightColor: const Color(0xFFE8E8E8),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(.8),
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(20.0))),
                      width: .25.sw,
                      height: 30.h,
                      padding:
                          EdgeInsets.symmetric(vertical: 5.w, horizontal: 25.w),
                    ),
                  ),
                ),
                Positioned.fill(
                    child: ClipRRect(
                  borderRadius: BorderRadius.circular(3.0),
                  child: Container(
                    width: 1.sw,
                    alignment: Alignment.bottomRight,
                    child: Padding(
                      padding: EdgeInsets.only(
                          right: 20.w, bottom: 20.h, left: 15.w),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Padding(
                                padding: EdgeInsets.only(left: 10.w),
                                child: Shimmer.fromColors(
                                  baseColor: Color(0xffbfbdbd),
                                  highlightColor: const Color(0xFFE8E8E8),
                                  child: Container(
                                    width: .1.sw,
                                    height: 20.h,
                                    color: Colors.grey.withOpacity(.8),
                                  ),
                                )),
                          ),
                          Icon(
                            Icons.location_pin,
                            color: Colors.white,
                            size: 22.w,
                          ),
                          Shimmer.fromColors(
                            baseColor: Color(0xffbfbdbd),
                            highlightColor: const Color(0xFFE8E8E8),
                            child: Container(
                              width: .08.sw,
                              height: 10.h,
                              color: Colors.grey.withOpacity(.8),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ))
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _shimmer({double width, double height}) {
    return Shimmer.fromColors(
        child: Container(
          width: width,
          height: height,
          color: Colors.grey,
        ),
        baseColor: Colors.grey.withOpacity(.5),
        highlightColor: Colors.white);
  }

// _follow() {
//   setState(() {
//     _requestingFollowing = true;
//   });
//   bool isFollowing = _profile.isFollowing;
//   try {
//     final saved =
//     UsersWebService().followUser(id: _profile.id, follow: !isFollowing);
//     setState(() {
//       isFollowing = !isFollowing;
//       if (isFollowing)
//         _profile.followers++;
//       else
//         _profile.followers--;
//       _profile.isFollowing = isFollowing;
//     });
//   } catch (e) {
//   } finally {
//     setState(() {
//       _requestingFollowing = false;
//     });
//   }
// }

  getUserLocation(double lt, lg) async {
    LocationData myLocation;
    String error;
    Location location = new Location();
    try {
      myLocation = await location.getLocation();
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        error = 'please grant permission';
        print(error);
      }
      if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        error = 'permission denied- please enable it from app settings';
        print(error);
      }
      myLocation = null;
    }
    final coordinates = new Coordinates(lt, lg);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    print(
        ' ${first.locality},, ${first.adminArea},,${first.subLocality},, ${first.subAdminArea},,${first.addressLine},, ${first.featureName},,${first.thoroughfare},, ${first.subThoroughfare}');
    setState(() {
      address1 = first.locality;
      address2 = first.subLocality;
      address3 = first.addressLine;
    });
    return first;
  }
}
