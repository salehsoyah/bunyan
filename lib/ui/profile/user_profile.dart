import 'dart:convert';
import 'dart:io';
import 'package:bunyan/localization/language/languages.dart';
import 'package:bunyan/localization/locale_constant.dart';
import 'package:bunyan/models/chat.dart';
import 'package:bunyan/models/enterprise.dart';
import 'package:bunyan/models/person.dart';
import 'package:bunyan/models/product_list.dart';
import 'package:bunyan/tools/res.dart';
import 'package:bunyan/tools/webservices/users.dart';
import 'package:bunyan/ui/chat/conversation_screen.dart';
import 'package:bunyan/ui/common/card_item.dart';
import 'package:bunyan/ui/onBoardingScreen.dart';
import 'package:bunyan/ui/product/product_screen.dart';
import 'package:clay_containers/widgets/clay_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:route_observer_mixin/route_observer_mixin.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:image_picker/image_picker.dart';

import '../actionButton.dart';
import '../profileInfo.dart';
import '../redirect_to_auth.dart';

class UserProfileScreen extends StatefulWidget {
  UserProfileScreen({Key key, this.profile, this.enterprise}) : super(key: key);

  final PersonModel profile;
  final EnterpriseModel enterprise;

  @override
  _UserProfileScreenState createState() => _UserProfileScreenState();
}

class _UserProfileScreenState extends State<UserProfileScreen>
    with RouteAware, RouteObserverMixin {
  List<ProductListModel> _products;
  bool isLoading;
  PersonModel _profile;
  bool _requestingFollowing = false;
  String _phone;
  String photoProfile;
  Locale currentLang;
  int _currentIndex = 0;

  @override
  void initState() {
    getCurrentUser();
    isLoading = true;
    getCurrentLang();
    super.initState();
    if (widget.profile != null) {
      _profile = PersonModel.fromJson(widget.profile.toJson());
    } else {
      _profile = Res.USER;
    }
    if (_profile != null) {
      _phone = _profile.phone?.replaceAll(' ', '') ?? '';
      _phone.replaceAll('+', '');
      if (_phone.startsWith('00')) _phone = _phone.replaceFirst('00', '');

      UsersWebService().getUserProfile(_profile.id).then((response) {
        setState(() {
          final oldProfile = PersonModel.fromJson(_profile.toJson());
          _profile = response['user'];
          photoProfile = _profile.photo;
          _products = [];
          _products.addAll(response['products']);
          _products.addAll(response['services']);

          if (oldProfile == Res.USER) {
            Res.USER = _profile;
            SharedPreferences.getInstance().then((prefs) {
              prefs.setString('user', jsonEncode(Res.USER.toJson()));
            });
          }
          isLoading = false;
        });
      });
    }
  }

  getCurrentLang() async {
    getLocale().then((locale) {
      setState(() {
        currentLang = locale;
      });
    });
  }

  getCurrentUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String user = prefs.getString('user');
    setState(() {
      _profile = PersonModel.fromJson(jsonDecode(user));
    });
  }

  logOut() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
    setState(() {
      Res.USER = null;
    });
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => OnBoardingScreen()));
  }

  @override
  void didPopNext() {
    Res.titleStream.add('الملف الشخصي');
    super.didPopNext();
  }

  @override
  void didPush() {
    super.didPush();
    Res.titleStream.add('الملف الشخصي');
  }

  File personalPhoto;
  _getFromGallery() async {
    var pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);
    setState(() {
      personalPhoto = File(pickedFile.path);
      uploadPhotoProfile(personalPhoto);
      pickedFile = null;
    });
  }

  _getFromCamera() async {
    var pickedFile = await ImagePicker().getImage(source: ImageSource.camera);
    setState(() {
      personalPhoto = File(pickedFile.path);
      uploadPhotoProfile(personalPhoto);
      pickedFile = null;
    });
  }

  uploadPhotoProfile(File photo) async {
    setState(() {
      isLoading = true;
    });
    UsersWebService()
        .updatePhotoProfile(idsession: _profile.id, profilePhoto: photo)
        .then((data) {
      setState(() {
        photoProfile = data['photo_url'];
        print('photoProfile $photoProfile');
        isLoading = false;
        if (data != null) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              elevation: 0,
              backgroundColor: Colors.black,
              behavior: SnackBarBehavior.floating,
              padding: EdgeInsets.all(0),
              content: Container(
                  padding: EdgeInsets.all(15),
                  child: Row(
                    children: [
                      Text(
                        Languages.of(context).editPhotoProfileSuccess,
                        style: GoogleFonts.cairo(
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                        ),
                      )
                    ],
                  )),
            ),
          );
          setState(() {
            initState();
            isLoading = false;
          });
        }
      });
    }).onError((error, stackTrace) {
      setState(() {
        isLoading = false;
      });
    });
  }

  void _showPicker(context) {
    showModalBottomSheet(
        backgroundColor: Colors.white,
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(35.0)),
        ),
        builder: (BuildContext bc) {
          Size size = MediaQuery.of(context).size;
          return SafeArea(
            child: Container(
              height: 140,
              padding: const EdgeInsets.symmetric(vertical: 20),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.vertical(top: Radius.circular(34.0)),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      _getFromGallery();
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      padding: const EdgeInsets.all(20),
                      width: size.width * 0.4,
                      decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(15)),
                      child: Column(
                        children: [
                          const Icon(
                            Icons.photo_library,
                            color: Colors.white,
                          ),
                          const SizedBox(height: 5),
                          Text(
                            Languages.of(context).modalBottomSheetPhotoLibrary,
                            style: const TextStyle(
                              fontWeight: FontWeight.w700,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      _getFromCamera();
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      padding: const EdgeInsets.all(20),
                      width: size.width * 0.4,
                      decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(15)),
                      child: Column(
                        children: [
                          const Icon(
                            Icons.photo_camera,
                            color: Colors.white,
                          ),
                          const SizedBox(height: 5),
                          Text(
                            Languages.of(context).modalBottomSheetCamera,
                            style: const TextStyle(
                              fontWeight: FontWeight.w700,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  Widget buttonIcon() {
    return Icon(
      Icons.chevron_right,
      color: Colors.grey,
      size: 30,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Res.USER != null
        ? Container(
      color: Color(0XFFeeeeee),
      child: NestedScrollView(
          headerSliverBuilder: (context, scrolled) {
            return [
              SliverAppBar(
                backgroundColor: Color(0XFFeeeeee),
                expandedHeight: .55.sh,
                //collapsedHeight: kToolbarHeight,
                automaticallyImplyLeading: false,
                flexibleSpace: LayoutBuilder(builder: (context, constraints) {
                  double scale = 1.0;
                  if (constraints.biggest.height <= 290)
                    scale = (constraints.biggest.height - 56) / (290 - 56.0);
                  return Stack(
                    children: [
                      FlexibleSpaceBar(
                        collapseMode: CollapseMode.parallax,
                        background: Padding(
                          padding: EdgeInsets.only(
                              right: 4, left: 4, bottom: .26.sh),
                          child: ClipRRect(
                              child: Hero(
                                tag: 'profile_picture',
                                child: _profile.photo != null
                                    ? TransitionToImage(
                                  image: AssetImage('assets/Image 280.jpg'),
                                  loadingWidget:
                                  _shimmer(width: 1.sw, height: 1.sw),
                                  placeholder: Container(
                                    width: 1.sw,
                                    height: 1.sw,
                                    child: Icon(Icons.broken_image_outlined),
                                    color: Colors.white,
                                  ),
                                  width: 1.sw,
                                  fit: BoxFit.cover,
                                )
                                    : Image(
                                  image: AssetImage('assets/Image 280.jpg'),
                                  fit: BoxFit.cover,
                                ),
                              )),
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: AnimatedOpacity(
                          opacity: scale,
                          duration: Duration(seconds: 0),
                          child: Transform.scale(
                            scale: scale,
                            origin: Offset(.0, .2.sh),
                            child: Card(
                              elevation: 3.0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                              child: Stack(
                                clipBehavior: Clip.none,
                                children: [
                                  Container(
                                      height: _profile.phone != null ? .37.sh : .30.sh,
                                      width: .85.sw,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius:
                                        BorderRadius.circular(20.0),
                                      ),
                                      padding: EdgeInsets.symmetric(
                                          vertical: 25.h, horizontal: 30.w),
                                      child: Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                        children: [
                                          SizedBox(height: 60),
                                          Text(
                                            _profile.name,
                                            style: GoogleFonts.cairo(
                                                fontWeight: FontWeight.w900,
                                                fontSize: 35.sp),
                                            textAlign: TextAlign.center,
                                          ),
                                          SizedBox(height: 10,),
                                          Divider(color: Colors.black),
                                          RichText(
                                            text: TextSpan(
                                              children: [
                                                WidgetSpan(
                                                  child: Icon(Icons.email_outlined, size: 30.sp),
                                                ),
                                                TextSpan(
                                                  text: "  "+_profile.email, style: GoogleFonts.cairo(
                                                    fontSize: 27.sp,color: Colors.black),
                                                ),
                                              ],
                                            ),
                                          ),
                                          _profile.phone != null ? Divider(color: Colors.black) : Container(),
                                          _profile.phone != null ? RichText(
                                            text: TextSpan(
                                              children: [
                                                WidgetSpan(
                                                  child: Icon(Icons.phone, size: 30.sp),
                                                ),
                                                TextSpan(
                                                  text: _profile.phone, style: GoogleFonts.cairo(
                                                    fontSize: 27.sp,color: Colors.black),
                                                ),
                                              ],
                                            ),
                                          ) : Container(),
                                          Divider(color: Colors.black)
                                        ],
                                      )),
                                  Positioned(
                                    bottom: -5,
                                    right: 0,
                                    left: 0,
                                    child: MaterialButton(
                                      onPressed: () {},
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          Languages.of(context).editprofile,
                                          style: TextStyle(
                                              color: Colors.lightBlue,
                                              fontSize: 17,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                      color: Colors.white,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                          BorderRadius.circular(10)),
                                    ),
                                  ),
                                  Positioned(
                                      top: -70,
                                      left: MediaQuery.of(context).size.width *
                                          0.5 -
                                          .075.sw -
                                          60,
                                      child: GestureDetector(
                                        onTap: () {
                                          _showPicker(context);
                                        },
                                        child: ProfileInfo(
                                            _profile.photo != null
                                                ? _profile.photo
                                                : 'http://sanjaymotels.com/wp-content/uploads/2019/01/testimony.png',
                                            true),
                                      )),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                }),
              )
            ];
          },
          body: Scaffold(
            body: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: 25,),
                  Align(
                    alignment: Alignment.center,
                    child: Card(
                      elevation: 3.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Stack(
                        clipBehavior: Clip.none,
                        children: [
                          Container(
                              width: .85.sw,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                BorderRadius.circular(20.0),
                              ),
                              padding: EdgeInsets.symmetric(
                                  vertical: 15.h, horizontal: 30.w),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Icon(Icons.info_outlined, size: 27.sp,),
                                  Text(
                                    Languages.of(context).about,
                                    style: GoogleFonts.cairo(
                                        fontSize: 27.sp),
                                    textAlign: TextAlign.center,
                                  ),
                                  Icon(Icons.arrow_forward_ios_sharp, size: 27.sp,)
                                ],
                              )),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 25,),
                  Align(
                    alignment: Alignment.center,
                    child: InkWell(
                      onTap: (){
                        logOut();
                      },
                      child: Card(
                        elevation: 3.0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)),
                        child: Stack(
                          clipBehavior: Clip.none,
                          children: [
                            Container(
                                width: .85.sw,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                  BorderRadius.circular(20.0),
                                ),
                                padding: EdgeInsets.symmetric(
                                    vertical: 15.h, horizontal: 30.w),
                                child: Text(
                                  Languages.of(context).logout,
                                  style: GoogleFonts.cairo(
                                      fontSize: 27.sp, fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center,
                                )),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
          )),
    ) :  RedirectToAuth(
      destination: 'favourites',
    );
  }

  Widget _contactBtn({String text, IconData icon, Color color, dynamic onTap}) {
    return InkWell(
      onTap: () {
        if (onTap != null) onTap();
      },
      child: Container(
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(5.0),
        ),
        padding: EdgeInsets.symmetric(vertical: 3.h, horizontal: 15.w),
        alignment: Alignment.center,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              text,
              style: GoogleFonts.cairo(color: Colors.white, fontSize: 23.sp),
            ),
            SizedBox(
              width: 12.w,
            ),
            Icon(
              icon,
              size: 27.sp,
              color: Colors.white,
            )
          ],
        ),
      ),
    );
  }

  _shimmerItem(index) {
    return Card(
      elevation: 2.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        textDirection: TextDirection.rtl,
        mainAxisSize: MainAxisSize.min,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: Stack(
              children: [
                Shimmer.fromColors(
                  baseColor: const Color(0xFFf3f3f3),
                  highlightColor: const Color(0xFFE8E8E8),
                  child: Container(
                    height: index.isOdd ? 380.h : 280.h,
                    width: double.infinity,
                    color: Colors.grey,
                  ),
                ),
                Positioned(
                  top: .0,
                  left: .0,
                  child: Shimmer.fromColors(
                    baseColor: Color(0xffbfbdbd),
                    highlightColor: const Color(0xFFE8E8E8),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(.8),
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(20.0))),
                      width: .25.sw,
                      height: 30.h,
                      padding:
                      EdgeInsets.symmetric(vertical: 5.w, horizontal: 25.w),
                    ),
                  ),
                ),
                Positioned.fill(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(3.0),
                      child: Container(
                        width: 1.sw,
                        alignment: Alignment.bottomRight,
                        child: Padding(
                          padding: EdgeInsets.only(
                              right: 20.w, bottom: 20.h, left: 15.w),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                child: Padding(
                                    padding: EdgeInsets.only(left: 10.w),
                                    child: Shimmer.fromColors(
                                      baseColor: Color(0xffbfbdbd),
                                      highlightColor: const Color(0xFFE8E8E8),
                                      child: Container(
                                        width: .1.sw,
                                        height: 20.h,
                                        color: Colors.grey.withOpacity(.8),
                                      ),
                                    )),
                              ),
                              Icon(
                                Icons.location_pin,
                                color: Colors.white,
                                size: 22.w,
                              ),
                              Shimmer.fromColors(
                                baseColor: Color(0xffbfbdbd),
                                highlightColor: const Color(0xFFE8E8E8),
                                child: Container(
                                  width: .08.sw,
                                  height: 10.h,
                                  color: Colors.grey.withOpacity(.8),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ))
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _shimmer({double width, double height}) {
    return Shimmer.fromColors(
        child: Container(
          width: width,
          height: height,
          color: Colors.grey,
        ),
        baseColor: Colors.grey.withOpacity(.5),
        highlightColor: Colors.white);
  }

  _follow() {
    setState(() {
      _requestingFollowing = true;
    });
    bool isFollowing = _profile.isFollowing;
    try {
      final saved =
      UsersWebService().followUser(id: _profile.id, follow: !isFollowing);
      setState(() {
        isFollowing = !isFollowing;
        if (isFollowing)
          _profile.followers++;
        else
          _profile.followers--;
        _profile.isFollowing = isFollowing;
      });
    } catch (e) {
    } finally {
      setState(() {
        _requestingFollowing = false;
      });
    }
  }
}
