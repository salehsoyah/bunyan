import 'package:bunyan/localization/language/languages.dart';
import 'package:bunyan/localization/locale_constant.dart';
import 'package:bunyan/models/banner.dart';
import 'package:bunyan/models/category.dart';
import 'package:bunyan/models/city.dart';
import 'package:bunyan/models/furnish.dart';
import 'package:bunyan/models/product_list.dart';
import 'package:bunyan/models/real_estate_filter.dart';
import 'package:bunyan/models/real_estate_type.dart';
import 'package:bunyan/models/region.dart';
import 'package:bunyan/tools/res.dart';
import 'package:bunyan/tools/webservices/addresses.dart';
import 'package:bunyan/tools/webservices/advertises.dart';
import 'package:bunyan/tools/webservices/products.dart';
import 'package:bunyan/ui/common/card_item.dart';
import 'package:bunyan/ui/common/premium_ads.dart';
import 'package:bunyan/ui/common/search_widget.dart';
import 'package:bunyan/ui/common/top_ad_banner.dart';
import 'package:bunyan/ui/main/main_screen.dart';
import 'package:bunyan/ui/notifications/notifications_screen.dart';
import 'package:bunyan/ui/product/product_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:route_observer_mixin/route_observer_mixin.dart';
import 'package:shimmer/shimmer.dart';

class RealEstatesScreen extends StatefulWidget {
  RealEstatesScreen({Key key}) : super(key: key);

  @override
  _RealEstatesScreenState createState() => _RealEstatesScreenState();
}

class _RealEstatesScreenState extends State<RealEstatesScreen>
    with TickerProviderStateMixin, RouteAware, RouteObserverMixin {
  bool _showFilter = false;
  bool _isFetching = false;
  List<ProductListModel> _realEstates = [];
  List<ProductListModel> _realEstatesSearch = [];

  List<ProductListModel> _top10 = [];
  List<BannerModel> _banners = [];
  List<CategoryModel> _cats = [];
  ScrollController _scrollController = ScrollController();
  RealEstateFilterModel _filter = RealEstateFilterModel();
  bool _stillFetch = true;
  bool isLoading = true;
  Locale currentLang;
  int _currentIndex = 0;
  List<RegionModel> regions = [];
  List<CityModel> cities = [];
  List<Furnish> furnishes = [
    Furnish(name: "Furnished", id: true),
    Furnish(name: "Unfurnished", id: false)
  ];

  int regionId;
  int cityId;
  String categoryId;
  int rooms;
  int baths;
  double priceFrom;
  double priceTo;
  bool furnished;

  GlobalKey<FormFieldState> _key = new GlobalKey();

  @override
  void initState() {
    AddressesWebService().getRegions().then((rgs) {
      setState(() {
        regions = rgs;
      });
      print(regions);
    });

    _stillFetch = true;
    isLoading = true;
    getCurrentLang();
    // ProductsWebService().getRealEstateTypes().then((value) {
    //   setState(() {
    //     Res.realEstateTypes = value;
    //   });
    // });
    super.initState();

    getLocale().then((locale) {
      setState(() {
        ProductsWebService().getCategories(locale.languageCode).then((value) {
          setState(() {
            _cats = value;
          });
        });
      });
    });

    // AdvertisesWebService().getBanners().then((value) {
    //   setState(() {
    //     _banners = value;
    //   });
    // });

    // ProductsWebService().getTop10().then((value) {
    //   setState(() {
    //     _top10.addAll(value);

    //   });
    // });

    Res.titleStream.add('عقارات');

    _filter.page = 1;

    _scrollController.addListener(_scrollListener);
  }

  getCities(int regionSelected) async {
    AddressesWebService().getCities(regionSelected).then((cts) {
      setState(() {
        Provider.of<CityBasket>(context, listen: false).affectCities(cts);
        cities = cts;
      });
      print(cities);
    });
  }

  getCurrentLang() async {
    getLocale().then((locale) {
      setState(() {
        currentLang = locale;
        getData();
        print('object ${currentLang.toString()}');
      });
    });
  }

  getData() async {
    print('object ${currentLang.toString()}');
    final futures = await Future.wait([
      ProductsWebService().getRealEstateTypes(),
      ProductsWebService().getCategories(currentLang.toString()),
      AdvertisesWebService().getBanners(),
      ProductsWebService().getTop10()
    ]);
    setState(() {
      Res.realEstateTypes = futures[0];
      _cats = futures[1];
      _banners = futures[2];
      _top10 = futures[3];
      isLoading = false;
    });
    _getData();
  }

  _scrollListener() {
    if (_scrollController.position.userScrollDirection ==
        ScrollDirection.reverse)
      Res.bottomNavBarAnimStream.add(false);
    else
      Res.bottomNavBarAnimStream.add(true);

    if (_scrollController.position.pixels <
            _scrollController.position.maxScrollExtent - 60.0 &&
        !_isFetching) {
      _filter.page++;
      _getData();
    }
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  void didPopNext() {
    Res.titleStream.add('عقارات');
    super.didPopNext();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            icon: const Icon(Icons.notifications_outlined),
            tooltip: 'Show Snackbar',
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => NotificationsScreen()));
            },
          ),
          PopupMenuButton<String>(
            icon: Icon(Icons.language_outlined),
            onSelected: (String result) {
              changeLanguage(context, result);
            },
            itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
              PopupMenuItem<String>(
                value: 'ar',
                child: Text('اَلْعَرَبِيَّةُ‎',
                    style: GoogleFonts.cairo(fontWeight: FontWeight.w700)),
              ),
              PopupMenuItem<String>(
                value: 'en',
                child: Text('English',
                    style: GoogleFonts.cairo(fontWeight: FontWeight.w700)),
              ),
            ],
          ),
        ],
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        elevation: 0,
        leading: InkWell(
          onTap: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => MainScreen(
                          menu_index: 0,
                        )));
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ClipOval(
              child: Image.asset(
                'assets/logo.min.png',
                height: 40,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        title: Text(
          Languages.of(context).realEstate,
          style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: WillPopScope(
        onWillPop: () async {
          if (_filter.type != null || _filter.category != null) {
            setState(() {
              _stillFetch = true;
              _filter.type = null;
              _filter.category = null;
              _filter.page = 1;
              _realEstates.clear();
            });
            _getData();
            return false;
          }
          if (_showFilter) {
            setState(() {
              _showFilter = false;
            });
            return false;
          }
          ;
          return true;
        },
        child: Container(
          child: SafeArea(
            child: Stack(
              children: [
                Container(
                  width: 1.sw,
                  child: CustomScrollView(
                      controller: _scrollController,
                      cacheExtent: 10000.0,
                      physics: AlwaysScrollableScrollPhysics(),
                      shrinkWrap: true,
                      slivers: [
                        SliverList(
                            delegate: SliverChildListDelegate([
                          Column(
                            textDirection: TextDirection.rtl,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              SizedBox(
                                height: 10.0,
                              ),
                              if (_banners.length > 0)
                                TopAdBanner(
                                  banners: _banners,
                                ),
                              // if (_filter.type == null)
                              //   Padding(
                              //       padding: EdgeInsets.only(
                              //           top: 5.h, left: .02.sw, right: .02.sw),
                              //       child: _types()),
                              Padding(
                                padding: EdgeInsets.only(top: 5.h),
                                child: AnimatedSize(
                                  duration: Duration(milliseconds: 300),
                                  curve: Curves.easeInOut,
                                  //height: !_showFilter ? .0 : .45.sh,
                                  vsync: this,
                                  child: Padding(
                                    padding: EdgeInsets.only(bottom: 10.h),
                                    child: _showFilter || _filter.type != null
                                        ? SearchWidget(
                                            showDropDown: false,
                                            onSearch: (filter) {
                                              _realEstates.clear();
                                              _stillFetch = true;
                                              filter.type = _filter.type;
                                              filter.page = 1;
                                              _filter = RealEstateFilterModel
                                                  .fromJson(filter.toJson());
                                              _getData();
                                            },
                                            filter: _filter,
                                          )
                                        : Container(),
                                  ),
                                ),
                              ),
                              if (_top10.isNotEmpty)
                                Padding(
                                  padding: EdgeInsets.only(bottom: 10.h),
                                  child: SizedBox(
                                    height: .15.sh,
                                    child: PremiumAds(
                                      ads: _top10,
                                    ),
                                  ),
                                ),
                            ],
                          ),
                        ])),

                        //categories
                        if (_filter.category == null)
                          SliverPadding(
                            padding: EdgeInsets.only(bottom: 7.h),
                            sliver: _categories(),
                          ),

                        SliverPadding(
                          padding: EdgeInsets.only(
                              right: .02.sw,
                              left: .02.sw,
                              bottom: _stillFetch ? .0 : 0.1.sh),
                          sliver: !_stillFetch &&
                                  !_isFetching &&
                                  _realEstates.isEmpty
                              ? SliverList(
                                  delegate: SliverChildListDelegate([
                                    Center(
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(
                                            vertical: .15.sh),
                                        child: Text(
                                          'لا يوجد عروض',
                                          style: GoogleFonts.cairo(
                                              color: Colors.grey,
                                              fontSize: 40.sp,
                                              fontWeight: FontWeight.w600),
                                        ),
                                      ),
                                    )
                                  ]),
                                )
                              : SliverStaggeredGrid.countBuilder(
                                  crossAxisCount: 2,
                                  itemCount: (_realEstatesSearch.length > 0 &&
                                          _realEstatesSearch.length <
                                              _realEstates.length)
                                      ? _realEstatesSearch.length
                                      : _realEstates.length,
                                  staggeredTileBuilder: (index) =>
                                      StaggeredTile.fit(1),
                                  itemBuilder: (context, index) {
                                    return InkWell(
                                        onTap: () => Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    ProductScreen(
                                                      product: (_realEstatesSearch
                                                                      .length >
                                                                  0 &&
                                                              _realEstatesSearch
                                                                      .length <
                                                                  _realEstates
                                                                      .length)
                                                          ? _realEstatesSearch[
                                                              index]
                                                          : _realEstates[index],
                                                    ))),
                                        child: CardItem(
                                            product: (_realEstatesSearch
                                                            .length >
                                                        0 &&
                                                    _realEstatesSearch.length <
                                                        _realEstates.length)
                                                ? _realEstatesSearch[index]
                                                : _realEstates[index]));
                                  },
                                ),
                        ),

                        if (_stillFetch)
                          SliverList(
                            delegate: SliverChildListDelegate([
                              Padding(
                                padding: EdgeInsets.only(top: 30.h),
                                child: _loadingWidget(),
                              )
                            ]),
                          )
                      ]),
                ),
                Positioned(
                    right: 10,
                    bottom: 10,
                    child: InkWell(
                      onTap: () {
                        showDialog(
                          barrierDismissible: false,
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              content: SingleChildScrollView(
                                child: Container(
                                  width: MediaQuery.of(context).size.width - 35,
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Container(
                                        padding: EdgeInsets.all(8),
                                        decoration: BoxDecoration(
                                            color: Colors.grey.withOpacity(0.4),
                                            borderRadius: BorderRadius.only(
                                                topRight: Radius.circular(5),
                                                topLeft: Radius.circular(5))),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            InkWell(
                                              onTap: () {
                                                Navigator.pop(context);
                                              },
                                              child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(15),
                                                child: Container(
                                                    padding: EdgeInsets.all(1),
                                                    color: Colors.green
                                                        .withOpacity(0.9),
                                                    child: Icon(
                                                      Icons.close,
                                                      color: Colors.white,
                                                    )),
                                              ),
                                            ),
                                            Text(
                                              Languages.of(context)
                                                  .advsearoption,
                                              style: TextStyle(
                                                  fontSize: 17,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Container(),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 12.0, right: 12.0, top: 20.0),
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.grey.withOpacity(0.4),
                                              borderRadius:
                                                  BorderRadius.circular(8)),
                                          child: DropdownButtonFormField(
                                            hint: Text(
                                              Languages.of(context).regions,
                                            ),
                                            isExpanded: true,
                                            onChanged: (value) {
                                              setState(() {
                                                regionId = value;
                                                _key.currentState.reset();
                                                getCities(value);
                                                print(value);
                                              });
                                            },
                                            onSaved: (value) {
                                              setState(() {});
                                            },
                                            items:
                                                regions.map((RegionModel val) {
                                              return DropdownMenuItem(
                                                value: val.id,
                                                child: Text(
                                                  val.name,
                                                ),
                                              );
                                            }).toList(),
                                            decoration: InputDecoration(
                                              floatingLabelBehavior:
                                                  FloatingLabelBehavior.always,
                                              border: InputBorder.none,
                                              contentPadding: EdgeInsets.only(
                                                  left: 10.0, right: 10.0),
                                            ),
                                            icon: Icon(
                                              Icons.keyboard_arrow_down_sharp,
                                              color: Colors.black,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 12.0, right: 12.0, top: 20.0),
                                        child: Container(
                                            decoration: BoxDecoration(
                                                color: Colors.grey
                                                    .withOpacity(0.4),
                                                borderRadius:
                                                    BorderRadius.circular(8)),
                                            child: DropdownButtonFormField(
                                              key: _key,
                                              hint: Text(
                                                Languages.of(context).cities,
                                              ),
                                              isExpanded: true,
                                              onChanged: (value) {
                                                setState(() {
                                                  cityId = value;
                                                  print(value);
                                                });
                                              },
                                              onSaved: (value) {
                                                setState(() {});
                                              },
                                              items: Provider.of<CityBasket>(
                                                      context)
                                                  .cities
                                                  .map((CityModel val) {
                                                return DropdownMenuItem(
                                                  value: val.id,
                                                  child: Text(
                                                    val.name,
                                                  ),
                                                );
                                              }).toList(),
                                              decoration: InputDecoration(
                                                floatingLabelBehavior:
                                                    FloatingLabelBehavior
                                                        .always,
                                                border: InputBorder.none,
                                                contentPadding: EdgeInsets.only(
                                                    left: 10.0, right: 10.0),
                                              ),
                                              icon: Icon(
                                                Icons.keyboard_arrow_down_sharp,
                                                color: Colors.black,
                                              ),
                                            )),
                                      ),
                                      // Padding(
                                      //   padding: const EdgeInsets.only(
                                      //       left: 12.0, right: 12.0, top: 20.0),
                                      //   child: Container(
                                      //     decoration: BoxDecoration(
                                      //         color:
                                      //             Colors.grey.withOpacity(0.4),
                                      //         borderRadius:
                                      //             BorderRadius.circular(8)),
                                      //     child: DropdownButtonFormField(
                                      //       hint: Text(
                                      //         'Categories',
                                      //       ),
                                      //       isExpanded: true,
                                      //       onChanged: (value) {
                                      //         setState(() {
                                      //           categoryId = value;
                                      //           print(value);
                                      //         });
                                      //       },
                                      //       items:
                                      //           _cats.map((CategoryModel val) {
                                      //         return DropdownMenuItem(
                                      //           value: val.id,
                                      //           child: Text(
                                      //             val.name,
                                      //           ),
                                      //         );
                                      //       }).toList(),
                                      //       decoration: InputDecoration(
                                      //         floatingLabelBehavior:
                                      //             FloatingLabelBehavior.always,
                                      //         border: InputBorder.none,
                                      //         contentPadding: EdgeInsets.only(
                                      //             left: 10.0, right: 10.0),
                                      //       ),
                                      //       icon: Icon(
                                      //         Icons.keyboard_arrow_down_sharp,
                                      //         color: Colors.black,
                                      //       ),
                                      //     ),
                                      //   ),
                                      // ),
                                      Row(
                                        children: [
                                          Expanded(
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 12.0,
                                                  right: 12.0,
                                                  top: 20.0),
                                              child: Container(
                                                decoration: BoxDecoration(
                                                    color: Colors.grey
                                                        .withOpacity(0.4),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8)),
                                                child: DropdownButtonFormField(
                                                  hint: Text(
                                                    Languages.of(context).rooms,
                                                  ),
                                                  isExpanded: true,
                                                  onChanged: (value) {
                                                    setState(() {
                                                      rooms = value;
                                                      print(value);
                                                    });
                                                  },
                                                  onSaved: (value) {
                                                    setState(() {});
                                                  },
                                                  items:
                                                      [1, 2, 3].map((int val) {
                                                    return DropdownMenuItem(
                                                      value: val,
                                                      child: Text(
                                                        val.toString(),
                                                      ),
                                                    );
                                                  }).toList(),
                                                  decoration: InputDecoration(
                                                    floatingLabelBehavior:
                                                        FloatingLabelBehavior
                                                            .always,
                                                    border: InputBorder.none,
                                                    contentPadding:
                                                        EdgeInsets.only(
                                                            left: 10.0,
                                                            right: 10.0),
                                                  ),
                                                  icon: Icon(
                                                    Icons
                                                        .keyboard_arrow_down_sharp,
                                                    color: Colors.black,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 12.0,
                                                  right: 12.0,
                                                  top: 20.0),
                                              child: Container(
                                                decoration: BoxDecoration(
                                                    color: Colors.grey
                                                        .withOpacity(0.4),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8)),
                                                child: DropdownButtonFormField(
                                                  hint: Text(
                                                    Languages.of(context).baths,
                                                  ),
                                                  isExpanded: true,
                                                  onChanged: (value) {
                                                    setState(() {
                                                      baths = value;
                                                      print(value);
                                                    });
                                                  },
                                                  onSaved: (value) {
                                                    setState(() {});
                                                  },
                                                  items:
                                                      [1, 2, 3].map((int val) {
                                                    return DropdownMenuItem(
                                                      value: val,
                                                      child: Text(
                                                        val.toString(),
                                                      ),
                                                    );
                                                  }).toList(),
                                                  decoration: InputDecoration(
                                                    floatingLabelBehavior:
                                                        FloatingLabelBehavior
                                                            .always,
                                                    border: InputBorder.none,
                                                    contentPadding:
                                                        EdgeInsets.only(
                                                            left: 10.0,
                                                            right: 10.0),
                                                  ),
                                                  icon: Icon(
                                                    Icons
                                                        .keyboard_arrow_down_sharp,
                                                    color: Colors.black,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 12.0, right: 12.0, top: 20.0),
                                        child: Container(
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(8)),
                                          child: Row(
                                            children: [
                                              Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width -
                                                    60,
                                                decoration: BoxDecoration(
                                                    color: Colors.grey
                                                        .withOpacity(0.4),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10)),
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 8.0,
                                                          right: 8.0),
                                                  child: Row(
                                                    children: [
                                                      Expanded(
                                                          flex: 1,
                                                          child: TextFormField(
                                                            enabled: false,
                                                            decoration: InputDecoration(
                                                                labelText:
                                                                    Languages.of(
                                                                            context)
                                                                        .price,
                                                                border:
                                                                    InputBorder
                                                                        .none),
                                                            onSaved:
                                                                (String value) {
                                                              // This optional block of code can be used to run
                                                              // code when the user saves the form.
                                                            },
                                                            validator:
                                                                (String value) {
                                                              return (value !=
                                                                          null &&
                                                                      value.contains(
                                                                          '@'))
                                                                  ? 'Do not use the @ char.'
                                                                  : null;
                                                            },
                                                          )),
                                                      Expanded(
                                                        flex: 2,
                                                        child: TextFormField(
                                                          onChanged: (value) {
                                                            if (value != '' &&
                                                                value != null) {
                                                              setState(() {
                                                                priceFrom =
                                                                    double.parse(
                                                                        value);
                                                              });
                                                            }
                                                          },
                                                          decoration: InputDecoration(
                                                              hintStyle: GoogleFonts.cairo(
                                                                  color: Colors
                                                                      .black38,
                                                                  fontSize:
                                                                      30.sp),
                                                              hintText:
                                                                  Languages.of(
                                                                          context)
                                                                      .from,
                                                              border:
                                                                  InputBorder
                                                                      .none),
                                                          onSaved:
                                                              (String value) {
                                                            // This optional block of code can be used to run
                                                            // code when the user saves the form.
                                                          },
                                                          validator:
                                                              (String value) {
                                                            return (value !=
                                                                        null &&
                                                                    value.contains(
                                                                        '@'))
                                                                ? 'Do not use the @ char.'
                                                                : null;
                                                          },
                                                        ),
                                                      ),
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            right: 10),
                                                        color: Colors.black45,
                                                        height: 50,
                                                        width: 2,
                                                      ),
                                                      Expanded(
                                                        flex: 3,
                                                        child: TextFormField(
                                                          onChanged: (value) {
                                                            if (value != '' &&
                                                                value != null) {
                                                              setState(() {
                                                                priceTo = double
                                                                    .parse(
                                                                        value);
                                                              });
                                                            }
                                                          },
                                                          decoration: InputDecoration(
                                                              labelText:
                                                                  Languages.of(
                                                                          context)
                                                                      .to,
                                                              border:
                                                                  InputBorder
                                                                      .none),
                                                          onSaved:
                                                              (String value) {
                                                            // This optional block of code can be used to run
                                                            // code when the user saves the form.
                                                          },
                                                          validator:
                                                              (String value) {
                                                            return (value !=
                                                                        null &&
                                                                    value.contains(
                                                                        '@'))
                                                                ? 'Do not use the @ char.'
                                                                : null;
                                                          },
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 12.0, right: 12.0, top: 20.0),
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.grey.withOpacity(0.4),
                                              borderRadius:
                                                  BorderRadius.circular(8)),
                                          child: DropdownButtonFormField(
                                            hint: Text(
                                              Languages.of(context).furnishing,
                                            ),
                                            isExpanded: true,
                                            onChanged: (value) {
                                              setState(() {
                                                furnished = value;
                                                print(value);
                                              });
                                            },
                                            onSaved: (value) {
                                              setState(() {});
                                            },
                                            items: furnishes.map((Furnish frn) {
                                              return DropdownMenuItem(
                                                value: frn.id,
                                                child: Text(
                                                  frn.name,
                                                ),
                                              );
                                            }).toList(),
                                            decoration: InputDecoration(
                                              floatingLabelBehavior:
                                                  FloatingLabelBehavior.always,
                                              border: InputBorder.none,
                                              contentPadding: EdgeInsets.only(
                                                  left: 10.0, right: 10.0),
                                            ),
                                            icon: Icon(
                                              Icons.keyboard_arrow_down_sharp,
                                              color: Colors.black,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              // Message which will be pop up on the screen
                              // Action widget which will provide the user to acknowledge the choice
                              actions: [
                                InkWell(
                                  onTap: () {
                                    Navigator.pop(context, {
                                      "regionId": regionId,
                                      "cityId": cityId,
                                      "categoryId": categoryId,
                                      "rooms": rooms,
                                      "baths": baths,
                                      "fromPrice": priceFrom,
                                      "toPrice": priceTo,
                                      "furnishing": furnished
                                    });
                                  },
                                  child: Container(
                                    padding: EdgeInsets.fromLTRB(
                                        15.0, 6.0, 15.0, 6.0),
                                    decoration: BoxDecoration(
                                        color: Colors.red,
                                        borderRadius:
                                            BorderRadius.circular(15)),
                                    child: Text(
                                      Languages.of(context).search,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                )
                              ],
                              contentPadding: EdgeInsets.all(0),
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0))),
                              insetPadding: EdgeInsets.zero,
                            );
                          },
                        ).then((value) {
                          print(value);
                          if (value != null) {
                            setState(() {
                              _realEstatesSearch = _realEstates;
                              if (value['furnishing'] != null) {
                                _realEstatesSearch = _realEstatesSearch
                                    .where((element) =>
                                        element.furnished ==
                                        value['furnishing'])
                                    .toList();
                              }
                              if (value['categoryId'] != null) {
                                _realEstatesSearch = _realEstatesSearch
                                    .where((element) =>
                                        element.category != null &&
                                        element.category.id ==
                                            value['categoryId'])
                                    .toList();
                              }
                              if (value['regionId'] != null) {
                                _realEstatesSearch = _realEstatesSearch
                                    .where((element) =>
                                        element.region != null &&
                                        element.region.id == value['regionId'])
                                    .toList();
                              }
                              if (value['cityId'] != null) {
                                _realEstatesSearch = _realEstatesSearch
                                    .where((element) =>
                                        element.city.id != null &&
                                        element.city.id == value['cityId'])
                                    .toList();
                              }

                              if (value['rooms'] != null) {
                                _realEstatesSearch = _realEstatesSearch
                                    .where((element) =>
                                        element.rooms != null &&
                                        element.rooms == value['rooms'])
                                    .toList();
                              }
                              if (value['baths'] != null) {
                                _realEstatesSearch = _realEstatesSearch
                                    .where((element) =>
                                        element.bathrooms != null &&
                                        element.bathrooms == value['baths'])
                                    .toList();
                              }
                              if (value['fromPrice'] != null) {
                                _realEstatesSearch = _realEstatesSearch
                                    .where((element) =>
                                        element.price != null &&
                                        element.price >= value['fromPrice'])
                                    .toList();
                              }
                              if (value['toPrice'] != null) {
                                _realEstatesSearch = _realEstatesSearch
                                    .where((element) =>
                                        element.price != null &&
                                        element.price <= value['toPrice'])
                                    .toList();
                              }

                              regionId = null;
                              categoryId = null;
                              rooms = null;
                              baths = null;
                              priceTo = null;
                              priceFrom = null;
                              furnished = null;
                              cityId = null;
                            });
                          }
                        });
                      },
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(40),
                          child: Container(
                              color: Colors.red.withOpacity(0.75),
                              child: Padding(
                                padding: EdgeInsets.all(20.h),
                                child: Icon(
                                  Icons.search_rounded,
                                  color: Colors.white,
                                  size: 30,
                                ),
                              ))),
                    ))
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 3.0,
        clipBehavior: Clip.antiAlias,
        child: Container(
          height: 65,
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: BottomNavigationBar(
                selectedLabelStyle:
                    GoogleFonts.cairo(fontWeight: FontWeight.w700),
                unselectedLabelStyle: GoogleFonts.cairo(),
                currentIndex: _currentIndex,
                backgroundColor: Colors.black,
                selectedItemColor: Colors.black,
                unselectedItemColor: Colors.grey,
                onTap: (index) {
                  print(index);
                  setState(() {
                    _currentIndex = index;
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MainScreen(
                                  menu_index: _currentIndex,
                                )));
                  });
                },
                items: [
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.home_outlined,
                    ),
                    activeIcon: Icon(Icons.home),
                    label: Languages.of(context).menuHome,
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.favorite_outline,
                    ),
                    activeIcon: Icon(Icons.favorite),
                    label: Languages.of(context).menuFavorite,
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      FontAwesome.comments,
                      color: Colors.transparent,
                    ),
                    label: '',
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.chat_outlined,
                    ),
                    activeIcon: Icon(Icons.chat),
                    label: Languages.of(context).productDetailsCallChat,
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.person_outline,
                    ),
                    activeIcon: Icon(Icons.person),
                    label: Languages.of(context).menuProfile,
                  ),
                ]),
          ),
        ),
      ),
      floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterDocked,
      floatingActionButton: MediaQuery.of(context).viewInsets.bottom == 0
          ? FloatingActionButton(
              mini: true,
              backgroundColor: Colors.black,
              child: Icon(FontAwesome.plus),
              onPressed: () => setState(() {}),
            )
          : Container(),
    );
  }

  Widget _loadingWidget() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        CupertinoActivityIndicator(
          radius: 20.sp,
        ),
        SizedBox(width: 20.w),
        Text(
          Languages.of(context).loader,
          style: GoogleFonts.cairo(
              color: Colors.grey, fontSize: 30.sp, fontWeight: FontWeight.w600),
        )
      ],
    );
  }

  Widget _types() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: !isLoading
          ? Row(
              //textDirection: TextDirection.rtl,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                for (final type in Res.realEstateTypes)
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10.w),
                    child: InkWell(
                      onTap: () {
                        _filter.type = type;
                        _filter.page = 1;
                        _realEstates.clear();
                        setState(() {
                          _stillFetch = true;
                        });
                        _getData();
                      },
                      child: Chip(
                        label: Text(
                          type.name,
                          style: GoogleFonts.cairo(),
                        ),
                        labelPadding: EdgeInsets.symmetric(horizontal: .04.sw),
                      ),
                    ),
                  ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.w),
                  child: InkWell(
                    onTap: () => setState(() {
                      FocusNode().requestFocus();
                      _showFilter = !_showFilter;
                    }),
                    child: Chip(
                        label: Text(
                          'فيلتر',
                          style: GoogleFonts.cairo(
                              color: _showFilter ? Colors.white : Colors.black),
                        ),
                        backgroundColor: _showFilter ? Colors.red : null,
                        labelPadding: EdgeInsets.symmetric(horizontal: .04.sw)),
                  ),
                )
              ],
            )
          : Container(),
    );
  }

  _shimmerItem(index) {
    return Card(
      elevation: 2.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        textDirection: TextDirection.rtl,
        mainAxisSize: MainAxisSize.min,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: Stack(
              children: [
                Shimmer.fromColors(
                  baseColor: const Color(0xFFf3f3f3),
                  highlightColor: const Color(0xFFE8E8E8),
                  child: Container(
                    height: index.isOdd ? 380.h : 280.h,
                    width: double.infinity,
                    color: Colors.grey,
                  ),
                ),
                Positioned(
                  top: .0,
                  left: .0,
                  child: Shimmer.fromColors(
                    baseColor: Color(0xffbfbdbd),
                    highlightColor: const Color(0xFFE8E8E8),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(.8),
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(20.0))),
                      width: .25.sw,
                      height: 30.h,
                      padding:
                          EdgeInsets.symmetric(vertical: 5.w, horizontal: 25.w),
                    ),
                  ),
                ),
                Positioned.fill(
                    child: ClipRRect(
                  borderRadius: BorderRadius.circular(3.0),
                  child: Container(
                    width: 1.sw,
                    alignment: Alignment.bottomRight,
                    child: Padding(
                      padding: EdgeInsets.only(
                          right: 20.w, bottom: 20.h, left: 15.w),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Padding(
                                padding: EdgeInsets.only(left: 10.w),
                                child: Shimmer.fromColors(
                                  baseColor: Color(0xffbfbdbd),
                                  highlightColor: const Color(0xFFE8E8E8),
                                  child: Container(
                                    width: .1.sw,
                                    height: 20.h,
                                    color: Colors.grey.withOpacity(.8),
                                  ),
                                )),
                          ),
                          Icon(
                            Icons.location_pin,
                            color: Colors.white,
                            size: 22.w,
                          ),
                          Shimmer.fromColors(
                            baseColor: Color(0xffbfbdbd),
                            highlightColor: const Color(0xFFE8E8E8),
                            child: Container(
                              width: .08.sw,
                              height: 10.h,
                              color: Colors.grey.withOpacity(.8),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ))
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _categories() {
    return SliverList(
      delegate: SliverChildListDelegate([
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: _cats
                  .map((cat) => InkWell(
                        onTap: () {
                          setState(() {
                            _stillFetch = true;
                            _filter.page = 1;
                            _filter.category = cat;
                            _realEstates.clear();
                          });
                          _getData();
                        },
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 3.w),
                          child: Container(
                              width: 135.w,
                              height: 135.w,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                shape: BoxShape.rectangle,
                                borderRadius: BorderRadius.circular(5),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black.withOpacity(0.5),
                                    offset: const Offset(0.0, 5.0), //(x,y)
                                    blurRadius: 3.0,
                                  ),
                                ],
                              ),
                              child: Stack(
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(5),
                                        topRight: Radius.circular(5)),
                                    child: TransitionToImage(
                                      image: AdvancedNetworkImage(
                                          cat.photo ?? '',
                                          timeoutDuration:
                                              Duration(minutes: 3)),
                                      loadingWidget:
                                          _shimmer(width: 135.w, height: 100.w),
                                      fit: BoxFit.cover,
                                      width: 135.w,
                                      height: 100.w,
                                    ),
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      gradient: LinearGradient(
                                        begin: Alignment.topCenter,
                                        end: Alignment.bottomCenter,
                                        colors: [
                                           Colors.white.withOpacity(0.3),
                                          Colors.white.withOpacity(0.3),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Text(
                                      cat.name,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black, fontSize: 16.w),
                                      textAlign: TextAlign.center,
                                    ),
                                  )
                                ],
                              )),
                        ),
                      ))
                  .toList(),
            ),
          ),
        )
      ]),
    );
  }

  Widget _shimmer({double width, double height}) {
    return Shimmer.fromColors(
      child: Container(
        width: width,
        height: height,
        color: Colors.grey,
      ),
      baseColor: const Color(0xFFf3f3f3),
      highlightColor: const Color(0xFFE8E8E8),
    );
  }

  void _getData() {
    if (_stillFetch && !_isFetching) {
      setState(() {
        _isFetching = true;
      });
      ProductsWebService().getHomeProducts(filter: _filter).then((value) {
        setState(() {
          _realEstates.addAll(value);
          _isFetching = false;
          if (value.length < Res.PAGE_SIZE) _stillFetch = false;
        });
      }).catchError((err) {
        _isFetching = false;
        _stillFetch = false;
      });
    }
  }
}

class CityBasket extends ChangeNotifier {
  List<CityModel> cities = [];

  void affectCities(List<CityModel> citiesSelected) {
    cities = citiesSelected;
    notifyListeners();
  }

  void addCity(CityModel city) {
    cities.add(city);
    notifyListeners();
  }
}
