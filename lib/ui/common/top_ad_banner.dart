import 'package:bunyan/models/banner.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shimmer/shimmer.dart';

class TopAdBanner extends StatelessWidget {
  final List<BannerModel> banners;

  const TopAdBanner({Key key, this.banners}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CarouselSlider.builder(
      itemCount: banners.length,
      itemBuilder: (context, index, realIndex) => Container(
        width: 1.sw,
        height: .18.sh,
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(10.0),
            child: TransitionToImage(
              image: AdvancedNetworkImage(
                banners[index].photo,
              ),
              fit: BoxFit.cover,
              placeholder: Container(
                width: double.infinity,
                height: double.infinity,
                child: Icon(
                  Icons.broken_image_outlined,
                  size: 35.sp,
                ),
              ),
              loadingWidget: Shimmer.fromColors(
                  child: Container(
                    width: double.infinity,
                    height: double.infinity,
                    color: Colors.red,
                  ),
                  baseColor: const Color(0xFFf3f3f3),
                  highlightColor: Colors.white),
            ),
          ),
        ),
      ),
      options: CarouselOptions(
        height: .18.sh,
        enlargeCenterPage: true,
        viewportFraction: .98,
        autoPlay: true,
      ),
    );
  }
}
