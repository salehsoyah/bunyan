import 'package:bunyan/ui/product/product_screen.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shimmer/shimmer.dart';

class PremiumAds extends StatelessWidget {
  final List ads;

  const PremiumAds({Key key, this.ads}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("ads $ads");
    return ads.isNotEmpty
        ? CarouselSlider.builder(
            itemCount: ads?.length ?? 0,
            itemBuilder: (context, index, realIndex) => InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ProductScreen(
                              product: ads[index],
                            )));
              },
              child: Container(
                width: 1.sw,
                height: .14.sh,
                child: Card(
                  elevation: 3.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10.0),
                    child: TransitionToImage(
                      image: AdvancedNetworkImage(ads[index].photos[0],
                          postProcessing: (list) {
                        return FlutterImageCompress.compressWithList(list,
                            quality: 20, format: CompressFormat.jpeg);
                      }),
                      loadingWidget: Shimmer.fromColors(
                          child: Container(
                            //width: double.infinity,
                            color: Colors.grey,
                            height: double.infinity,
                          ),
                          baseColor: Colors.grey.withOpacity(.5),
                          highlightColor: Colors.white),
                      width: .15.sw,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ),
            options: CarouselOptions(
                height: .15.sh,
                viewportFraction: .5.w,
                autoPlay: true,
                enableInfiniteScroll: true,
                autoPlayInterval: Duration(milliseconds: 2000),
                autoPlayAnimationDuration: Duration(milliseconds: 800)),
          )
        : Container();
  }
}
