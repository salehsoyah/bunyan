import 'dart:math';

import 'package:bunyan/localization/locale_constant.dart';
import 'package:bunyan/models/product_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shimmer/shimmer.dart';

class CardItem extends StatefulWidget {
  final ProductListModel product;

  const CardItem({Key key, this.product}) : super(key: key);

  @override
  State<CardItem> createState() => _CardItemState();
}

class _CardItemState extends State<CardItem> {
  Locale currentLang;

  @override
  void initState() {
    getCurrentLang();
    super.initState();
  }

  getCurrentLang() async {
    getLocale().then((locale) {
      currentLang = locale;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 3.sp, vertical: 3.sp),
      decoration: BoxDecoration(
        color: Colors.transparent,
        borderRadius: BorderRadius.all(Radius.circular(15)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        textDirection: TextDirection.rtl,
        mainAxisSize: MainAxisSize.min,
        children: [
          Stack(
            children: [
              ConstrainedBox(
                constraints:
                    BoxConstraints(minHeight: 180.h, minWidth: double.infinity),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  child: TransitionToImage(
                    image: AdvancedNetworkImage(
                      widget.product.photos?.first ??
                          widget.product.photos ??
                          '',
                      cacheRule: CacheRule(maxAge: Duration(days: 20)),
                      useDiskCache: true,
                      /*postProcessing: (list) {
                      return FlutterImageCompress.compressWithList(list,
                          keepExif: true,
                          quality: 20, format: CompressFormat.jpeg);
                    }*/
                    ),
                    fit: BoxFit.cover,
                    width: double.infinity,
                    placeholder: Icon(Icons.broken_image_outlined),
                    loadingWidget: Shimmer.fromColors(
                        child: Container(
                          //width: double.infinity,
                          height: Random().nextInt(1) == 1 ? 480.h : 320.h,
                          color: Colors.grey,
                        ),
                        baseColor: Colors.grey.withOpacity(.5),
                        highlightColor: Colors.white),
                  ),
                ),
              ),
              /*Image.network(
                product.image,
                fit: BoxFit.cover,
                //height: index.isOdd ? 380.h : 280.h,
                width: double.infinity,
              ),*/
            ],
          ),
          Container(
            width: 1.sw,
            alignment: Alignment.bottomRight,
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 7),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Icon(
                                  Icons.visibility_outlined,
                                  size: 16,
                                  color: Colors.black,
                                ),
                                SizedBox(width: 3),
                                Text(
                                  widget.product.views ?? '',
                                  style: TextStyle(
                                      fontSize: 10,
                                      fontWeight: FontWeight.w700,
                                      color: Colors.black),
                                ),
                              ],
                            ),
                            Icon(Icons.more_horiz_sharp)
                          ],
                        ),

                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        border: Border.all(color: Colors.grey),
                                        color: Colors.white10,
                                        borderRadius: BorderRadius.circular(30),
                                        image: DecorationImage(
                                          image: NetworkImage(widget
                                                      .product.enterprise ==
                                                  null
                                              ? (widget.product.owner.photo != null)
                                                  ? widget.product.owner.photo
                                                  : widget.product.enterprise.photo
                                              : 'http://sanjaymotels.com/wp-content/uploads/2019/01/testimony.png'),
                                          fit: BoxFit.cover,
                                        )),
                                    height: 32,
                                    width: 32,
                                  ),
                                  const SizedBox(width: 5),
                                  ConstrainedBox(
                                    constraints: BoxConstraints(
                                      maxWidth:
                                          MediaQuery.of(context).size.width / 2 -
                                              80,
                                    ),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          widget.product.title,
                                          maxLines: 1,
                                          //stepGranularity: 1.0,
                                          overflow: TextOverflow.ellipsis,
                                          style: GoogleFonts.cairo(
                                              fontWeight: FontWeight.w700,
                                              color: Colors.black,
                                              fontSize: 12,
                                              height: 1.2),
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            ConstrainedBox(
                                              constraints: BoxConstraints(
                                                maxWidth: MediaQuery.of(context)
                                                            .size
                                                            .width /
                                                        2 -
                                                    120,
                                              ),
                                              child: Text(
                                                widget.product.enterprise != null
                                                    ? widget.product.enterprise.name
                                                    : widget.product.owner.name,
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontSize: 8,
                                                    fontWeight: FontWeight.w700,
                                                    color: Colors.black87),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ]),
                      ],
                    ),
                  ),
                  // if (product.adr != null)
                  //   Icon(
                  //     Icons.location_pin,
                  //     color: Colors.white,
                  //     size: 22.w,
                  //   ),
                  // if (product.adr != null)
                  //   Text(
                  //     product.adr.name ?? 'N/A',
                  //     style: GoogleFonts.cairo(
                  //         fontSize: 17.sp,
                  //         fontWeight: FontWeight.bold,
                  //         color: Colors.white),
                  //   ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  String formatDecimal(double n) {
    return n.toStringAsFixed(n.truncateToDouble() == n ? 0 : 1);
  }
}
