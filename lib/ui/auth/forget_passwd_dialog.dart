import 'package:bunyan/tools/webservices/users.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

class ForgetPasswdDialog extends StatefulWidget {
  ForgetPasswdDialog({Key key}) : super(key: key);

  @override
  _ForgetPasswdDialogState createState() => _ForgetPasswdDialogState();
}

class _ForgetPasswdDialogState extends State<ForgetPasswdDialog> {
  int _step = 0;
  TextEditingController textController1 = TextEditingController();
  TextEditingController textController2 = TextEditingController();
  bool _isRequesting = false;
  String _email;
  String _code;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 18.0, horizontal: 20.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(_step == 0 ? 'البريد الإلكتروني' : _step == 1 ? 'الكود المرسل للإيميل' : 'كلمة السر الجديدة', style: GoogleFonts.cairo(fontWeight: FontWeight.bold, fontSize: 15.0),),
          SizedBox(height: 15.0,),
          TextFormField(
            controller: textController1,
            keyboardType: _step == 0 ? TextInputType.emailAddress : _step == 1 ? TextInputType.number : TextInputType.text,
            obscureText: _step == 2,
            decoration: InputDecoration(
              labelText: _step == 0 ?  'الإيميل' : _step == 1 ? 'الكود' : 'كلمة السر',
            ),
          ),
          if (_step == 2)
            SizedBox(height: 8.0,),
          if (_step == 2)
            TextFormField(
              obscureText: true,
              controller: textController2,
              decoration: InputDecoration(
                labelText: 'تأكيد كلمة السر',
              ),
            ),
          SizedBox(height: 15.0,),
          
          TextButton(onPressed: _isRequesting ? null : _next, child: Text('التالي', style: GoogleFonts.cairo(fontWeight: FontWeight.w600),))
        ],
      ),
    );
  }

  Future<void> _next() async {
    if (_step == 0 && textController1.text.isNotEmpty) {
      setState(() {
        _isRequesting = true;
      });
      final response = await UsersWebService().forgetPasswd(textController1.text);
      if (response) {
        FocusScope.of(context).unfocus();
        setState(() {
          _step++;
          _email = textController1.text;
          textController1.text = '';
        });
      }
      else
        _showDialog('الإيميل غير متوفر');

      setState(() {
        _isRequesting = false;
      });
    } else if (_step == 1 && textController1.text.isNotEmpty) {
      setState(() {
        _isRequesting = true;
      });
      final response = await UsersWebService().checkForgetPasswdCode(mail: _email, code: textController1.text);
      if (response) {
        FocusScope.of(context).unfocus();
        setState(() {
          _step++;
          _code = textController1.text;
          textController1.text = '';
        });
      }
      else
        _showDialog('الكود الذي ادخلته غير صحيح');

      setState(() {
        _isRequesting = false;
      });
    } else if (_step == 2 && textController1.text.isNotEmpty && textController2.text.isNotEmpty) {
      
      if (textController2.text != textController1.text) {
        _showDialog('كلمة السر غير مطابقة');
        return;
      }

      if (textController1.text.length < 6) {
        _showDialog('ككلمة السر متكونة من 6 احرف على الاقل');
        return;
      }
      
      setState(() {
        _isRequesting = true;
      });
      final response = await UsersWebService().updateForgetPasswdCode(
          mail: _email, passwd: textController1.text, code: _code);
      if (response)
        setState(() {
          Navigator.pop(context);
        });
      else
        _showDialog('خطأ عند تغيير كلمة المرور');

      setState(() {
        _isRequesting = false;
      });
    }
  }

  void _showDialog(String text) {
    showDialog(context: context, builder: (context) => AlertDialog(
      content: Text(text),
      title: Text('خطأ'),
      actions: [
        TextButton(
            onPressed: () => Navigator.pop(context),
            child: Text(
              'موافق',
              style: GoogleFonts.cairo(color: Colors.teal),
            ))
      ],
    ));
  }
}