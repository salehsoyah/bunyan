import 'dart:convert';

import 'package:bunyan/exceptions/unauthorized.dart';
import 'package:bunyan/localization/language/languages.dart';
import 'package:bunyan/tools/res.dart';
import 'package:bunyan/tools/webservices/users.dart';
import 'package:bunyan/ui/auth/forget_passwd_dialog.dart';
import 'package:bunyan/ui/auth/signup/signup_entr_screen.dart';
import 'package:bunyan/ui/favorites/favorites_screen.dart';
import 'package:bunyan/ui/main/main_screen.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:location/location.dart';
import 'package:permission_handler/permission_handler.dart';
import 'signup/signup_screen.dart';
import 'package:bunyan/ui/common/location_picker.dart';
import 'package:bunyan/models/position.dart';
import 'package:location/location.dart';
class LoginScreen extends StatefulWidget {
  final String destination;


  LoginScreen({Key key, this.destination}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final _mailController = TextEditingController();
  final _passwdController = TextEditingController();
  final _passwdNode = FocusNode();
  bool _isRequesting = false;
  bool _accepted = true;
  bool _showAcceptError = false;
  PositionModel _position;
  //getCurrentLocation();
  LocationData mycurrentLocation;
  bool _isLoadingLocation=false;
  @override
  void didChangeDependencies() async {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Colors.white,
        statusBarIconBrightness: Brightness.dark,
      ),
    );
  }




  getCurrentLocation() async {
    final location = Location();
    mycurrentLocation = await location.getLocation();
    setState(() {
      _position = PositionModel(
          lat: mycurrentLocation.latitude, lng: mycurrentLocation.longitude);
      print('_position ${_position.toJson()}');
      _isLoadingLocation = false;
    });
  }
  void initState() {

    _isLoadingLocation = true;
    getCurrentLocation();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Colors.white,
        statusBarIconBrightness: Brightness.dark,
      ),
    );
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 80.w),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Hero(
                      tag: 'logo',
                      child: Image.asset(
                        'assets/logo.min.png',
                        width: .3.sw,
                        fit: BoxFit.cover,
                      )),
                  SizedBox(
                    height: 30.h,
                  ),
                  TextFormField(
                    enabled: !_isRequesting,
                    keyboardType: TextInputType.emailAddress,
                    controller: _mailController,
                    textInputAction: TextInputAction.next,
                    validator: (txt) => txt.isEmpty || !txt.contains('@')
                        ? Languages.of(context).emptyValidator
                        : null,
                    onFieldSubmitted: (txt) => _passwdNode.requestFocus(),
                    textDirection: TextDirection.ltr,
                    decoration: InputDecoration(
                      labelStyle: GoogleFonts.cairo(),
                      labelText: Languages.of(context).email,
                      isDense: true,
                      contentPadding: EdgeInsets.symmetric(
                          vertical: 17.h, horizontal: 15.w),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      disabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      focusedErrorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                    ),
                  ),
                  SizedBox(
                    height: 30.h,
                  ),
                  TextFormField(
                    enabled: !_isRequesting,
                    keyboardType: TextInputType.text,
                    autocorrect: false,
                    validator: (txt) => txt.isEmpty
                        ? Languages.of(context).emptyValidator
                        : null,
                    textDirection: TextDirection.ltr,
                    obscureText: true,
                    controller: _passwdController,
                    textInputAction: TextInputAction.go,
                    onFieldSubmitted: (txt) => _login(),
                    decoration: InputDecoration(
                      labelStyle: GoogleFonts.cairo(),
                      labelText: Languages.of(context).loginPassword,
                      isDense: true,
                      contentPadding: EdgeInsets.symmetric(
                          vertical: 17.h, horizontal: 15.w),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      disabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      focusedErrorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                    ),
                  ),
                  SizedBox(
                    height: 20.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      InkWell(
                        onTap: _openForgetPasswdDialog,
                        child: Text(
                          Languages.of(context).loginForgetPwd,
                          style: GoogleFonts.cairo(
                              color: Colors.grey,
                              fontSize: 15,
                              fontWeight: FontWeight.w600,
                              decoration: TextDecoration.underline),
                        ),
                      ),
                    ],
                  ),
                  /*Align(
                    alignment: Alignment.centerRight,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Checkbox(
                          onChanged: (bool value) {
                            setState(() {
                              _accepted = value;
                            });
                          },
                          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                          value: _accepted,
                          visualDensity: VisualDensity.compact,
                        ),
                        RichText(
                          text: TextSpan(children: [
                            TextSpan(
                                text: 'اوافق على ',
                                style: GoogleFonts.cairo(
                                  color: Colors.blue,
                                  fontSize: 20.sp,
                                )),
                            TextSpan(
                                text: 'سياسة الخصوصية ',
                                style: GoogleFonts.cairo(
                                    color: Colors.blue,
                                    fontSize: 20.sp,
                                    decoration: TextDecoration.underline)),
                            TextSpan(
                                text: 'و ',
                                style: GoogleFonts.cairo(
                                  color: Colors.blue,
                                  fontSize: 20.sp,
                                )),
                            TextSpan(
                                text: 'شروط الاستخدام ',
                                style: GoogleFonts.cairo(
                                    color: Colors.blue,
                                    fontSize: 20.sp,
                                    decoration: TextDecoration.underline)),
                          ]),
                        ),
                      ],
                    ),
                  ),*/
                  if (_showAcceptError)
                    Align(
                        alignment: Alignment.centerRight,
                        child: Padding(
                          padding: EdgeInsets.only(right: 18.w),
                          child: Text(
                            'الرجاء الموافقة',
                            style: GoogleFonts.cairo(
                                color: Colors.red, fontSize: 15.sp),
                          ),
                        )),
                  SizedBox(height: 50.h),
                  MaterialButton(
                    height: 45,
                    onPressed: _login,
                    child: !_isRequesting
                        ? Text(
                            Languages.of(context).loginSignIn,
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w900),
                          )
                        : const SizedBox(
                            height: 24,
                            width: 24,
                            child: CircularProgressIndicator(
                              strokeWidth: 2,
                              color: Colors.white,
                            ),
                          ),
                    color: Colors.black,
                    mouseCursor: MouseCursor.defer,
                    textColor: Colors.white,
                    minWidth: double.infinity,
                  ),
                  SizedBox(height: 20),
                  Container(
                    alignment: Alignment.center,
                    child: GestureDetector(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            Languages.of(context).loginAccount,
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                            ),
                          ),
                          SizedBox(width: 5),
                          Text(
                            Languages.of(context).signUp,
                            style: TextStyle(
                              decoration: TextDecoration.underline,
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                        ],
                      ),
                      onTap: () => _signUp(),
                    ),
                  ),
                  SizedBox(height: 10),
                  TextButton(
                    child: Text(
                      Languages.of(context).loginAsGuest,
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                    style: TextButton.styleFrom(
                      primary: Colors.blue,
                      onSurface: Colors.grey,
                    ),
                    onPressed: () {
                      //addon
                      Res.USER = null;
                      setState(() {
                        _isRequesting = false;
                      });
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MainScreen()));
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _login() async {
    setState(() {
      _showAcceptError = false;
    });
    if (_formKey.currentState.validate() && _accepted) {
      setState(() {
        _isRequesting = true;
      });
      try {
        final user = await UsersWebService()
            .login(mail: _mailController.text, passwd: _passwdController.text);
        Res.USER = user;
        final prefs = await SharedPreferences.getInstance();
        await prefs.setString('user', jsonEncode(user.toJson()));
        if (widget.destination == 'home') {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => MainScreen()));
        }
        if (widget.destination == 'favourites') {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => MainScreen(
                        showVerifMail: false,
                        menu_index: 1,
                      )));
        }
        if (widget.destination == 'ads') {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => MainScreen(
                        showVerifMail: false,
                        menu_index: 2,
                      )));
        }
        if (widget.destination == 'chats') {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => MainScreen(
                        showVerifMail: false,
                        menu_index: 3,
                      )));
        }

        if (widget.destination == 'profile') {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => MainScreen(
                        showVerifMail: false,
                        menu_index: 4,
                      )));
        }
        if (widget.destination == null) {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => MainScreen(
                        showVerifMail: false,
                        menu_index: 0,
                      )));
        }
      } catch (err) {
        setState(() {
          _isRequesting = false;
        });
        if (err is UnauthorizedException)
          _showDialog(text: Languages.of(context).chekconnection);
        else if (err is DioError) _showDialog();
      }
    } else if (!_accepted)
      setState(() {
        _showAcceptError = true;
      });
  }

  void _showDialog({String text = ""}) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              content: Text(
                Languages.of(context).makesureoftheinformation,
                style: GoogleFonts.cairo(color: Colors.black),
              ),
              actions: [
                FlatButton(
                    onPressed: () => Navigator.pop(context),
                    child: Text(Languages.of(context).back,
                      style: GoogleFonts.cairo(color: Colors.blue),
                    ))
              ],
            ));
  }

  _signUp() {
    if (!_isRequesting) {
      showDialog(
          context: context,
          builder: (context) => GestureDetector(
                onTap: () => print(''),
                child: Dialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  child: SizedBox(
                    //width: .8.sw,
                    child: Container(
                      padding: EdgeInsets.only(
                          top: 40.h, left: 60.w, right: 60.w, bottom: 25.h),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            Languages.of(context).register,
                            textAlign: TextAlign.start,
                            style: GoogleFonts.cairo(
                              color: Colors.black,
                              fontSize: 35.sp,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          SizedBox(
                            height: 35.h,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 30),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                _dialogBtn(
                                    text:
                                        Languages.of(context).registerAsCompany,
                                    icon: 'register_company',
                                    onTap: () {
                                      Navigator.pop(context);
                                      Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  SignupEntrScreen()));
                                    }),
                                Expanded(
                                  child: Container(),
                                ),
                                _dialogBtn(
                                    icon: 'register_personal',
                                    text:
                                        Languages.of(context).registerAsPerson,
                                    onTap: () {
                                      Navigator.pop(context);
                                      Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  SignupScreen()));
                                    })
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ));
    }
  }

  Widget _dialogBtn(
      {@required String icon,
      @required String text,
      @required Function onTap}) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(
              'assets/icons/$icon.svg',
              width: 48,
            ),
            Text(
              text,
              style: GoogleFonts.cairo(
                  color: Colors.black,
                  fontSize: 20.sp,
                  fontWeight: FontWeight.w700),
            ),
          ],
        ),
      ),
    );
  }

  void _openForgetPasswdDialog() {
    showDialog(
        builder: (BuildContext context) => Dialog(
              child: ForgetPasswdDialog(),
            ),
        context: context);
  }
}
