import 'dart:async';

import 'package:bunyan/localization/language/languages.dart';
import 'package:bunyan/ui/main/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class MailVerificationScreen extends StatefulWidget {
  MailVerificationScreen({Key key}) : super(key: key);

  @override
  _MailVerificationScreenState createState() => _MailVerificationScreenState();
}

class _MailVerificationScreenState extends State<MailVerificationScreen> {

  StreamController<ErrorAnimationType> _errorController = StreamController<ErrorAnimationType>();
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: WillPopScope(
        onWillPop: () {
          if (_isLoading) {
            setState(() {
              _isLoading = false;
            });
            return Future.value(false);
          }
          return Future.value(true);
        },
        child: Container(
          width: 1.sw,
          padding: EdgeInsets.symmetric(vertical: 20.h, horizontal: 40.w),
          child: IndexedStack(
            index: _isLoading ? 1 : 0,
            children: [
              Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    Languages.of(context).activateacount,
                    style: GoogleFonts.cairo(
                        fontSize: 40.sp, fontWeight: FontWeight.w500),
                  ),
                  Text(
                    Languages.of(context).pleasegoemail,
                    style: GoogleFonts.cairo(
                        fontSize: 25.sp, fontWeight: FontWeight.w500),
                    textAlign: TextAlign.center,
                  ),
                  Directionality(
                    textDirection: TextDirection.ltr,
                    child: PinCodeTextField(
                        appContext: context,
                      length: 5,
                      textInputAction: TextInputAction.go,
                      //onChanged: _checkCode,
                      keyboardType: TextInputType.number,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      obscureText: false,
                      cursorColor: Colors.black,
                      onSubmitted: _checkCode,
                      onCompleted: _checkCode,
                      autoDisposeControllers: true,
                      errorAnimationController: _errorController,
                      cursorWidth: .0,
                      textStyle: GoogleFonts.cairo(fontSize: 20.sp),
                      pinTheme: PinTheme(shape: PinCodeFieldShape.underline),
                      onChanged: (String value) {  },
                    ),
                  )
                ],
              ),
              Positioned.fill(child: Center(
                child: Wrap(
                  children: [
                    SizedBox(width: 100.w, height: 100.w,child: CircularProgressIndicator())
                  ],
                ),
              ))
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _checkCode(String code) async {
    setState(() {
      _isLoading = true;
    });
    await Future.delayed(Duration(seconds: 2));
    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => MainScreen(showVerifMail: false,)), (route) => false);
  }
}
