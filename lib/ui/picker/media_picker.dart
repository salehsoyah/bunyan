import 'dart:io';
import 'dart:typed_data';

import 'package:bunyan/localization/language/languages.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

class MediaPicker {
  static final _picker = ImagePicker();
  static bool clickable = true;

  static Future<MediaItem> getMedia(BuildContext context) async {
    final data = await showDialog<MediaItem>(
        barrierDismissible: !clickable,
        context: context,
        builder: (context) => WillPopScope(
              onWillPop: () async {
                return !clickable;
              },
              child: Dialog(
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 50.sp),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(right: 30.sp, bottom: 50.sp),
                        child: Text(
                          Languages.of(context).pickertitle,
                          style: GoogleFonts.cairo(
                              fontSize: 30.sp,
                              fontWeight: FontWeight.w900,
                              color: Colors.black),
                        ),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          InkWell(
                            onTap: !clickable
                                ? null
                                : () async {
                                    final data =
                                        await _getFiles(pickVideo: true);
                                    Navigator.pop(context, data);
                                  },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    color:
                                        clickable ? Colors.blue : Colors.grey,
                                    shape: BoxShape.circle,
                                  ),
                                  padding: EdgeInsets.all(30.sp),
                                  child: Icon(
                                    Icons.videocam_sharp,
                                    color: Colors.white,
                                  ),
                                ),
                                Text(
                                  Languages.of(context).adVideo,
                                  style: GoogleFonts.cairo(
                                      color:
                                          clickable ? Colors.blue : Colors.grey,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                          InkWell(
                            onTap: !clickable
                                ? null
                                : () async {
                                    final data =
                                        await _getFiles(pickVideo: false);
                                    Navigator.pop(context, data);
                                  },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    color:
                                        clickable ? Colors.blue : Colors.grey,
                                    shape: BoxShape.circle,
                                  ),
                                  padding: EdgeInsets.all(30.sp),
                                  child: Icon(
                                    Icons.camera_alt,
                                    color: Colors.white,
                                  ),
                                ),
                                Text(
                                  Languages.of(context).adPhoto,
                                  style: GoogleFonts.cairo(
                                      color:
                                          clickable ? Colors.blue : Colors.grey,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ));
    clickable = true;
    return data;
  }

  static Future<MediaItem> _getFiles({bool pickVideo = false}) async {
    clickable = false;
    PickedFile file;
    if (pickVideo)
      file = await _picker.getVideo(source: ImageSource.gallery);
    else
      file = await _picker.getImage(source: ImageSource.gallery);
    if (file != null) {
      final thumb = pickVideo
          ? await VideoThumbnail.thumbnailData(video: file.path)
          : await file.readAsBytes();
      return MediaItem(thumb, !pickVideo, File(file.path));
    }
    return null;
    /*_assets = await PhotoPicker.pickAsset(
        context: context,
        pickedAssetList: _assets,
        pickType: PickType.onlyImage,
        maxSelected: 8,
        provider: I18nProvider.arabic);
    _photos.clear();
    for (final asset in _assets) {
      final index = _photos.length;
      _photos.add(null);
      final thumb = await asset
          .thumbDataWithSize(600, 600);
      final file = await asset.file;

      setState(() {
        _photos.removeAt(index);
        _photos.insert(
            index,
            _MediaItem(
                thumb,
                asset.type == AssetType.image,
                file));
      });
    };*/
  }
}

class MediaItem {
  final Uint8List thumb;
  final bool isPhoto;
  final File file;

  MediaItem(this.thumb, this.isPhoto, this.file);
}
