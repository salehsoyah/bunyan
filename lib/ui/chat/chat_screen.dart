import 'package:bunyan/localization/language/languages.dart';
import 'package:bunyan/models/chat.dart';
import 'package:bunyan/tools/res.dart';
import 'package:bunyan/tools/webservices/users.dart';
import 'package:bunyan/ui/chat/conversation_screen.dart';
import 'package:clay_containers/clay_containers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:loading_animations/loading_animations.dart';
import 'package:route_observer_mixin/route_observer_mixin.dart';
import 'package:shimmer/shimmer.dart';

import '../redirect_to_auth.dart';

class ChatScreen extends StatefulWidget {
  ChatScreen({Key key}) : super(key: key);

  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<ChatScreen> with RouteAware, RouteObserverMixin {
  List<ChatModel> _chats;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Res.titleStream.add('الدردشة');

    UsersWebService().getChats().then((value) {
      setState(() {
        _chats = value;
      });
    });
  }

  @override
  void didPopNext() {
    Res.titleStream.add('الدردشة');
    super.didPopNext();
  }

  @override
  Widget build(BuildContext context) {
    print(_chats == null
        ? 2
        : _chats.isEmpty
            ? 0
            : 1);
    return Res.USER != null
        ? Stack(
            //index: _chats == null ? 2 : _chats.isEmpty ? 0 : 1,
            children: [
              if (_chats == null)
                Center(
                  child: LoadingBouncingGrid.square(
                    backgroundColor: Color(0xffd6d6d6),
                  ),
                )
              else if (_chats.isEmpty)
                Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(
                        Icons.mode_comment_outlined,
                        color: Color(0xffd6d6d6),
                        size: 90.sp,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: ClayText(
                          Languages.of(context).noChats,
                          style: GoogleFonts.cairo(
                            fontSize: 40.sp,
                            fontWeight: FontWeight.bold,
                          ),
                          depth: -10,
                          textColor: Color(0xffd6d6d6),
                        ),
                      ),
                    ],
                  ),
                )
              else
                ListView.builder(
                  padding:
                      EdgeInsets.symmetric(horizontal: 15.h, vertical: 30.h),
                  itemCount: _chats.length,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ConversationScreen(
                                    chat: _chats[index],
                                    senderName: _chats[index].sender.name,
                                  ))),
                      child: Stack(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(right: .045.sh),
                            child: Card(
                              elevation: 5.0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10.0),
                                child: Container(
                                  height: .15.sh,
                                  width: double.infinity,
                                  padding: EdgeInsets.symmetric(vertical: 15.h),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        width: .07.sh,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            '${_chats[index].sender.name} ارسل لك رسالة',
                                            style: GoogleFonts.cairo(
                                                fontSize: 25.sp,
                                                color: Colors.black),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.symmetric(
                                              horizontal: 22.w,
                                            ),
                                            child: Text(
                                              _chats[index]
                                                  .date
                                                  .toIso8601String(),
                                              style: GoogleFonts.cairo(
                                                  color: Colors.grey,
                                                  fontSize: 20.sp),
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            right: .0,
                            child: SizedBox(
                              height: .15.sh,
                              child: Center(
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(500.0)),
                                  elevation: 3.0,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(500.0),
                                    child: TransitionToImage(
                                        image: AdvancedNetworkImage(
                                            _chats[index].sender.photo),
                                        loadingWidget: _shimmer(
                                            width: .1.sh, height: .1.sh),
                                        placeholder: Container(
                                          width: .1.sh,
                                          height: .1.sh,
                                          child:
                                              Icon(Icons.broken_image_outlined),
                                          color: Colors.white,
                                        ),
                                        height: .1.sh,
                                        fit: BoxFit.cover,
                                        width: .1.sh),
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    );
                  },
                ),
            ],
          )
        : RedirectToAuth(
            destination: 'chats',
          );
  }

  Widget _shimmer({double width, double height}) {
    return Shimmer.fromColors(
        child: Container(
          width: width,
          height: height,
          color: Colors.grey,
        ),
        baseColor: Colors.grey.withOpacity(.5),
        highlightColor: Colors.white);
  }
}
