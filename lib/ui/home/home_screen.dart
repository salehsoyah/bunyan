

import 'package:bunyan/localization/language/languages.dart';
import 'package:bunyan/localization/locale_constant.dart';
import 'package:bunyan/models/banner.dart';
import 'package:bunyan/models/product.dart';

import 'package:bunyan/models/real_estate_filter.dart';
import 'package:bunyan/tools/res.dart';
import 'package:bunyan/tools/webservices/advertises.dart';
import 'package:bunyan/tools/webservices/products.dart';
import 'package:bunyan/ui/common/card_item.dart';
import 'package:bunyan/ui/common/premium_ads.dart';
import 'package:bunyan/ui/common/top_ad_banner.dart';
import 'package:bunyan/ui/enterprises/enterprises_screen.dart';
import 'package:bunyan/ui/news/news_screen.dart';
import 'package:bunyan/ui/product/product_screen.dart';
import 'package:bunyan/ui/real_estates/real_estates_screen.dart';
import 'package:bunyan/ui/services/services_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:route_observer_mixin/route_observer_mixin.dart';
import 'package:shimmer/shimmer.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with RouteAware, RouteObserverMixin {
  List<ProductModel> _products = [];
  List<BannerModel> _banners = [];

  bool _isFetching = true;
  ScrollController _scrollController = ScrollController();
  RealEstateFilterModel _filter = RealEstateFilterModel();
  bool isLoadingSearch = false;
  Locale _locale;
  @override
  void initState() {
    _isFetching = true;
    getCurrentLang();
    super.initState();
    getData();
    _scrollController.addListener(_scrollListener);
  }

  getData() async {
    final futures = await Future.wait([
      ProductsWebService().getHomeProducts(),
      AdvertisesWebService().getBanners(),
    ]);
    setState(() {
      _products = futures[0];
      _banners = futures[1];
      _isFetching = false;
    });
  }

  getCurrentLang() async {
    getLocale().then((locale) {
      setState(() {
        _locale = locale;
      });
    });
  }

  _scrollListener() {
    if (_scrollController.position.userScrollDirection ==
        ScrollDirection.reverse)
      Res.bottomNavBarAnimStream.add(false);
    else
      Res.bottomNavBarAnimStream.add(true);
  }

  @override
  void didPop() {
    Res.bottomNavBarAnimStream.add(true);
    super.didPop();
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.removeListener(_scrollListener);
  }

  void _filterData() {
    setState(() {
      isLoadingSearch = true;
    });
    ProductsWebService().getHomeProducts(filter: _filter).then((value) {
      setState(() {
        _products.removeRange(0, _products.length);
        _products.addAll(value);

        isLoadingSearch = false;
      });
    }).catchError((err) {
      isLoadingSearch = false;
    });
  }

  @override
  void didPopNext() {
    print('pop');
    Res.titleStream.add('الرئيسية');
    super.didPopNext();
  }

  @override
  void didPush() {
    super.didPush();
    Res.titleStream.add('الرئيسية');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // floatingActionButton: FloatingActionButton(
        //   onPressed: () {
        //   },
        //   child: const Icon(Icons.headphones),
        //   backgroundColor: Colors.green,
        // ),
        body: CustomScrollView(
           slivers: [
        SliverList(
            delegate: SliverChildListDelegate([
          Column(
            textDirection: TextDirection.rtl,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              if ((_banners ?? []).isNotEmpty)
                TopAdBanner(
                  banners: _banners,
                ),
              Padding(
                padding: EdgeInsets.only(bottom: 20.h),
                child: PremiumAds(
                  ads: _products,
                ),
              ),

              Padding(
                  padding: EdgeInsets.symmetric(vertical: .02.sw),
                  child: _categoriesList()),
              _searchWidget(),

            ],
          ),
        ]
            ,)
        ,
        ),
        SliverPadding(
          padding: EdgeInsets.only(
              top: 0, right: .02.sw, left: .02.sw, bottom: 0.1.sh),
          sliver: SliverStaggeredGrid.countBuilder(
            crossAxisCount: 2,
            itemCount: _isFetching ? 4 : _products.length,
            staggeredTileBuilder: (index) => StaggeredTile.fit(1),
            crossAxisSpacing: .0,
            itemBuilder: (context, index) {
              // print(' _products[index] ${_products[index].toJson()}');
              return _isFetching
                  ? _shimmerItem(index)
                  : _products.length > 0
                      ? InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ProductScreen(
                                          product: _products[index],
                                        )));
                            Res.titleStream.add('عقارات');
                          },
                          child: CardItem(product: _products[index]),
                        )
                      : Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Text(
                            Languages.of(context).redirectToAuthMessage,
                            style: GoogleFonts.cairo(
                              fontSize: 20,
                              fontWeight: FontWeight.w700,
                              color: Colors.black,
                            ),
                          ),
                        );
            },
          ),
        ),
      ],

    ),

    );
  }

  Widget _categoriesList() {
    return SizedBox(
      width: double.infinity,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            InkWell(
              onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => RealEstatesScreen())),
              child: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.grey[300],
                ),
                width: 110,
                height: 35,
                child: Text(
                  Languages.of(context).realEstate,
                  style: GoogleFonts.cairo(fontWeight: FontWeight.w700),
                ),
                padding: EdgeInsets.symmetric(horizontal: .02.sw),
                margin: EdgeInsets.symmetric(horizontal: .02.sw),
              ),
            ),
            InkWell(
              onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => ServicesScreen())),
              child: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.grey[300],
                ),
                width: 110,
                height: 35,
                child: Text(
                  Languages.of(context).services,
                  style: GoogleFonts.cairo(fontWeight: FontWeight.w700),
                ),
                padding: EdgeInsets.symmetric(horizontal: .02.sw),
                margin: EdgeInsets.symmetric(horizontal: .02.sw),
              ),
            ),
            InkWell(
              onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => EnterprisesScreen())),
              child: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.grey[300],
                ),
                width: 110,
                height: 35,
                child: Text(
                  Languages.of(context).agencies,
                  style: GoogleFonts.cairo(fontWeight: FontWeight.w700),
                ),
                padding: EdgeInsets.symmetric(horizontal: .02.sw),
                margin: EdgeInsets.symmetric(horizontal: .02.sw),
              ),
            ),
            InkWell(
              onTap: () => Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => NewsScreen())),
              child: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.grey[300],
                ),
                width: 110,
                height: 35,
                child: Text(
                  Languages.of(context).news,
                  style: GoogleFonts.cairo(fontWeight: FontWeight.w700),
                ),
                padding: EdgeInsets.symmetric(horizontal: .02.sw),
                margin: EdgeInsets.symmetric(horizontal: .02.sw),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _shimmerItem(index) {
    return Card(
      elevation: 2.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        textDirection: TextDirection.rtl,
        mainAxisSize: MainAxisSize.min,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: Stack(
              children: [
                Shimmer.fromColors(
                  baseColor: const Color(0xFFf3f3f3),
                  highlightColor: const Color(0xFFE8E8E8),
                  child: Container(
                    height: index.isOdd ? 380.h : 280.h,
                    width: double.infinity,
                    color: Colors.grey,
                  ),
                ),
                Positioned(
                  top: .0,
                  left: .0,
                  child: Shimmer.fromColors(
                    baseColor: Color(0xffbfbdbd),
                    highlightColor: const Color(0xFFE8E8E8),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(.8),
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(20.0))),
                      width: .25.sw,
                      height: 30.h,
                      padding:
                          EdgeInsets.symmetric(vertical: 5.w, horizontal: 25.w),
                    ),
                  ),
                ),
                Positioned.fill(
                    child: ClipRRect(
                  borderRadius: BorderRadius.circular(3.0),
                  child: Container(
                    width: 1.sw,
                    alignment: Alignment.bottomRight,
                    child: Padding(
                      padding: EdgeInsets.only(
                          right: 20.w, bottom: 20.h, left: 15.w),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Padding(
                                padding: EdgeInsets.only(left: 10.w),
                                child: Shimmer.fromColors(
                                  baseColor: Color(0xffbfbdbd),
                                  highlightColor: const Color(0xFFE8E8E8),
                                  child: Container(
                                    width: .1.sw,
                                    height: 20.h,
                                    color: Colors.grey.withOpacity(.8),
                                  ),
                                )),
                          ),
                          Icon(
                            Icons.location_pin,
                            color: Colors.white,
                            size: 22.w,
                          ),
                          Shimmer.fromColors(
                            baseColor: Color(0xffbfbdbd),
                            highlightColor: const Color(0xFFE8E8E8),
                            child: Container(
                              width: .08.sw,
                              height: 10.h,
                              color: Colors.grey.withOpacity(.8),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ))
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _searchWidget() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
      child: SizedBox(
        height: 40,
        child: TextField(
          onChanged: (value) => _filter.adTitle = value,
          cursorHeight: 26,
          cursorColor: Colors.black,
          enabled: true,
          style: GoogleFonts.cairo(fontSize: 26.sp),
          decoration: InputDecoration(
              border: InputBorder.none,
              focusedErrorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Colors.black)),
              disabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Colors.black)),
              errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Colors.red)),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Colors.black)),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Colors.black)),
              suffixIcon: GestureDetector(
                onTap: _filterData,
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: (Radius.circular(
                          _locale.toString() == 'ar' ? 10 : 0)),
                      bottomLeft: (Radius.circular(
                          _locale.toString() == 'ar' ? 10 : 0)),
                      topRight: (Radius.circular(
                          _locale.toString() == 'ar' ? 0 : 10)),
                      bottomRight: (Radius.circular(
                          _locale.toString() == 'ar' ? 0 : 10)),
                    ),
                    color: Colors.black,
                  ),
                  child: !isLoadingSearch
                      ? Icon(
                          Icons.search,
                          color: Colors.white,
                          size: 24,
                        )
                      : const SizedBox(
                          height: 20,
                          width: 20,
                          child: CircularProgressIndicator(
                            strokeWidth: 2,
                            color: Colors.white,
                          ),
                        ),
                ),
              ),
              suffixIconConstraints:
                  BoxConstraints(minWidth: 60, minHeight: 40, maxWidth: 60),
              contentPadding: EdgeInsets.only(top: 0, left: 20.w, right: 20),
              hintStyle:
                  GoogleFonts.cairo(color: Colors.black38, fontSize: 30.sp),
              hintText: Languages.of(context).searchPlaceholder),
        ),
      ),
    );
  }
}
