import 'dart:convert';

import 'package:bunyan/localization/locale_constant.dart';
import 'package:bunyan/models/person.dart';
import 'package:bunyan/tools/res.dart';
import 'package:bunyan/tools/webservices/products.dart';
import 'package:bunyan/ui/main/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'auth/login_screen.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool _gotMisc = false;
  bool _timeFinished = false;
  Locale currentLang;

  @override
  void initState() {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Colors.white,
        statusBarIconBrightness: Brightness.dark,
      ),
    );
    getCurrentLang();

    super.initState();
  }

  getCurrentLang() async {
    getLocale().then((locale) {
      setState(() {
        currentLang = locale;
        Future.delayed(Duration(seconds: 1)).then((_) => {
              _getMisc().then((value) => {if (_gotMisc) _goToScreen()})
            });
      });
    });
  }

  // @override
  // void dispose() {
  //   SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
  //   SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Image.asset(
        'assets/splash.gif',
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        fit: BoxFit.cover,
      ),
    );
  }

  Future<void> _getMisc() async {
    final futures = await Future.wait([
      ProductsWebService().getCategories(currentLang.toString()),
      // AddressesWebService().getRegions(),
      // ProductsWebService().getRealEstateTypes(),
      // ProductsWebService().getServicesCategories()
    ]);
    Res.catgories = futures[0];
    // Res.regions = futures[1];
    // Res.realEstateTypes = futures[2];
    // Res.servicesCategories = futures[3];
    // Res.realEstateTypes.forEach((element) {
    //   print(element.toJson());
    // });
    // for (RegionModel region in Res.regions) {
    //   region.cities = await AddressesWebService().getCities(region.id);
    // }
    _gotMisc = true;
    if (_timeFinished) _goToScreen();
  }

  Future<void> _goToScreen() async {
    final prefs = await SharedPreferences.getInstance();
    final user = await prefs.getString(('user'));
    if (user != null) Res.USER = PersonModel.fromJson(jsonDecode(user));
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => Res.USER != null
                ? MainScreen(
                    showVerifMail: false,
                  )
                : LoginScreen()));
  }
}
