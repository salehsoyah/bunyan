import 'package:bunyan/localization/language/languages.dart';
import 'package:bunyan/localization/locale_constant.dart';
import 'package:bunyan/models/news.dart';
import 'package:bunyan/tools/res.dart';
import 'package:bunyan/tools/webservices/products.dart';
import 'package:bunyan/ui/main/main_screen.dart';
import 'package:bunyan/ui/news/article_screen.dart';
import 'package:bunyan/ui/notifications/notifications_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:route_observer_mixin/route_observer_mixin.dart';
import 'package:shimmer/shimmer.dart';

class NewsScreen extends StatefulWidget {
  NewsScreen({Key key}) : super(key: key);

  @override
  _NewsScreenState createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen>
    with RouteAware, RouteObserverMixin {
  ScrollController _scrollController = ScrollController();

  List<NewsModel> _news = [];
  bool _isFetching = true;
  int _currentIndex = 0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Res.titleStream.add("اخبار");

    ProductsWebService().getNews().then((value) {
      setState(() {
        _news = value;
        _isFetching = false;
      });
    });

    _scrollController.addListener(_scrollListener);
  }

  _scrollListener() {
    if (_scrollController.position.userScrollDirection ==
        ScrollDirection.reverse) {
      Res.bottomNavBarAnimStream.add(false);
    } else {
      Res.bottomNavBarAnimStream.add(true);
    }
  }

  @override
  void didPop() {
    Res.bottomNavBarAnimStream.add(true);
    super.didPop();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _scrollController.removeListener(_scrollListener);
  }

  @override
  void didPopNext() {
    Res.titleStream.add("اخبار");
    super.didPopNext();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            icon: const Icon(Icons.notifications_outlined),
            tooltip: 'Show Snackbar',
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => NotificationsScreen()));
            },
          ),
          PopupMenuButton<String>(
            icon: Icon(Icons.language_outlined),
            onSelected: (String result) {
              changeLanguage(context, result);
            },
            itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
              PopupMenuItem<String>(
                value: 'ar',
                child: Text('اَلْعَرَبِيَّةُ‎',
                    style: GoogleFonts.cairo(fontWeight: FontWeight.w700)),
              ),
              PopupMenuItem<String>(
                value: 'en',
                child: Text('English',
                    style: GoogleFonts.cairo(fontWeight: FontWeight.w700)),
              ),
            ],
          ),
        ],
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        elevation: 0,
        leading: InkWell(
          onTap: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => MainScreen(
                      menu_index: 0,
                    )));
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ClipOval(
              child: Image.asset(
                'assets/logo.min.png',
                height: 40,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        title: Text(
          Languages.of(context).news,
          style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,        automaticallyImplyLeading: false,
      ),
      body: SafeArea(
        child: ListView.builder(
            controller: _scrollController,
            itemCount: _isFetching ? 5 : _news.length,
            itemBuilder: (context, index) => _isFetching
                ? _shimmerCard()
                : InkWell(
                    onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ArticleScreen(
                                  news: _news[index],
                                ))),
                    child: Hero(
                      tag: _news[index],
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)),
                        elevation: 5.0,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              TransitionToImage(
                                image: AdvancedNetworkImage(_news[index].photo,
                                    timeoutDuration: Duration(minutes: 3),
                                    retryLimit: 5),
                                loadingWidget: _shimmerBox(
                                  height: .3.sh,
                                ),
                                placeholder: Container(
                                  height: .3.sh,
                                  width: double.infinity,
                                  color: Colors.grey.shade200,
                                  child: Icon(Icons.broken_image_outlined),
                                ),
                                height: .3.sh,
                                fit: BoxFit.cover,
                                width: double.infinity,
                              ),
                              SizedBox(
                                height: 20.h,
                              ),
                              Text(
                                _news[index].title,
                                style: GoogleFonts.cairo(
                                    fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center,
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    vertical: 20.h, horizontal: 10.w),
                                child: Text(
                                  '${_news[index].content}...',
                                  style: GoogleFonts.cairo(),
                                  textAlign: TextAlign.center,
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 40.h),
                                child: Text(
                                  'اقرأ المزيد',
                                  style: GoogleFonts.cairo(color: Colors.teal),
                                  textAlign: TextAlign.center,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  )),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 3.0,
        clipBehavior: Clip.antiAlias,
        child: Container(
          height: 65,
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: BottomNavigationBar(
                selectedLabelStyle:
                GoogleFonts.cairo(fontWeight: FontWeight.w700),
                unselectedLabelStyle: GoogleFonts.cairo(),
                currentIndex: _currentIndex,
                backgroundColor: Colors.black,
                selectedItemColor: Colors.black,
                unselectedItemColor: Colors.grey,
                onTap: (index) {
                  print(index);
                  setState(() {
                    _currentIndex = index;
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MainScreen(
                              menu_index: _currentIndex,
                            )));
                  });
                },
                items: [
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.home_outlined,
                    ),
                    activeIcon: Icon(Icons.home),
                    label: Languages.of(context).menuHome,
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.favorite_outline,
                    ),
                    activeIcon: Icon(Icons.favorite),
                    label: Languages.of(context).menuFavorite,
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      FontAwesome.comments,
                      color: Colors.transparent,
                    ),
                    label: '',
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.chat_outlined,
                    ),
                    activeIcon: Icon(Icons.chat),
                    label: Languages.of(context).productDetailsCallChat,
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.person_outline,
                    ),
                    activeIcon: Icon(Icons.person),
                    label: Languages.of(context).menuProfile,
                  ),
                ]),
          ),
        ),
      ),
      floatingActionButtonLocation:
      FloatingActionButtonLocation.miniCenterDocked,
      floatingActionButton: MediaQuery.of(context).viewInsets.bottom == 0
          ? FloatingActionButton(
        mini: true,
        backgroundColor: Colors.black,
        child: Icon(FontAwesome.plus),
        onPressed: () => setState(() {
        }),
      )
          : Container(),
    );
  }

  Widget _shimmerCard() {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      elevation: 5.0,
      child: Column(
        children: [
          ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: _shimmerBox(width: double.infinity, height: .3.sh)),
          SizedBox(
            height: 20.h,
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 8.h),
            child: _shimmerBox(width: .8.sw, height: 15.h),
          ),
          Center(child: _shimmerBox(width: .3.sw, height: 15.h)),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 20.h, horizontal: 10.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              textDirection: TextDirection.ltr,
              children: List<int>.generate(5, (index) => index).map((e) {
                return Padding(
                  padding: EdgeInsets.only(bottom: 8.h),
                  child: _shimmerBox(
                      width: e < 4 ? double.infinity : .2.sw, height: 8.h),
                );
              }).toList(),
            ),
          ),
          SizedBox(
            height: 50.h,
          )
        ],
      ),
    );
  }

  Widget _shimmerBox({double width, double height}) {
    return Shimmer.fromColors(
      baseColor: const Color(0xFFf3f3f3),
      highlightColor: const Color(0xFFE8E8E8),
      child: Container(
        height: height,
        width: width,
        color: Colors.grey,
      ),
    );
  }
}
