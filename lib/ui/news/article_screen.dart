import 'package:bunyan/models/news.dart';
import 'package:bunyan/tools/res.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:route_observer_mixin/route_observer_mixin.dart';
import 'package:shimmer/shimmer.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ArticleScreen extends StatefulWidget {
  final NewsModel news;

  ArticleScreen({Key key, this.news}) : super(key: key);

  @override
  _ArticleScreenState createState() => _ArticleScreenState();
}

class _ArticleScreenState extends State<ArticleScreen>
    with RouteAware, RouteObserverMixin {

  ScrollController _scrollController = ScrollController();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _scrollController.addListener(_scrollListener);
  }

  @override
  void didPop() {
    Res.bottomNavBarAnimStream.add(true);
    super.didPop();
  }

  @override
  void didPopNext() {
    // TODO: implement didPopNext
    super.didPopNext();
    Res.titleStream.add(widget.news.title.substring(0, 20) + '...');
  }

  @override
  void didPush() {
    // TODO: implement didPush
    super.didPush();
    Res.titleStream.add(widget.news.title.substring(0, 20) + '...');
  }

  _scrollListener() {
    if (_scrollController.position.userScrollDirection ==
        ScrollDirection.reverse) {
      Res.bottomNavBarAnimStream.add(false);
    } else {
      Res.bottomNavBarAnimStream.add(true);
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _scrollController.removeListener(_scrollListener);
  }

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: widget.news.id,
      child: SingleChildScrollView(
        controller: _scrollController,
        child: Column(
          children: [
            TransitionToImage(
              image: AdvancedNetworkImage(widget.news.photo,
                  timeoutDuration: Duration(minutes: 3)),
              loadingWidget: _shimmer(width: 80.w, height: 80.w),
              placeholder: Container(
                width: 1.sw,
                child: Icon(
                  Icons.broken_image_outlined,
                  color: Colors.white,
                ),
              ),
              fit: BoxFit.cover,
              width: 1.sw,
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 50.h, horizontal: 30.w),
              child: Text(
                widget.news.content,
                style: GoogleFonts.cairo(),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _shimmer({double width, double height}) {
    return Shimmer.fromColors(
        child: Container(
          width: width,
          height: height,
          color: Colors.grey,
        ),
        baseColor: Colors.grey.withOpacity(.5),
        highlightColor: Colors.white);
  }
}
