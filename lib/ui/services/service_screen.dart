import 'package:bunyan/localization/language/languages.dart';
import 'package:bunyan/localization/locale_constant.dart';
import 'package:bunyan/models/chat.dart';
import 'package:bunyan/models/service.dart';
import 'package:bunyan/tools/res.dart';
import 'package:bunyan/tools/webservices/products.dart';
import 'package:bunyan/tools/webservices/users.dart';
import 'package:bunyan/ui/chat/conversation_screen.dart';
import 'package:bunyan/ui/common/location_picker.dart';
import 'package:bunyan/ui/main/main_screen.dart';
import 'package:bunyan/ui/notifications/notifications_screen.dart';
import 'package:bunyan/ui/profile/profile_agent.dart';
import 'package:bunyan/ui/profile/profile_screen.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:route_observer_mixin/route_observer_mixin.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:html/parser.dart' show parse;

class ServiceScreen extends StatefulWidget {
  ServiceScreen({Key key, this.service}) : super(key: key);

  final ServiceModel service;

  @override
  _ServiceScreenState createState() => _ServiceScreenState();
}

class _ServiceScreenState extends State<ServiceScreen>
    with RouteAware, RouteObserverMixin {
  ScrollController _scrollController = ScrollController();
  bool _isFavoriting = false;
  bool _isLiking = false;
  bool _isFollowingEnterprise = false;
  int _currentIndex = 0;
  String _phone;

  @override
  void initState() {
    super.initState();
    Res.titleStream.add('خدمات');

    _scrollController.addListener(_scrollListener);

    _phone = widget.service.owner.phone.replaceAll(' ', '');
    _phone = _phone.replaceAll('+', '');
    if (_phone.startsWith('00')) _phone = _phone.replaceFirst('00', '');
  }

  _scrollListener() {
    if (_scrollController.position.userScrollDirection ==
        ScrollDirection.reverse)
      Res.bottomNavBarAnimStream.add(false);
    else
      Res.bottomNavBarAnimStream.add(true);
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  void didPopNext() {
    Res.titleStream.add('عقارات');
    super.didPopNext();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            icon: const Icon(Icons.notifications_outlined),
            tooltip: 'Show Snackbar',
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => NotificationsScreen()));
            },
          ),
          PopupMenuButton<String>(
            icon: Icon(Icons.language_outlined),
            onSelected: (String result) {
              changeLanguage(context, result);
            },
            itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
              PopupMenuItem<String>(
                value: 'ar',
                child: Text('اَلْعَرَبِيَّةُ‎',
                    style: GoogleFonts.cairo(fontWeight: FontWeight.w700)),
              ),
              PopupMenuItem<String>(
                value: 'en',
                child: Text('English',
                    style: GoogleFonts.cairo(fontWeight: FontWeight.w700)),
              ),
            ],
          ),
        ],
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        elevation: 0,
        leading: InkWell(
          onTap: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => MainScreen(
                          menu_index: 0,
                        )));
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ClipOval(
              child: Image.asset(
                'assets/logo.min.png',
                height: 40,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        title: Text(
          "Services",
          style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          controller: _scrollController,
          //padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 8.0),
          child: Column(
            children: [
              _slider(),
              SizedBox(
                height: 8.0,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 8.0),
                child: Column(
                  children: [
                    _headerTitle(),
                    SizedBox(height: 20.0),
                    _contentCard(),
                    SizedBox(
                      height: 15.0,
                    ),
                    _userCard(),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 3.0,
        clipBehavior: Clip.antiAlias,
        child: Container(
          height: 65,
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: BottomNavigationBar(
                selectedLabelStyle:
                    GoogleFonts.cairo(fontWeight: FontWeight.w700),
                unselectedLabelStyle: GoogleFonts.cairo(),
                currentIndex: _currentIndex,
                backgroundColor: Colors.black,
                selectedItemColor: Colors.black,
                unselectedItemColor: Colors.grey,
                onTap: (index) {
                  print(index);
                  setState(() {
                    _currentIndex = index;
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MainScreen(
                                  menu_index: _currentIndex,
                                )));
                  });
                },
                items: [
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.home_outlined,
                    ),
                    activeIcon: Icon(Icons.home),
                    label: Languages.of(context).menuHome,
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.favorite_outline,
                    ),
                    activeIcon: Icon(Icons.favorite),
                    label: Languages.of(context).menuFavorite,
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      FontAwesome.comments,
                      color: Colors.transparent,
                    ),
                    label: '',
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.chat_outlined,
                    ),
                    activeIcon: Icon(Icons.chat),
                    label: Languages.of(context).productDetailsCallChat,
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.person_outline,
                    ),
                    activeIcon: Icon(Icons.person),
                    label: Languages.of(context).menuProfile,
                  ),
                ]),
          ),
        ),
      ),
      floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterDocked,
      floatingActionButton: MediaQuery.of(context).viewInsets.bottom == 0
          ? FloatingActionButton(
              mini: true,
              backgroundColor: Colors.black,
              child: Icon(FontAwesome.plus),
              onPressed: () => setState(() {}),
            )
          : Container(),
    );
  }

  Widget _headerTitle() {
    Size size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.only(top: 35.h, left: 25.w, right: 25.w),
      width: 1.sw,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ConstrainedBox(
                constraints: BoxConstraints(
                  maxWidth: size.width - 100,
                ),
                child: Text(
                  (widget.service.title ?? 'N/A') +
                      " " +
                      widget.service.id.toString(),
                  style: GoogleFonts.cairo(
                      color: Colors.black,
                      fontSize: 30.sp,
                      height: 1.4,
                      fontWeight: FontWeight.bold),
                ),
              ),
              _textIcon(
                  text: widget.service.region?.name ?? 'N/A',
                  icon: Icons.location_pin),
            ],
          ),
          _textIcon(
            icon: Icons.date_range,
            text: DateTime.now().difference(widget.service.createdAt).inDays ==
                    0
                ? Languages.of(context).today
                : DateTime.now().difference(widget.service.createdAt).inDays <=
                        7
                    ? Languages.of(context).before +
                        ' ' +
                        DateTime.now()
                            .difference(widget.service.createdAt)
                            .inDays
                            .toString() +
                        ' ' +
                        Languages.of(context).days
                    : '${widget.service.createdAt.day}/${widget.service.createdAt.month}/${widget.service.createdAt.year}',
          ),
          SizedBox(
            height: 35.w,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _productActionBtn(
                  color: Colors.blue,
                  icon: FontAwesome.thumbs_up,
                  text: widget.service.likes.toString(),
                  onTap: _isLiking ? null : _likeProd,
                  done: widget.service.liked),
              SizedBox(
                width: 20.w,
              ),
              _productActionBtn(
                  color: Colors.red,
                  icon: Icons.favorite_border,
                  text: widget.service.favorite
                      ? Languages.of(context).productDetailsUnFavorite
                      : Languages.of(context).productDetailsFavorite,
                  onTap: _isFavoriting ? null : _updateFavorite,
                  done: widget.service.favorite),
              SizedBox(
                width: 20.w,
              ),
              _productActionBtn(
                color: Colors.green,
                icon: Icons.share,
                text: Languages.of(context).productDetailsShare,
                onTap: () => _showErrorDialog(
                    text: 'غير متاح') /*Share.share('https://google.com')*/,
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _productActionBtn(
      {Color color,
      String text,
      IconData icon,
      bool done = false,
      Function() onTap}) {
    return InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: color),
          color: done ? color : null,
          borderRadius: BorderRadius.circular(5.0),
        ),
        padding: EdgeInsets.symmetric(vertical: 3.h, horizontal: 8.w),
        child: Center(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                icon,
                size: 17.sp,
                color: done ? Colors.white : color,
              ),
              SizedBox(
                width: 8.w,
              ),
              Text(
                text,
                style: GoogleFonts.cairo(
                    color: done ? Colors.white : color, fontSize: 16.sp),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _userCard() {
    Size size = MediaQuery.of(context).size;
    return Stack(
      children: [
        Image.asset("assets/Image 280.jpg"),
        Padding(
          padding: EdgeInsets.only(top: 200.w),
          child: Card(
            elevation: 5.0,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 40.h, horizontal: 20),
              child: Column(
                children: [
                  SizedBox(
                    height: 40.w,
                  ),
                  SizedBox(
                    height: 20.h,
                  ),
                  SizedBox(
                    height: 20.h,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Expanded(
                        child: _contactBtn(
                            text: Languages.of(context).productDetailsCallWhats,
                            icon: FontAwesome.whatsapp,
                            onTap: () {
                              launch('https://wa.me/$_phone}');
                            },
                            color: Colors.green),
                      ),
                      SizedBox(
                        width: 5.sp,
                      ),
                      Expanded(
                        child: _contactBtn(
                            text: Languages.of(context).productDetailsCallPhone,
                            icon: FontAwesome.phone,
                            onTap: () {
                              launch('tel:${_phone}');
                            },
                            color: Colors.blue),
                      ),
                      SizedBox(
                        width: 5.sp,
                      ),
                      Expanded(
                        child: _contactBtn(
                            text: Languages.of(context).productDetailsCallChat,
                            icon: FontAwesome.comments,
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ConversationScreen(
                                        chat: ChatModel(
                                          receiver: widget.service.owner,
                                          sender: Res.USER,
                                        ),
                                        senderName:
                                        widget.service.owner.name,
                                      )));
                            },
                            color: Colors.red),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 40.h,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30),
                    child: MaterialButton(
                      onPressed: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ProfileAgent(
                                profile: widget.service.owner,
                                enterprise: widget.service.enterprise,
                              ))),
                      child: Text(
                        Languages.of(context).productDetailsMoreAds,
                        style: GoogleFonts.cairo(
                            fontSize: 14, fontWeight: FontWeight.w700),
                      ),
                      textColor: Colors.black,
                      minWidth: double.infinity,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                          side: BorderSide(color: Colors.black)),
                      visualDensity: VisualDensity.comfortable,
                      padding: EdgeInsets.symmetric(vertical: 10),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        Positioned(
          top: 150.w,
          right: .0,
          left: .0,
          child: InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ProfileAgent(
                        profile: widget.service.owner,
                        enterprise: widget.service.enterprise,
                      )));
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Card(
                  color: Colors.grey,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5000.0)),
                  child: Container(
                    padding: EdgeInsets.all(1.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(5000.0),
                      child: Hero(
                        tag: 'profile_picture',
                        child: TransitionToImage(
                          image: AdvancedNetworkImage(
                              widget.service.owner?.photo ?? ''),
                          loadingWidget: _shimmer(width: 100.w, height: 100.w),
                          placeholder: Container(
                            width: 100.w,
                            height: 100.w,
                            child: Icon(Icons.broken_image_outlined),
                            color: Colors.white,
                          ),
                          width: 100.w,
                          height: 100.w,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
                Text(
                  widget.service.owner?.name ?? '',
                  style: GoogleFonts.cairo(fontSize: 15.sp),
                )
              ],
            ),
          ),
        ),
       Padding(
         padding: const EdgeInsets.only(left: 8.0, right: 8.0),
         child: Row(
           mainAxisAlignment: MainAxisAlignment.spaceBetween,
           children: [
             Positioned(
               child: Column(
                 crossAxisAlignment: CrossAxisAlignment.center,
                 children: [
                   Card(
                     color: Colors.grey,
                     shape: RoundedRectangleBorder(
                         borderRadius: BorderRadius.circular(5000.0)),
                     child: Container(
                       padding: EdgeInsets.all(1.0),
                       child: ClipRRect(
                         borderRadius: BorderRadius.circular(5000.0),
                         child: Hero(
                           tag: 'profile_picture',
                           child: TransitionToImage(
                             image: AdvancedNetworkImage(
                                 widget.service.owner?.photo ?? ''),
                             loadingWidget: _shimmer(width: 100.w, height: 100.w),
                             placeholder: Container(
                               width: 100.w,
                               height: 100.w,
                               child: Icon(Icons.broken_image_outlined),
                               color: Colors.white,
                             ),
                             width: 100.w,
                             height: 100.w,
                             fit: BoxFit.cover,
                           ),
                         ),
                       ),
                     ),
                   ),
                 ],
               ),
             ),
             Positioned(
                 right: 10,
                 top: 10,
                 child: InkWell(
                   onTap: () {
                     Navigator.push(
                       context,
                       MaterialPageRoute(
                           builder: (context) => ProfileScreen(
                             profile: widget.service.owner,
                             enterprise: widget.service.enterprise,
                           )),
                     );
                   },
                   child: Container(
                     width: size.width * 0.25,
                     decoration: BoxDecoration(
                       color: Colors.white,
                       borderRadius: BorderRadius.circular(5.0),
                     ),
                     padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                     alignment: Alignment.center,
                     child: Row(
                       crossAxisAlignment: CrossAxisAlignment.center,
                       mainAxisAlignment: MainAxisAlignment.center,
                       children: [
                         SizedBox(
                           width: 5,
                         ),
                         Text(
                           "Go to profile",
                           style: GoogleFonts.cairo(
                               color: Colors.black,
                               fontSize: 13,
                               fontWeight: FontWeight.w700),
                         ),
                       ],
                     ),
                   ),
                 )),
           ],
         ),
       )
      ],
    );
  }

  Widget _textIcon({String text, IconData icon}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Icon(
          icon,
          color: Colors.grey,
          size: 25.sp,
        ),
        SizedBox(
          width: 10.w,
        ),
        Text(
          text,
          style: GoogleFonts.cairo(
              color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 20.sp),
        ),
      ],
    );
  }

  Widget _shimmer({double width, double height}) {
    return Shimmer.fromColors(
      child: Container(
        width: width,
        height: height,
        color: const Color(0xFFf3f3f3),
      ),
      baseColor: const Color(0xFFf3f3f3),
      highlightColor: const Color(0xFFE8E8E8),
    );
  }

  Widget _headerImage() {
    return TransitionToImage(
      image:
          AdvancedNetworkImage(widget.service.photos ?? '', useDiskCache: true),
      width: 1.sw,
      fit: BoxFit.cover,
      loadingWidget: _shimmer(),
      placeholder: Icon(Icons.broken_image_outlined),
      repeat: ImageRepeat.noRepeat,
    );
  }

  Widget _slider() {
    return Stack(
      children: [
        CarouselSlider.builder(
            options: CarouselOptions(
              height: .48.sh,
              viewportFraction: 1,
              autoPlay: widget.service.photos.length > 1,
              autoPlayInterval: Duration(seconds: 2, milliseconds: 500),
              enableInfiniteScroll: widget.service.photos.length > 1,
            ),
            itemCount: widget.service.photos.length,
            itemBuilder: (context, index, realIndex) => TransitionToImage(
                  image: AdvancedNetworkImage(widget.service.photos[index],
                      useDiskCache: true),
                  width: 1.sw,
                  fit: BoxFit.cover,
                  loadingWidget: _shimmer(),
                  placeholder: Icon(Icons.broken_image_outlined),
                  repeat: ImageRepeat.noRepeat,
                )),
        Positioned(
          top: .0,
          left: .0,
          child: Container(
            decoration: BoxDecoration(
                color: Colors.red,
                borderRadius:
                    BorderRadius.only(bottomRight: Radius.circular(20.0))),
            padding: EdgeInsets.symmetric(vertical: 5.w, horizontal: 25.w),
            child: Text(
              '${formatDecimal(widget.service.price)} QAR',
              style: GoogleFonts.cairo(
                  fontSize: 20.0.sp,
                  color: Colors.white.withOpacity(.9),
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        /*Positioned.fill(
          bottom: .0,
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(bottom: 20.h),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.black54,
                    borderRadius: BorderRadius.circular(5.0)),
                padding: EdgeInsets.all(10.w),
                child: StreamBuilder<int>(
                  stream: _carouselStreamController.stream,
                  builder: (context, snapshot) {
                    return Row(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: widget.service.photos
                          .map((e) =>
                          Icon(
                            widget.service.photos.indexOf(e) == snapshot.data
                                ? FontAwesome.circle
                                : FontAwesome.circle_o,
                            color: Colors.white,
                            size: 12.sp,
                          ))
                          .toList(),
                    );
                  }
                ),
              ),
            ),
          ),
        )*/
      ],
    );
  }

  String formatDecimal(double n) {
    return n.toStringAsFixed(n.truncateToDouble() == n ? 0 : 1);
  }

  Widget _contactBtn(
      {String text, IconData icon, Color color, Function() onTap}) {
    Size size = MediaQuery.of(context).size;
    return InkWell(
      onTap: onTap,
      child: Container(
        width: size.width * 0.25,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(5.0),
        ),
        padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        alignment: Alignment.center,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              icon,
              size: 27.sp,
              color: Colors.white,
            ),
            SizedBox(
              width: 5,
            ),
            Text(
              text,
              style: GoogleFonts.cairo(
                  color: Colors.white,
                  fontSize: 13,
                  fontWeight: FontWeight.w700),
            ),
          ],
        ),
      ),
    );
  }

  Widget _enterpriseActionBtn(
      {Color color,
      IconData icon,
      String text,
      VoidCallback onTap,
      bool done = false}) {
    return InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(color: color),
            borderRadius: BorderRadius.circular(5.0),
            color: done ? color : null),
        padding: EdgeInsets.symmetric(vertical: 3.h, horizontal: 5.w),
        alignment: Alignment.center,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              icon,
              size: 17.sp,
              color: !done ? color : Colors.white,
            ),
            SizedBox(
              width: 8.w,
            ),
            Text(
              text,
              style: GoogleFonts.cairo(
                  color: !done ? color : Colors.white, fontSize: 16.sp),
            )
          ],
        ),
      ),
    );
  }

  Future<void> updateEntrFollow() async {
    setState(() {
      _isFollowingEnterprise = true;
    });
    final willFollow = !widget.service.enterprise.isFollowing;
    try {
      final success = await UsersWebService().followEnterprise(
          id: widget.service.enterprise.id, follow: willFollow);
      if (!success)
        _showErrorDialog();
      else
        setState(() {
          widget.service.enterprise.isFollowing = willFollow;
          widget.service.enterprise.followers += willFollow ? 1 : -1;
          widget.service.enterprise.isFollowing = willFollow;
        });
    } catch (e) {
      _showErrorDialog();
    }
    setState(() {
      _isFollowingEnterprise = false;
    });
  }

  void _showErrorDialog({String text}) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              content: Text(
                text ?? 'خطأ في الخوادم. الرجاء المحاولة لاحقا.',
                style: GoogleFonts.cairo(),
              ),
              actions: [
                TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: Text(
                      'موافق',
                      style: GoogleFonts.cairo(color: Colors.teal),
                    ))
              ],
            ));
  }

  Future<void> _updateFavorite() async {
    setState(() {
      _isFavoriting = true;
    });
    try {
      await ProductsWebService().setFavorit(
          id: widget.service.id,
          isRealestate: !(widget.service is ServiceModel),
          set: !widget.service.favorite);
      setState(() {
        widget.service.favorite = !widget.service.favorite;
      });
    } catch (e) {
      _showErrorDialog();
    }
    setState(() {
      _isFavoriting = false;
    });
  }

  void _likeProd() {
    if (!_isLiking) {
      setState(() {
        _isLiking = true;
      });
      ProductsWebService()
          .likeProd(
              id: widget.service.id,
              isRealEstate: false,
              dislike: !widget.service.liked)
          .then((value) {
        setState(() {
          if (value) {
            widget.service.liked = !widget.service.liked;
            widget.service.likes += widget.service.liked ? 1 : -1;
          } else
            _showErrorDialog();
          _isLiking = false;
        });
      }).catchError((err) {
        _showErrorDialog();
        setState(() {
          _isLiking = false;
        });
      });
    }
  }

  Widget _contentCard() {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      elevation: 3.0,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
        width: 1.sw,
        child: Column(
          children: [
            Text(
              parse(widget.service.description).documentElement.text,
              style: GoogleFonts.cairo(),
            ),
            _textPair(
                key: Languages.of(context).productDetailsID, value: '2215441'),
            SizedBox(
              height: 40.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                _button(
                    text: Languages.of(context).productDetailsReport,
                    icon: Icons.assistant_photo,
                    borderColor: Colors.red,
                    onTap: _reportAd,
                    textColor: Colors.red),
                _button(
                    text: Languages.of(context).productDetailsShowOnMap,
                    icon: Icons.map_sharp,
                    borderColor: Colors.blue,
                    onTap: _showOnMap,
                    textColor: Colors.blue),
              ],
            ),
          ],
        ),
      ),
    );
  }

  _showOnMap() {
    final position = widget.service.position;
    if (position != null && position.lat != null && position.lng != null)
      Navigator.push(
          Res.mainContext,
          MaterialPageRoute(
              builder: (context) => LocationPicker(
                    position: position,
                  )));
    else
      ScaffoldMessenger.of(Res.scaffoldKey.currentContext)
          .showSnackBar(SnackBar(content: Text('Lيس لهذا الإعلان موقع محدد')));
  }

  Future<void> _reportAd() async {
    final success = await ProductsWebService().reportProduct(
        id: widget.service.id,
        type: widget.service is ServiceModel
            ? ProductsWebService.SERVICE
            : ProductsWebService.REAL_ESTATE);
    if (success)
      showDialog(
          context: context,
          builder: (context) => AlertDialog(
                content: Text(
                  'شكرا لقيامك بالتبليغ عن الإعلان.',
                  style: GoogleFonts.cairo(),
                ),
                actions: [
                  TextButton(
                      onPressed: () => Navigator.pop(context),
                      child: Text(
                        'موافق',
                        style: GoogleFonts.cairo(color: Colors.teal),
                      ))
                ],
              ));

    _showErrorDialog();
  }

  Widget _button(
      {String text,
      Color color = Colors.transparent,
      Color borderColor = Colors.transparent,
      Color textColor = Colors.black,
      VoidCallback onTap,
      IconData icon}) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(5.0),
      child: InkWell(
        onTap: () {
          if (onTap != null) onTap();
        },
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: borderColor),
            color: color,
            borderRadius: BorderRadius.circular(5.0),
          ),
          padding: EdgeInsets.symmetric(vertical: 3.h, horizontal: 5.w),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Icon(
                icon,
                size: 17.sp,
                color: textColor,
              ),
              SizedBox(
                width: 8.w,
              ),
              Text(
                text,
                style: GoogleFonts.cairo(color: textColor, fontSize: 16.sp),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _textPair({String key, String value, IconData icon}) {
    return Row(
      children: [
        icon != null
            ? Icon(
                icon,
                size: 25.sp,
              )
            : Container(),
        icon != null
            ? SizedBox(
                width: 5.w,
              )
            : Container(),
        key != null
            ? Text(
                '$key:',
                style: GoogleFonts.cairo(
                    fontSize: 20.sp,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              )
            : Container(),
        SizedBox(
          width: key != null ? 10.w : .0,
        ),
        Text(
          value,
          style: GoogleFonts.cairo(fontSize: 23.sp, color: Colors.black),
        ),
      ],
    );
  }
}
