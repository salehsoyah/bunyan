import 'package:bunyan/localization/language/languages.dart';
import 'package:bunyan/localization/locale_constant.dart';
import 'package:bunyan/models/banner.dart';
import 'package:bunyan/models/category.dart';
import 'package:bunyan/models/city.dart';
import 'package:bunyan/models/product_list.dart';
import 'package:bunyan/models/region.dart';
import 'package:bunyan/models/services_filter.dart';
import 'package:bunyan/tools/res.dart';
import 'package:bunyan/tools/webservices/addresses.dart';
import 'package:bunyan/tools/webservices/advertises.dart';
import 'package:bunyan/tools/webservices/products.dart';
import 'package:bunyan/ui/common/card_item.dart';
import 'package:bunyan/ui/common/search_widget.dart';
import 'package:bunyan/ui/common/top_ad_banner.dart';
import 'package:bunyan/ui/main/main_screen.dart';
import 'package:bunyan/ui/notifications/notifications_screen.dart';
import 'package:bunyan/ui/real_estates/real_estates_screen.dart';
import 'package:bunyan/ui/services/service_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:route_observer_mixin/route_observer_mixin.dart';
import 'package:shimmer/shimmer.dart';

class ServicesScreen extends StatefulWidget {
  ServicesScreen({Key key}) : super(key: key);

  @override
  _ServicesState createState() => _ServicesState();
}

class _ServicesState extends State<ServicesScreen>
    with TickerProviderStateMixin, RouteAware, RouteObserverMixin {
  bool _showFilter = false;
  bool _isFetching = false;
  List<ProductListModel> services = [];
  List<ProductListModel> servicesSearch = [];
  List<RegionModel> regions = [];
  List<CityModel> cities = [];
  List<BannerModel> _banners = [];
  List<CategoryModel> _cats = [];
  ScrollController _scrollController = ScrollController();
  ServicesFilterModel _filter = ServicesFilterModel();
  bool _stillFetch = true;
  Locale currentLang;
  int _currentIndex = 0;
  GlobalKey<FormFieldState> _key = new GlobalKey();

  int regionId;
  int cityId;
  double priceFrom;
  double priceTo;
  String serviceId;

  @override
  void initState() {
    AddressesWebService().getRegions().then((rgs) {
      setState(() {
        regions = rgs;
      });
      print(regions);
    });
    _stillFetch = true;

    getLocale().then((locale) {
      setState(() {
        ProductsWebService()
            .getServicesCategories(locale.languageCode)
            .then((value) {
          setState(() {
            Res.servicesCategories = value;
            _cats = Res.servicesCategories;
            _stillFetch = false;
          });
        });
      });
    });

    super.initState();

    // AdvertisesWebService().getBanners().then((value) {
    //   setState(() {
    //     _banners = value;
    //   });
    // });

    _filter.page = 1;
    getCurrentLang();

    Res.titleStream.add('خدمات');

    _scrollController.addListener(_scrollListener);
  }

  getCurrentLang() async {
    getLocale().then((locale) {
      setState(() {
        currentLang = locale;
        getData();
        print('object ${currentLang.toString()}');
      });
    });
  }

  getData() async {
    final futures = await Future.wait([
      ProductsWebService().getServicesCategories(currentLang.toString()),
      AdvertisesWebService().getBanners(),
    ]);
    setState(() {
      Res.servicesCategories = futures[0];
      _banners = futures[1];
    });
    _getData();
  }

  _scrollListener() {
    if (_scrollController.position.userScrollDirection ==
        ScrollDirection.reverse)
      Res.bottomNavBarAnimStream.add(false);
    else
      Res.bottomNavBarAnimStream.add(true);

    if (_scrollController.position.pixels <
            _scrollController.position.maxScrollExtent - 60.0 &&
        !_isFetching) {
      _filter.page++;
      _getData();
    }
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  void didPopNext() {
    Res.titleStream.add('خدمات');
    super.didPopNext();
  }

  getCities(int regionSelected) async {
    AddressesWebService().getCities(regionSelected).then((cts) {
      setState(() {
        Provider.of<CityBasket>(context, listen: false).affectCities(cts);
        cities = cts;
      });
      print(cities);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            icon: const Icon(Icons.notifications_outlined),
            tooltip: 'Show Snackbar',
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => NotificationsScreen()));
            },
          ),
          PopupMenuButton<String>(
            icon: Icon(Icons.language_outlined),
            onSelected: (String result) {
              changeLanguage(context, result);
            },
            itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
              PopupMenuItem<String>(
                value: 'ar',
                child: Text('اَلْعَرَبِيَّةُ‎',
                    style: GoogleFonts.cairo(fontWeight: FontWeight.w700)),
              ),
              PopupMenuItem<String>(
                value: 'en',
                child: Text('English',
                    style: GoogleFonts.cairo(fontWeight: FontWeight.w700)),
              ),
            ],
          ),
        ],
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        elevation: 0,
        leading: InkWell(
          onTap: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => MainScreen(
                          menu_index: 0,
                        )));
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ClipOval(
              child: Image.asset(
                'assets/logo.min.png',
                height: 40,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        title: Text(
          "Services",
          style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: Stack(
        children: [
          WillPopScope(
            onWillPop: () async {
              if (_filter.category != null) {
                setState(() {
                  _stillFetch = true;
                  _filter.category = null;
                  _filter.page = 1;
                  services.clear();
                });
                _getData();
                return false;
              }
              if (_showFilter) {
                setState(() {
                  _showFilter = false;
                });
                return false;
              }
              ;
              return true;
            },
            child: Container(
              child: SafeArea(
                child: Container(
                  width: 1.sw,
                  child: CustomScrollView(
                      controller: _scrollController,
                      cacheExtent: 10000.0,
                      physics: AlwaysScrollableScrollPhysics(),
                      shrinkWrap: true,
                      slivers: [
                        SliverList(
                            delegate: SliverChildListDelegate([
                          Column(
                            textDirection: TextDirection.rtl,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              SizedBox(
                                height: 10.0,
                              ),
                              if (_banners.length > 0)
                                TopAdBanner(
                                  banners: _banners,
                                ),
                              // Padding(
                              //   padding: EdgeInsets.only(top: 5.h),
                              //   child: AnimatedSize(
                              //     duration: Duration(milliseconds: 300),
                              //     curve: Curves.easeInOut,
                              //     height: !_showFilter ? .0 : .45.sh,
                              //     vsync: this,
                              //     child: Padding(
                              //       padding: EdgeInsets.only(bottom: 10.h),
                              //       child: SearchWidget(
                              //         showDropDown: false,
                              //         onSearch: (filter) {
                              //           print(filter.toRequest());
                              //           services.clear();
                              //           _stillFetch = true;
                              //           filter.page = 1;
                              //           _filter = ServicesFilterModel.fromJson(
                              //               filter.toJson());
                              //           _getData();
                              //         },
                              //         filter: _filter,
                              //       ),
                              //     ),
                              //   ),
                              // ),
                              /*Padding(
                              padding: EdgeInsets.only(bottom: 10.h),
                              child: SizedBox(
                                height: .15.sh,
                                child: PremiumAds(
                                  ads: _realEstates,
                                ),
                              ),
                            ),*/
                            ],
                          ),
                        ])),

                        //categories
                        if (_filter.category == null)
                          SliverPadding(
                            padding: EdgeInsets.only(bottom: 7.h),
                            sliver: _categories(),
                          ),

                        SliverPadding(
                          padding: EdgeInsets.only(
                              right: .02.sw,
                              left: .02.sw,
                              bottom: _stillFetch ? .0 : 0.1.sh),
                          sliver: !_stillFetch &&
                                  !_isFetching &&
                                  services.isEmpty
                              ? SliverList(
                                  delegate: SliverChildListDelegate([
                                    Center(
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(
                                            vertical: .15.sh),
                                        child: Text(
                                          'لا يوجد عروض',
                                          style: GoogleFonts.cairo(
                                              color: Colors.grey,
                                              fontSize: 40.sp,
                                              fontWeight: FontWeight.w600),
                                        ),
                                      ),
                                    )
                                  ]),
                                )
                              : SliverStaggeredGrid.countBuilder(
                                  crossAxisCount: 2,
                                  itemCount: (servicesSearch.length > 0 &&
                                          servicesSearch.length <
                                              services.length)
                                      ? servicesSearch.length
                                      : services.length,
                                  staggeredTileBuilder: (index) =>
                                      StaggeredTile.fit(1),
                                  itemBuilder: (context, index) {
                                    return InkWell(
                                        onTap: () => Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    ServiceScreen(
                                                      service: (servicesSearch
                                                                      .length >
                                                                  0 &&
                                                              servicesSearch
                                                                      .length <
                                                                  services
                                                                      .length)
                                                          ? servicesSearch[
                                                              index]
                                                          : services[index],
                                                    ))),
                                        /*Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ProductScreen(
                                          product: _realEstates[index],
                                        ))),*/
                                        child: CardItem(
                                            product:
                                                (servicesSearch.length > 0 &&
                                                        servicesSearch.length <
                                                            services.length)
                                                    ? servicesSearch[index]
                                                    : services[index]));
                                  },
                                ),
                        ),

                        if (_stillFetch)
                          SliverList(
                            delegate: SliverChildListDelegate([
                              Padding(
                                padding: EdgeInsets.only(top: 30.h),
                                child: _loadingWidget(),
                              )
                            ]),
                          )
                      ]),
                ),
              ),
            ),
          ),
          Positioned(
              right: 10,
              bottom: 10,
              child: InkWell(
                onTap: () {
                  showDialog(
                    barrierDismissible: false,
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        content: SingleChildScrollView(
                          child: Container(
                            width: MediaQuery.of(context).size.width - 35,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Container(
                                  padding: EdgeInsets.all(8),
                                  decoration: BoxDecoration(
                                      color: Colors.grey.withOpacity(0.4),
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(5),
                                          topLeft: Radius.circular(5))),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          Navigator.pop(context);
                                        },
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(15),
                                          child: Container(
                                              padding: EdgeInsets.all(1),
                                              color:
                                                  Colors.green.withOpacity(0.9),
                                              child: Icon(
                                                Icons.close,
                                                color: Colors.white,
                                              )),
                                        ),
                                      ),
                                      Text(
                                        Languages.of(context).advsearoption,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Container(),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 12.0, right: 12.0, top: 20.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: Colors.grey.withOpacity(0.4),
                                        borderRadius: BorderRadius.circular(8)),
                                    child: DropdownButtonFormField(
                                      hint: Text(
                                        Languages.of(context).regions,
                                      ),
                                      isExpanded: true,
                                      onChanged: (value) {
                                        setState(() {
                                          regionId = value;
                                          _key.currentState.reset();
                                          getCities(value);
                                          print(value);
                                        });
                                      },
                                      onSaved: (value) {
                                        setState(() {});
                                      },
                                      items: regions.map((RegionModel val) {
                                        return DropdownMenuItem(
                                          value: val.id,
                                          child: Text(
                                            val.name,
                                          ),
                                        );
                                      }).toList(),
                                      decoration: InputDecoration(
                                        floatingLabelBehavior:
                                            FloatingLabelBehavior.always,
                                        border: InputBorder.none,
                                        contentPadding: EdgeInsets.only(
                                            left: 10.0, right: 10.0),
                                      ),
                                      icon: Icon(
                                        Icons.keyboard_arrow_down_sharp,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 12.0, right: 12.0, top: 20.0),
                                  child: Container(
                                      decoration: BoxDecoration(
                                          color: Colors.grey.withOpacity(0.4),
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                      child: DropdownButtonFormField(
                                        key: _key,
                                        hint: Text(
                                          Languages.of(context).cities,
                                        ),
                                        isExpanded: true,
                                        onChanged: (value) {
                                          setState(() {
                                            cityId = value;
                                            print(value);
                                          });
                                        },
                                        onSaved: (value) {
                                          setState(() {});
                                        },
                                        items: Provider.of<CityBasket>(context)
                                            .cities
                                            .map((CityModel val) {
                                          return DropdownMenuItem(
                                            value: val.id,
                                            child: Text(
                                              val.name,
                                            ),
                                          );
                                        }).toList(),
                                        decoration: InputDecoration(
                                          floatingLabelBehavior:
                                              FloatingLabelBehavior.always,
                                          border: InputBorder.none,
                                          contentPadding: EdgeInsets.only(
                                              left: 10.0, right: 10.0),
                                        ),
                                        icon: Icon(
                                          Icons.keyboard_arrow_down_sharp,
                                          color: Colors.black,
                                        ),
                                      )),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 12.0, right: 12.0, top: 20.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: Colors.grey.withOpacity(0.4),
                                        borderRadius: BorderRadius.circular(8)),
                                    child: DropdownButtonFormField(
                                      hint: Text(
                                        Languages.of(context).services,
                                      ),
                                      isExpanded: true,
                                      onChanged: (value) {
                                        setState(() {
                                          serviceId = value;
                                          print(value);
                                        });
                                      },
                                      items: _cats.map((CategoryModel val) {
                                        return DropdownMenuItem(
                                          value: val.id,
                                          child: Text(
                                            val.name,
                                          ),
                                        );
                                      }).toList(),
                                      decoration: InputDecoration(
                                        floatingLabelBehavior:
                                            FloatingLabelBehavior.always,
                                        border: InputBorder.none,
                                        contentPadding: EdgeInsets.only(
                                            left: 10.0, right: 10.0),
                                      ),
                                      icon: Icon(
                                        Icons.keyboard_arrow_down_sharp,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 12.0, right: 12.0, top: 20.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(8)),
                                    child: Row(
                                      children: [
                                        Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width -
                                              60,
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.grey.withOpacity(0.4),
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                left: 8.0, right: 8.0),
                                            child: Row(
                                              children: [
                                                Expanded(
                                                    flex: 1,
                                                    child: TextFormField(
                                                      enabled: false,
                                                      decoration:
                                                          const InputDecoration(
                                                              labelText:
                                                                  'Price',
                                                              border:
                                                                  InputBorder
                                                                      .none),
                                                      onSaved: (String value) {
                                                        // This optional block of code can be used to run
                                                        // code when the user saves the form.
                                                      },
                                                      validator:
                                                          (String value) {
                                                        return (value != null &&
                                                                value.contains(
                                                                    '@'))
                                                            ? 'Do not use the @ char.'
                                                            : null;
                                                      },
                                                    )),
                                                Expanded(
                                                  flex: 2,
                                                  child: TextFormField(
                                                    onChanged: (value) {
                                                      if (value != '' &&
                                                          value != null) {
                                                        setState(() {
                                                          priceFrom =
                                                              double.parse(
                                                                  value);
                                                        });
                                                      }
                                                    },
                                                    decoration:
                                                        const InputDecoration(
                                                            labelText: 'From',
                                                            border: InputBorder
                                                                .none),
                                                    onSaved: (String value) {
                                                      // This optional block of code can be used to run
                                                      // code when the user saves the form.
                                                    },
                                                    validator: (String value) {
                                                      return (value != null &&
                                                              value.contains(
                                                                  '@'))
                                                          ? 'Do not use the @ char.'
                                                          : null;
                                                    },
                                                  ),
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      right: 10),
                                                  color: Colors.black45,
                                                  height: 50,
                                                  width: 2,
                                                ),
                                                Expanded(
                                                  flex: 3,
                                                  child: TextFormField(
                                                    onChanged: (value) {
                                                      if (value != '' &&
                                                          value != null) {
                                                        setState(() {
                                                          priceTo =
                                                              double.parse(
                                                                  value);
                                                        });
                                                      }
                                                    },
                                                    decoration:
                                                        const InputDecoration(
                                                            labelText: 'To',
                                                            border: InputBorder
                                                                .none),
                                                    onSaved: (String value) {
                                                      // This optional block of code can be used to run
                                                      // code when the user saves the form.
                                                    },
                                                    validator: (String value) {
                                                      return (value != null &&
                                                              value.contains(
                                                                  '@'))
                                                          ? 'Do not use the @ char.'
                                                          : null;
                                                    },
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        // Message which will be pop up on the screen
                        // Action widget which will provide the user to acknowledge the choice
                        actions: [
                          InkWell(
                            onTap: () {
                              Navigator.pop(context, {
                                "regionId": regionId,
                                "cityId": cityId,
                                "serviceId": serviceId,
                                "fromPrice": priceFrom,
                                "toPrice": priceTo,
                              });
                            },
                            child: Container(
                              padding:
                                  EdgeInsets.fromLTRB(15.0, 6.0, 15.0, 6.0),
                              decoration: BoxDecoration(
                                  color: Colors.red,
                                  borderRadius: BorderRadius.circular(15)),
                              child: Text(
                                Languages.of(context).search,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          )
                        ],
                        contentPadding: EdgeInsets.all(0),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0))),
                        insetPadding: EdgeInsets.zero,
                      );
                    },
                  ).then((value) {
                    print(value);
                    if (value != null) {
                      setState(() {
                        servicesSearch = services;

                        if (value['serviceId'] != null) {
                          servicesSearch = servicesSearch
                              .where((element) =>
                                  element.category != null &&
                                  element.category.id == value['serviceId'])
                              .toList();
                        }
                        if (value['regionId'] != null) {
                          servicesSearch = servicesSearch
                              .where((element) =>
                                  element.region != null &&
                                  element.region.id == value['regionId'])
                              .toList();
                        }
                        if (value['cityId'] != null) {
                          servicesSearch = servicesSearch
                              .where((element) =>
                                  element.city != null &&
                                  element.city.id == value['cityId'])
                              .toList();
                        }

                        if (value['fromPrice'] != null) {
                          servicesSearch = servicesSearch
                              .where((element) =>
                                  element.price != null &&
                                  element.price >= value['fromPrice'])
                              .toList();
                        }
                        if (value['toPrice'] != null) {
                          servicesSearch = servicesSearch
                              .where((element) =>
                                  element.price != null &&
                                  element.price <= value['toPrice'])
                              .toList();
                        }
                        print("" + servicesSearch.length.toString());

                        regionId = null;
                        serviceId = null;
                        priceTo = null;
                        priceFrom = null;
                        cityId = null;
                      });
                    }
                  });
                },
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(40),
                    child: Container(
                        color: Colors.red.withOpacity(0.75),
                        child: Padding(
                          padding: EdgeInsets.all(20.h),
                          child: Icon(
                            Icons.search_rounded,
                            color: Colors.white,
                            size: 30,
                          ),
                        ))),
              ))
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 3.0,
        clipBehavior: Clip.antiAlias,
        child: Container(
          height: 65,
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: BottomNavigationBar(
                selectedLabelStyle:
                    GoogleFonts.cairo(fontWeight: FontWeight.w700),
                unselectedLabelStyle: GoogleFonts.cairo(),
                currentIndex: _currentIndex,
                backgroundColor: Colors.black,
                selectedItemColor: Colors.black,
                unselectedItemColor: Colors.grey,
                onTap: (index) {
                  print(index);
                  setState(() {
                    _currentIndex = index;
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MainScreen(
                                  menu_index: _currentIndex,
                                )));
                  });
                },
                items: [
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.home_outlined,
                    ),
                    activeIcon: Icon(Icons.home),
                    label: Languages.of(context).menuHome,
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.favorite_outline,
                    ),
                    activeIcon: Icon(Icons.favorite),
                    label: Languages.of(context).menuFavorite,
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      FontAwesome.comments,
                      color: Colors.transparent,
                    ),
                    label: '',
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.chat_outlined,
                    ),
                    activeIcon: Icon(Icons.chat),
                    label: Languages.of(context).productDetailsCallChat,
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.person_outline,
                    ),
                    activeIcon: Icon(Icons.person),
                    label: Languages.of(context).menuProfile,
                  ),
                ]),
          ),
        ),
      ),
      floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterDocked,
      floatingActionButton: MediaQuery.of(context).viewInsets.bottom == 0
          ? FloatingActionButton(
              mini: true,
              backgroundColor: Colors.black,
              child: Icon(FontAwesome.plus),
              onPressed: () => setState(() {}),
            )
          : Container(),
    );
  }

  Widget _loadingWidget() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        CupertinoActivityIndicator(
          radius: 20.sp,
        ),
        SizedBox(width: 20.w),
        Text(
          Languages.of(context).loader,
          style: GoogleFonts.cairo(
              color: Colors.grey, fontSize: 30.sp, fontWeight: FontWeight.w600),
        )
      ],
    );
  }

  _shimmerItem(index) {
    return Card(
      elevation: 2.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        textDirection: TextDirection.rtl,
        mainAxisSize: MainAxisSize.min,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: Stack(
              children: [
                Shimmer.fromColors(
                  baseColor: const Color(0xFFf3f3f3),
                  highlightColor: const Color(0xFFE8E8E8),
                  child: Container(
                    height: index.isOdd ? 380.h : 280.h,
                    width: double.infinity,
                    color: Colors.grey,
                  ),
                ),
                Positioned(
                  top: .0,
                  left: .0,
                  child: Shimmer.fromColors(
                    baseColor: Color(0xffbfbdbd),
                    highlightColor: const Color(0xFFE8E8E8),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(.8),
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(20.0))),
                      width: .25.sw,
                      height: 30.h,
                      padding:
                          EdgeInsets.symmetric(vertical: 5.w, horizontal: 25.w),
                    ),
                  ),
                ),
                Positioned.fill(
                    child: ClipRRect(
                  borderRadius: BorderRadius.circular(3.0),
                  child: Container(
                    width: 1.sw,
                    alignment: Alignment.bottomRight,
                    child: Padding(
                      padding: EdgeInsets.only(
                          right: 20.w, bottom: 20.h, left: 15.w),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Padding(
                                padding: EdgeInsets.only(left: 10.w),
                                child: Shimmer.fromColors(
                                  baseColor: Color(0xffbfbdbd),
                                  highlightColor: const Color(0xFFE8E8E8),
                                  child: Container(
                                    width: .1.sw,
                                    height: 20.h,
                                    color: Colors.grey.withOpacity(.8),
                                  ),
                                )),
                          ),
                          Icon(
                            Icons.location_pin,
                            color: Colors.white,
                            size: 22.w,
                          ),
                          Shimmer.fromColors(
                            baseColor: Color(0xffbfbdbd),
                            highlightColor: const Color(0xFFE8E8E8),
                            child: Container(
                              width: .08.sw,
                              height: 10.h,
                              color: Colors.grey.withOpacity(.8),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ))
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _categories() {
    return SliverList(
      delegate: SliverChildListDelegate([
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: _cats
                  .map((cat) => InkWell(
                onTap: () {
                  setState(() {
                    _stillFetch = true;
                    _filter.page = 1;
                    _filter.category = cat;
                    services.clear();
                  });
                  _getData();
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 3.w),
                  child: Container(
                      width: 135.w,
                      height: 135.w,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(5),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.5),
                            offset: const Offset(0.0, 5.0), //(x,y)
                            blurRadius: 3.0,
                          ),
                        ],
                      ),
                      child: Stack(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(5),
                                topRight: Radius.circular(5)),
                            child: TransitionToImage(
                              image: AdvancedNetworkImage(
                                  cat.photo ?? '',
                                  timeoutDuration:
                                  Duration(minutes: 3)),
                              loadingWidget:
                              _shimmer(width: 135.w, height: 100.w),
                              fit: BoxFit.cover,
                              width: 135.w,
                              height: 100.w,
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                                  Colors.white.withOpacity(0.3),
                                  Colors.white.withOpacity(0.3),
                                ],
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Text(
                              cat.name,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black, fontSize: 16.w),
                              textAlign: TextAlign.center,
                            ),
                          )
                        ],
                      )),
                ),
              ))
                  .toList(),
            ),
          ),
        )
      ]),
    );
  }

  Widget _shimmer({double width, double height}) {
    return Shimmer.fromColors(
        child: Container(
          width: width,
          height: height,
          color: Colors.grey,
        ),
        baseColor: Colors.grey.withOpacity(.5),
        highlightColor: Colors.white);
  }

  void _getData() {
    if (_stillFetch && !_isFetching) {
      setState(() {
        _isFetching = true;
      });
      ProductsWebService().getServices(filter: _filter).then((value) {
        setState(() {
          services.addAll(value);
          _isFetching = false;
          if (value.length < Res.PAGE_SIZE) _stillFetch = false;
        });
      }).catchError((err) {
        _isFetching = false;
        _stillFetch = false;
      });
    }
  }
}
