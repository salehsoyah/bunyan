import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:bunyan/localization/language/languages.dart';
import 'package:bunyan/models/category.dart';
import 'package:bunyan/models/city.dart';
import 'package:bunyan/models/position.dart';
import 'package:bunyan/models/region.dart';
import 'package:bunyan/models/service.dart';
import 'package:bunyan/tools/res.dart';
import 'package:bunyan/tools/webservices/products.dart';
import 'package:bunyan/ui/common/location_picker.dart';
import 'package:bunyan/ui/home/home_screen.dart';
import 'package:bunyan/ui/picker/media_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class ServicePage extends StatefulWidget {
  ServicePage({Key key}) : super(key: key);

  @override
  _ServicePageState createState() => _ServicePageState();
}

class _ServicePageState extends State<ServicePage> {
  CategoryModel _category;
  RegionModel _region;
  CityModel _city;
  List<File> _assets = [];
  List<MediaItem> _photos = [];
  ServiceModel _service = ServiceModel();
  final _formKey = GlobalKey<FormState>();

  final _priceController = TextEditingController();
  final _addressController = TextEditingController();
  final _titleController = TextEditingController();
  final _descController = TextEditingController();
  PositionModel _position;
  bool _showPhotoError = false;
  bool _showLocationError = false;
  bool _isRequesting = false;

  bool _isBanner = false;
  bool _isSpecial = false;
  bool _isNormal = false;
  bool _isLoadingLocation = false;
  LocationData mycurrentLocation;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  CameraPosition _cameraPosition;
  Completer<GoogleMapController> _mapController = Completer();
  @override
  void initState() {
    _isLoadingLocation = true;
    getCurrentLocation();
    super.initState();
  }

  getCurrentLocation() async {
    final location = Location();
    mycurrentLocation = await location.getLocation();

    initMarker(mycurrentLocation.latitude, mycurrentLocation.longitude);
    setState(() {
      _position = PositionModel(
          lat: mycurrentLocation.latitude, lng: mycurrentLocation.longitude);
      print('_position ${_position.toJson()}');
      _isLoadingLocation = false;
    });
  }

  void initMarker(lat, lang) async {
    var markerIdVal = '1';
    final MarkerId markerId = MarkerId(markerIdVal);
    final Marker marker = Marker(
      markerId: markerId,
      position: LatLng(lat, lang),
      infoWindow: InfoWindow(title: 'test'),
    );
    print('lat ${marker.position.latitude}');
    setState(() {
      markers[markerId] = marker;
      _cameraPosition = CameraPosition(target: LatLng(lat, lang), zoom: 14.0);
      //print(markerId);
    });
    final GoogleMapController controller = await _mapController.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_cameraPosition));
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              Languages.of(context).adPhoto,
              style: GoogleFonts.cairo(
                  color: Colors.black, fontWeight: FontWeight.w700),
            ),
            SizedBox(
              height: .22.sh,
              width: 1.sw,
              child: ListView.builder(
                itemCount: _photos.length + 1,
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  MediaItem photo;

                  if (index < _photos.length) photo = _photos[index];

                  return Center(
                    child: Wrap(
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 15.w,
                          ),
                          child: index == _photos.length
                              ? InkWell(
                                  onTap: _getMedium,
                                  child: Container(
                                    width: .15.sh,
                                    height: .15.sh,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          color: const Color(0XFF4d4d4d),
                                          width: .8),
                                      borderRadius: BorderRadius.circular(20.0),
                                    ),
                                    child: Center(
                                      child: Icon(
                                        Icons.add,
                                        size: 50.sp,
                                        color: const Color(0XFF4d4d4d)
                                            .withOpacity(.7),
                                      ),
                                    ),
                                  ),
                                )
                              : ClipRRect(
                                  borderRadius: BorderRadius.circular(20.0),
                                  child: photo == null
                                      ? Container(
                                          width: .15.sh,
                                          height: .15.sh,
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                                color: const Color(0XFF4d4d4d),
                                                width: .8),
                                            borderRadius:
                                                BorderRadius.circular(20.0),
                                          ),
                                          child: Center(
                                            child: CircularProgressIndicator(
                                              strokeWidth: 2.0,
                                            ),
                                          ),
                                        )
                                      : Stack(
                                          children: [
                                            Image.memory(
                                              photo.thumb,
                                              fit: BoxFit.cover,
                                              width: .15.sh,
                                              height: .15.sh,
                                            ),
                                            if (!photo.isPhoto)
                                              Positioned(
                                                top: .0,
                                                left: .0,
                                                child: Container(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            vertical: 4.h,
                                                            horizontal: 20.w),
                                                    decoration: BoxDecoration(
                                                        color: const Color(
                                                            0XFF4d4d4d),
                                                        borderRadius:
                                                            BorderRadius.only(
                                                                bottomRight: Radius
                                                                    .circular(
                                                                        20.0))),
                                                    child: Text(
                                                      'فيديو',
                                                      style: GoogleFonts.cairo(
                                                          fontSize: 15.sp,
                                                          color: Colors.white),
                                                    )),
                                              )
                                          ],
                                        ),
                                ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
            if (_showPhotoError)
              Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    Languages.of(context).adPhotoValidation,
                    style: TextStyle(color: Colors.red, fontSize: 12.0),
                  )),
            if (_showPhotoError)
              SizedBox(
                height: 35.h,
              ),
            /*
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Checkbox(
                value: _isSpecial,
                onChanged: (value) {
                  setState(() {
                    _isSpecial = value;
                  });
                }),
            Text(
              'إعلان مدفوع (مميز)',
              style: GoogleFonts.cairo(fontSize: 25.sp),
            ),
          ],
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Checkbox(
                value: _isBanner,
                onChanged: (value) {
                  setState(() {
                    _isBanner = value;
                  });
                }),
            Text(
              'إعلان مدفوع (بانر)',
              style: GoogleFonts.cairo(fontSize: 25.sp),
            ),
          ],
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Checkbox(
                value: _isNormal,
                onChanged: (value) {
                  setState(() {
                    _isNormal = value;
                  });
                }),
            Text(
              'إعلان مدفوع (عادي)',
              style: GoogleFonts.cairo(fontSize: 25.sp),
            ),
          ],
        ),
        SizedBox(
          height: 10.h,
        ),*/

            DropdownButtonFormField(
              items: Res.servicesCategories
                  .map((e) => DropdownMenuItem(
                        child: Text(e.arabicName ?? e.name),
                        value: e,
                      ))
                  .toList(),
              value: _category,
              isExpanded: true,
              onChanged: (val) {
                setState(() {
                  _category = val;
                });
              },
              validator: (e) => e == null ? 'الرجاء التأكد' : null,
              decoration: InputDecoration(
                labelText: Languages.of(context).adSubcategory,
                enabledBorder: OutlineInputBorder(),
                border: OutlineInputBorder(borderSide: BorderSide(width: .4)),
                focusedBorder:
                    OutlineInputBorder(borderSide: BorderSide(width: .4)),
                focusedErrorBorder:
                    OutlineInputBorder(borderSide: BorderSide(width: .4)),
                errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey)),
              ),
            ),
            SizedBox(
              height: 35.h,
            ),
            DropdownButtonFormField(
              items: Res.regions
                  .map((e) => DropdownMenuItem(
                        child: Text(e.arabicName),
                        value: e,
                      ))
                  .toList(),
              value: _region,
              isExpanded: true,
              onChanged: (val) {
                setState(() {
                  _region = val;
                  _city = null;
                });
              },
              validator: (e) => e == null ? 'الرجاء التأكد' : null,
              decoration: InputDecoration(
                labelText: Languages.of(context).adCity,
                enabledBorder: OutlineInputBorder(),
                border: OutlineInputBorder(),
                focusedBorder: OutlineInputBorder(),
                focusedErrorBorder: OutlineInputBorder(),
                errorBorder: OutlineInputBorder(),
              ),
            ),
            SizedBox(
              height: 35.h,
            ),
            DropdownButtonFormField(
              items: _region == null
                  ? []
                  : _region.cities
                      .map((e) => DropdownMenuItem(
                            child: Text(e.name),
                            value: e,
                          ))
                      .toList(),
              value: _city,
              isExpanded: true,
              validator: (e) =>
                  _region != null && _region.cities.isNotEmpty && e == null
                      ? 'الرجاء التأكد'
                      : null,
              onChanged: _region == null
                  ? null
                  : (val) {
                      setState(() {
                        _city = val;
                      });
                    },
              decoration: InputDecoration(
                labelText: Languages.of(context).adZone,
                enabledBorder: OutlineInputBorder(),
                border: OutlineInputBorder(),
                focusedBorder: OutlineInputBorder(),
                focusedErrorBorder: OutlineInputBorder(),
                errorBorder: OutlineInputBorder(),
              ),
            ),
            SizedBox(
              height: 35.h,
            ),
            TextFormField(
              keyboardType: TextInputType.number,
              controller: _priceController,
              validator: RequiredValidator(errorText: 'الرجاء التأكد'),
              decoration: InputDecoration(
                labelText: Languages.of(context).adPrice,
                enabledBorder: OutlineInputBorder(),
                border: OutlineInputBorder(),
                focusedBorder: OutlineInputBorder(),
                focusedErrorBorder: OutlineInputBorder(),
                errorBorder: OutlineInputBorder(),
              ),
            ),
            SizedBox(
              height: 35.h,
            ),
            SizedBox(
              width: double.infinity,
              height: 200,
              child: !_isLoadingLocation
                  ? GoogleMap(
                      onMapCreated: (GoogleMapController controller) {
                        _mapController.complete(controller);
                      },
                      initialCameraPosition: _cameraPosition,
                      markers: Set<Marker>.of(markers.values),
                      zoomControlsEnabled: false,
                      // hide location button
                      myLocationButtonEnabled: false,
                      mapType: MapType.normal,
                    )
                  : Container(),
            ),
            SizedBox(
              height: 35.h,
            ),
            Center(
              child: MaterialButton(
                minWidth: 130,
                onPressed: _isRequesting
                    ? null
                    : () async {
                        final coordinates = await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => LocationPicker(),
                          ),
                        );
                        setState(() {
                          _position = coordinates;
                          initMarker(_position.lat, _position.lng);
                        });
                      },
                child: Text(
                  Languages.of(context).adOnLocation,
                  style: GoogleFonts.cairo(
                      color: Colors.white, fontWeight: FontWeight.w700),
                ),
                color: const Color(0XFF4d4d4d),
                disabledColor: Colors.grey,
                disabledTextColor: Colors.white,
              ),
            ),
            if (_showLocationError)
              Text(
                Languages.of(context).adOnLocationValidation,
                style: TextStyle(color: Colors.red, fontSize: 12.0),
              ),
            SizedBox(
              height: 35.h,
            ),
            /*DropdownButtonFormField(
          items: [
            DropdownMenuItem(
              child: Text('عربي'),
              value: 0,
            ),
            DropdownMenuItem(
              child: Text('English'),
              value: 1,
            ),
            DropdownMenuItem(
              child: Text('عربي & English'),
              value: 2,
            ),
          ],
          value: _lang,
          isExpanded: true,
          onChanged: (val) {
            setState(() {
              _lang = val;
            });
          },
          decoration: InputDecoration(
            labelText: 'اللغة',
            enabledBorder: OutlineInputBorder(),
            border: OutlineInputBorder(),
            focusedBorder: OutlineInputBorder(),
            focusedErrorBorder: OutlineInputBorder(),
            errorBorder: OutlineInputBorder(),
          ),
        ),
        SizedBox(
          height: 35.h,
        ),*/
            TextFormField(
              textInputAction: TextInputAction.next,
              keyboardType: TextInputType.text,
              controller: _titleController,
              validator: MultiValidator([
                RequiredValidator(errorText: 'الرجاء التأكد'),
                MinLengthValidator(5, errorText: 'الرجاء التأكد')
              ]),
              decoration: InputDecoration(
                labelText: Languages.of(context).adAddress,
                enabledBorder: OutlineInputBorder(),
                border: OutlineInputBorder(),
                focusedBorder: OutlineInputBorder(),
                focusedErrorBorder: OutlineInputBorder(),
                errorBorder: OutlineInputBorder(),
              ),
            ),
            SizedBox(
              height: 35.h,
            ),
            TextFormField(
              textInputAction: TextInputAction.done,
              keyboardType: TextInputType.text,
              minLines: 2,
              maxLines: 2,
              controller: _descController,
              validator: MultiValidator([
                RequiredValidator(errorText: 'الرجاء التأكد'),
                MinLengthValidator(5, errorText: 'الرجاء التأكد')
              ]),
              decoration: InputDecoration(
                labelText: Languages.of(context).adDescription,
                enabledBorder: OutlineInputBorder(),
                border: OutlineInputBorder(),
                focusedBorder: OutlineInputBorder(),
                focusedErrorBorder: OutlineInputBorder(),
                errorBorder: OutlineInputBorder(),
              ),
            ),

            /*       if ([1, 2].contains(_lang))
          TextFormField(
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              labelText: 'Title in english',
              enabledBorder: OutlineInputBorder(),
              border: OutlineInputBorder(),
              focusedBorder: OutlineInputBorder(),
              focusedErrorBorder: OutlineInputBorder(),
              errorBorder: OutlineInputBorder(),
            ),
          ),
        if ([1, 2].contains(_lang))
          SizedBox(
            height: 35.h,
          ),

        if ([0, 2].contains(_lang))
          SizedBox(
            height: 35.h,
          ),
        if ([1, 2].contains(_lang))
          TextFormField(
            minLines: 2,
            maxLines: 2,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              labelText: 'Descriptions in english',
              enabledBorder: OutlineInputBorder(),
              border: OutlineInputBorder(),
              focusedBorder: OutlineInputBorder(),
              focusedErrorBorder: OutlineInputBorder(),
              errorBorder: OutlineInputBorder(),
            ),
          ),*/

            SizedBox(
              height: 30.h,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40),
              child: MaterialButton(
                onPressed: _isRequesting ? null : _postService,
                child: Text(
                  Languages.of(context).adAction,
                  style: GoogleFonts.cairo(
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.w700),
                ),
                color: Colors.black,
                disabledColor: Colors.grey,
                disabledTextColor: Colors.white,
                textColor: Colors.white,
                minWidth: double.infinity,
              ),
            ),
            SizedBox(
              height: .1.sh,
            )
          ],
        ));
  }

  _postService() async {
    setState(() {
      _showPhotoError = _photos.isEmpty;
      _showLocationError = _position == null;
    });
    if (_formKey.currentState.validate() &&
        _photos.isNotEmpty &&
        _position != null) {
      final product = ServiceModel(
          photos: _photos.map((e) => e.file.path).toList(),
          title: _titleController.text,
          category: _category,
          price: double.parse(_priceController.text),
          description: _descController.text,
          city: _city,
          adr: _addressController.text,
          region: _region,
          position: _position);
      setState(() {
        _isRequesting = true;
      });

      try {
        await ProductsWebService().addProduct(product, isRealEstate: false);
        _showDialog(
            text: 'تم إستقبال طلبكم بنجاح\nسيتم نشر إعلانكم بعد موافقة الإدارة',
            onTap: () {
              Res.selectedPageStream.add(0);
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => HomeScreen()));
            });
      } catch (e) {
        print(e);
        _showDialog();
      } finally {
        setState(() {
          _isRequesting = false;
        });
      }
    }
  }

  void _showDialog({String text, Function onTap}) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              content: SingleChildScrollView(
                child: Text(
                  text ?? 'خطأ في الخوادم. الرجاء المحاولة لاحقا.',
                  style: GoogleFonts.cairo(),
                ),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                      if (onTap != null) onTap();
                    },
                    child: Text(
                      'موافق',
                      style: GoogleFonts.cairo(color: Colors.teal),
                    ))
              ],
            ));
  }

  Future<void> _getMedium() async {
    final medium = await MediaPicker.getMedia(context);
    setState(() {
      _photos.add(medium);
    });
  }
}

class _MediaItem {
  final Uint8List thumb;
  final bool isPhoto;
  final File file;

  _MediaItem(this.thumb, this.isPhoto, this.file);
}
