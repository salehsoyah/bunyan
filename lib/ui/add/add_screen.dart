import 'package:bunyan/localization/language/languages.dart';
import 'package:bunyan/localization/locale_constant.dart';
import 'package:bunyan/models/region.dart';
import 'package:bunyan/tools/res.dart';
import 'package:bunyan/tools/webservices/addresses.dart';
import 'package:bunyan/tools/webservices/products.dart';
import 'package:bunyan/ui/add/real_estate_page.dart';
import 'package:bunyan/ui/add/service_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:loading_animations/loading_animations.dart';
import 'package:route_observer_mixin/route_observer_mixin.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../redirect_to_auth.dart';

class AddScreen extends StatefulWidget {
  AddScreen({Key key}) : super(key: key);

  @override
  _AddScreenState createState() => _AddScreenState();
}

class _AddScreenState extends State<AddScreen>
    with RouteAware, RouteObserverMixin {
  int _page = 0;
  int _action;
  ScrollController _scrollController = ScrollController();
  bool isLoading;
  Locale currentLang;
  var user;

  @override
  void initState() {
    isLoading = true;
    super.initState();
    getCurrentLang();

    Res.titleStream.add('إضافة إعلان');
    _scrollController.addListener(_scrollListener);
  }

  getCurrentLang() async {
    getLocale().then((locale) {
      setState(() {
        currentLang = locale;
        getData();
      });
    });
  }

  getData() async {
    final futures = await Future.wait([
      ProductsWebService().getCategories(currentLang.toString()),
      AddressesWebService().getRegions(),
      ProductsWebService().getRealEstateTypes(),
      ProductsWebService().getServicesCategories(currentLang.toString())
    ]);
    setState(() {
      isLoading = false;
    });
    Res.catgories = futures[0];
    Res.regions = futures[1];
    Res.realEstateTypes = futures[2];
    Res.servicesCategories = futures[3];
    Res.realEstateTypes.forEach((element) {
      print(element.toJson());
    });
    for (RegionModel region in Res.regions) {
      region.cities = await AddressesWebService().getCities(region.id);
    }
    final prefs = await SharedPreferences.getInstance();
    user = prefs.getString(('user'));
    print('user $user');
  }

  _scrollListener() {
    if (_scrollController.position.userScrollDirection ==
        ScrollDirection.reverse)
      Res.bottomNavBarAnimStream.add(false);
    else
      Res.bottomNavBarAnimStream.add(true);
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.removeListener(_scrollListener);
  }

  @override
  void didPopNext() {
    Res.titleStream.add('إضافة إعلان');
    super.didPopNext();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        if (_page > 0) {
          setState(() {
            _page = 0;
          });
          return Future.value(false);
        }
        return Future.value(true);
      },
      child: !isLoading
          ? Res.USER != null
              ? Container(
                  padding: EdgeInsets.symmetric(horizontal: 20.w),
                  child: SingleChildScrollView(
                    controller: _scrollController,
                    child: Column(
                      children: [
                        // Padding(
                        //   padding: const EdgeInsets.symmetric(vertical: 10),
                        //   child: DropdownButtonFormField(
                        //     items: [
                        //       DropdownMenuItem(
                        //         child: Text(Languages.of(context).realEstate),
                        //         value: 0,
                        //       ),
                        //       DropdownMenuItem(
                        //         child: Text(Languages.of(context).services),
                        //         value: 1,
                        //       ),
                        //     ],
                        //     value: _action,
                        //     isExpanded: true,
                        //     onChanged: (val) {
                        //       setState(() {
                        //         _action = val;
                        //       });
                        //     },
                        //     decoration: InputDecoration(
                        //       labelText: Languages.of(context).adType,
                        //       enabledBorder: OutlineInputBorder(),
                        //       border: OutlineInputBorder(
                        //           borderSide: BorderSide(width: .4)),
                        //       focusedBorder: OutlineInputBorder(
                        //           borderSide: BorderSide(width: .4)),
                        //       focusedErrorBorder: OutlineInputBorder(
                        //           borderSide: BorderSide(width: .4)),
                        //       errorBorder: OutlineInputBorder(
                        //           borderSide: BorderSide(color: Colors.grey)),
                        //     ),
                        //   ),
                        // ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              GestureDetector(
                                onTap: () => setState(() {
                                  _action = 0;
                                }),
                                child: Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.42,
                                  alignment: Alignment.center,
                                  margin:
                                      const EdgeInsets.symmetric(horizontal: 5),
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 20, horizontal: 10),
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.black),
                                      color: _action == 0
                                          ? Theme.of(context).primaryColor
                                          : Colors.white,
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(20))),
                                  child: Column(
                                    children: [
                                      SvgPicture.asset(
                                        'assets/icons/new_ad_property.svg',
                                        width: 48,
                                        color: _action == 0
                                            ? Colors.white
                                            : const Color(0XFF4d4d4d),
                                      ),
                                      Text(
                                        Languages.of(context).realEstate,
                                        style: GoogleFonts.cairo(
                                          color: _action == 0
                                              ? Colors.white
                                              : const Color(0XFF4d4d4d),
                                          fontSize: 18,
                                          fontWeight: FontWeight.w900,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () => setState(() {
                                  _action = 1;
                                }),
                                child: Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.42,
                                  alignment: Alignment.center,
                                  margin:
                                      const EdgeInsets.symmetric(horizontal: 5),
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 20, horizontal: 10),
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.black),
                                      color: _action == 1
                                          ? Theme.of(context).primaryColor
                                          : Colors.white,
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(20))),
                                  child: Column(
                                    children: [
                                      SvgPicture.asset(
                                        'assets/icons/new_ad_services.svg',
                                        width: 48,
                                        color: _action == 1
                                            ? Colors.white
                                            : const Color(0XFF4d4d4d),
                                      ),
                                      Text(
                                        Languages.of(context).services,
                                        style: GoogleFonts.cairo(
                                          color: _action == 1
                                              ? Colors.white
                                              : const Color(0XFF4d4d4d),
                                          fontSize: 18,
                                          fontWeight: FontWeight.w900,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10.h,
                        ),
                        if (_action == 0) RealEstatePage(),
                        if (_action == 1) ServicePage(),
                      ],
                    ),
                  ),
                )
              : RedirectToAuth(
                  destination: 'ads',
                )
          : Center(
              child: LoadingBouncingGrid.square(
                backgroundColor: Color(0xffd6d6d6),
              ),
            ),
    );
  }
}
