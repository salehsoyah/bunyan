import 'package:flutter/material.dart';

class ProfileInfo extends StatefulWidget {
  final String imagePath;
  final bool isMyProfile;

  ProfileInfo(this.imagePath, this.isMyProfile);

  @override
  _ProfileInfoState createState() => _ProfileInfoState();
}

class _ProfileInfoState extends State<ProfileInfo> {
  var image;
  @override
  void initState() {
    setState(() {
      image = NetworkImage(widget.imagePath);
      print(image);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final color = Theme.of(context).primaryColor;

    return Center(
      child: Stack(
        children: [
          buildImage(),
              widget.isMyProfile ? Positioned(
                  bottom: 0,
                  right: 4,
                  child: buildEditIcon(color)
                ): Container(),

        ],
      ),
    );
  }

  Widget buildImage() {
    return ClipOval(
      child: Container(
        width: 129.0,
        height: 129.0,
        decoration: new BoxDecoration(
          color: const Color(0xff7c94b6),
          image: new DecorationImage(
            image: image,
            fit: BoxFit.cover,
          ),
          borderRadius: new BorderRadius.all(new Radius.circular(150.0)),
          boxShadow: [
            BoxShadow(
              offset: Offset(0.7, 1.2),
              blurRadius: 6,
              color: Color.fromRGBO(0, 0, 0, 0.45),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildEditIcon(Color color) => buildCircle(
        color: Colors.white,
        all: 3,
        child: buildCircle(
          color: color,
          all: 8,
          child: Icon(
            Icons.camera_alt,
            color: Colors.white,
            size: 20,
          ),
        ),
      );

  Widget buildCircle({
    Widget child,
    double all,
    Color color,
  }) =>
      ClipOval(
        child: Container(
          padding: EdgeInsets.all(all),
          color: color,
          child: child,
        ),
      );
}
