import 'package:bunyan/tools/res.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:route_observer_mixin/route_observer_mixin.dart';


class AboutScreen extends StatefulWidget {
  AboutScreen({Key key}) : super(key: key);

  @override
  _AboutScreenState createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> with RouteAware, RouteObserverMixin {

  @override
  void didPopNext() {
    print('pop');
    Res.titleStream.add('عن بنيان');
    super.didPopNext();
  }

  @override
  void didPush() {
    // TODO: implement didPush
    super.didPush();
    Res.titleStream.add('عن بنيان');
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 30.h, horizontal: 40.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          """
أهلا بكم في شركة بنيان للتسويق العقاريشركة بنيانللتسويقالعقاري هي شركة قطريةمتخصصة بالعقاراتفي قطر, مقرها في الوسيلومرخصة بموجب قانون نظام الشركات القطري , ولديها تعاقد واتفاقيات مع أكبر الشركات العقاريةفي قطرتأسست شركة بنيانللتسويقالعقاري على يد طاقم مهني متخصص في قطاع الإستثمار والتسويق العقاري, لتجذب جميع الأنظار اليها لتصبح إحدى الشركات الرائدة والموثوق فيها بهذا المجالمن الجدير الذكر ان نشاط بنيانللتسويقالعقاري هو نشاط واسع النطاق, فنحن نقوم على تسويق مشاريع داخل قطرويشمل نشاطنا كافة أنواع العقارات مثل-الشقق الخاصة بأنواعها ( الشقق الذكية /الحديثة / المطلة على البحر / القريبة من المركز /الدوبلكس وغيرها)..-الفلل الفاخرة بأنواعها وأحجامها المختلفة ( فلل مطلة على البحر / فلل مستقلة بشكل كامل / وفلل للإستثمار)الأراضي الزراعية والأراضي المعدة للبناء-المتاجر الكبيرة والصغيرة ( محلات تجارية / مكاتب / مخازن وغيرها)..-تسعى شركة بنيانللتسويقالعقاري وكل فرد من أفراد طاقمها في تقديم أفضل الفرص العقاريةالأقرب والأنسب لطلبات عملائهاتقدم شركة بنيانللتسويقالعقاري خدمات اضافية متعلقة بالعقارات وإدارة الممتلكات بشكل كامل وشامل , للحصول على معلومات اضافية عن خدماتنا نرجو الضغط هنارؤية بنيانللتسويق العقاريشركة بنيانللتسويقالعقاري تؤمن بأن نجاحنا هو ثمرة الممارسات الشفافة التي هي أساس تعاملنا مع عملائنا والتي تتماشى مع معنى كلمة بنيان
رؤية بنيانللتسويقالعقاري هي أن تكون الشركة الأولى الرائدة في تقديم الخدمات العقاريةالمبتكرة في قطاع العقار القطريبما يتماشى مع احتياجات المستثمرين سواء كانوا من المؤسسات أو الأفراد , الأمر الذي يساعد في تحويل قطرالىالوجهة الأولى للمستثمرين الأجانبفي المجال العقاريًاعيرسًاعسوتاندهشدقلفي التسويقالعقاري وعليه نقوم ايضا بنشر آخر الأخبار والمعلومات العقاريةالمتعلقة بذلك.اليوم نحن فخورين بتأمين إحتياجات العملاء على نطاق فردي أو مؤسساتييكمن هدفنا الأساسي في تقديم أرقى خدمة ممكنة لكل عملائنا والعمل على إرضائهم من خلال دراسة أهدافهم بدقة وبتمحيص كبييرين لإختيار العقار المناسب لهم, من خلال نشر المعرفة والخبرة بكل الطرق المتاحةوفي النهاية نحن ننظر إلى الفرص المستقبلية بشغف ونعدكم بتقديم أرقى الخدمات العقاريةالتي تساعد المستثمرين فيقطاع العقار القطري
""",
          style: GoogleFonts.cairo(),
        ),

        SizedBox(height: 30.h,),

        Text('تابعنا على', style: GoogleFonts.cairo(fontSize: 40.sp),),

        SizedBox(height: 50.h,),

        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Icon(FontAwesome.instagram),
            Icon(FontAwesome.facebook_official),
          ],
        ),

        SizedBox(height: .13.sh,),
      ],
    )));
  }
}
