import 'package:bunyan/localization/language/languages.dart';
import 'package:bunyan/models/notification.dart';
import 'package:bunyan/tools/res.dart';
import 'package:bunyan/tools/webservices/notifications.dart';
import 'package:clay_containers/clay_containers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:loading_animations/loading_animations.dart';
import 'package:route_observer_mixin/route_observer_mixin.dart';
import 'package:shimmer/shimmer.dart';

class NotificationsScreen extends StatefulWidget {
  NotificationsScreen({Key key}) : super(key: key);

  @override
  _NotificationsScreenState createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen>
    with RouteAware, RouteObserverMixin {
  bool _isLoading = true;
  List<NotificationModel> _notifs;

  @override
  void initState() {
    super.initState();
    Res.titleStream.add('التنبيهات');

    NotificationsWebService().getNotifications().then((notifs) {
      setState(() {
        _isLoading = false;
        _notifs = notifs;
      });
    });
  }

  @override
  void didPopNext() {
    Res.titleStream.add('التنبيهات');
    super.didPopNext();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: !_isLoading
          ? IndexedStack(
              index: _isLoading
                  ? 0
                  : _notifs != null && _notifs.isEmpty
                      ? 1
                      : 2,
              children: [
                Center(
                  child: CircularProgressIndicator(),
                ),
                Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(
                        Icons.notifications_off_outlined,
                        color: Color(0xffd6d6d6),
                        size: 90.sp,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: ClayText(
                          Languages.of(context).emptyNotifications,
                          style: GoogleFonts.cairo(
                            fontSize: 40.sp,
                            fontWeight: FontWeight.bold,
                          ),
                          depth: -10,
                          textColor: Color(0xffd6d6d6),
                        ),
                      ),
                    ],
                  ),
                ),
                ListView.builder(
                  padding:
                      EdgeInsets.symmetric(horizontal: 15.h, vertical: 30.h),
                  itemCount: _notifs.length,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return Stack(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(right: .045.sh),
                          child: Card(
                            elevation: 5.0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0)),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10.0),
                              child: Container(
                                height: .15.sh,
                                width: double.infinity,
                                padding: EdgeInsets.symmetric(vertical: 15.h),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      width: .1.sh,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          width: .6.sw,
                                          child: Text(
                                            _notifs[index].title,
                                            style: GoogleFonts.cairo(
                                                fontSize: 25.sp,
                                                color: Colors.black),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                            horizontal: 22.w,
                                          ),
                                          child: Text(
                                            '27/05/2020 14:56',
                                            style: GoogleFonts.cairo(
                                                color: Colors.grey,
                                                fontSize: 20.sp),
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          right: .0,
                          child: SizedBox(
                            height: .15.sh,
                            child: Center(
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(500.0)),
                                elevation: 3.0,
                                child: SizedBox(
                                  height: .115.sh,
                                  width: .115.sh,
                                  child: Stack(
                                    children: [
                                      Positioned.fill(
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(50.0),
                                          child: Container(
                                            color: Colors.grey,
                                            child: TransitionToImage(
                                                image: AdvancedNetworkImage(
                                                    _notifs[index]
                                                            .product
                                                            .photos
                                                            .isNotEmpty
                                                        ? _notifs[index]
                                                            .product
                                                            .photos
                                                            .first
                                                        : ''),
                                                height: .08.sh,
                                                loadingWidget: _shimmer(
                                                    width: .08.sh,
                                                    height: .08.sh),
                                                placeholder: Container(
                                                  width: .08.sh,
                                                  height: .08.sh,
                                                  child: Icon(Icons
                                                      .broken_image_outlined),
                                                ),
                                                fit: BoxFit.cover,
                                                width: .08.sh),
                                          ),
                                        ),
                                      ),
                                      Positioned(
                                        bottom: .0,
                                        right: .0,
                                        child: Container(
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(5000.0),
                                              color: Colors.white),
                                          padding: EdgeInsets.all(2.0),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(5000.0),
                                            child: Container(
                                              color: Colors.grey,
                                              child: TransitionToImage(
                                                  image: AdvancedNetworkImage(
                                                      _notifs[index]
                                                              .owner
                                                              .photo ??
                                                          ''),
                                                  loadingWidget: _shimmer(
                                                      width: .035.sh,
                                                      height: .035.sh),
                                                  placeholder: Container(
                                                    width: .035.sh,
                                                    height: .035.sh,
                                                    child: Icon(
                                                      Icons
                                                          .broken_image_outlined,
                                                      size: .02.sh,
                                                    ),
                                                  ),
                                                  height: .035.sh,
                                                  fit: BoxFit.cover,
                                                  width: .035.sh),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    );
                  },
                ),
              ],
            )
          : Center(
              child: LoadingBouncingGrid.square(
                backgroundColor: Color(0xffd6d6d6),
              ),
            ),
    );
  }

  Widget _shimmer({double width, double height}) {
    return Shimmer.fromColors(
        child: Container(
          width: width,
          height: height,
          color: Colors.grey,
        ),
        baseColor: Colors.grey.withOpacity(.5),
        highlightColor: Colors.white);
  }
}
