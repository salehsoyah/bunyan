import 'dart:ui';

import 'package:bunyan/tools/res.dart';
import 'package:bunyan/ui/about/about_screen.dart';
import 'package:bunyan/ui/add/add_screen.dart';
import 'package:bunyan/ui/enterprises/enterprises_screen.dart';
import 'package:bunyan/ui/news/news_screen.dart';
import 'package:bunyan/ui/real_estates/real_estates_screen.dart';
import 'package:bunyan/ui/services/services_screen.dart';
import 'package:bunyan/ui/splash_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class MainDrawer extends StatelessWidget {
  final BuildContext ctx;

  const MainDrawer({Key key, this.ctx}) : super(key: key);

  /*getCurrentLocation() async {
    final location = Location();
    mycurrentLocation = await location.getLocation();
  }*/
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: .65.sw,
      child: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.transparent,
        ),
        child: Drawer(
          child: Container(
            color: Color(0xff303030).withOpacity(.3),
            child: ClipRRect(
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 20.0, sigmaY: 20.0),
                child: Container(
                  height: 1.sh,
                  width: .65.sw,
                  color: Colors.white30,
                  child: SingleChildScrollView(
                    child: SafeArea(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ListTile(
                            title: Text(
                              'القائمة الرئيسية',
                              textAlign: TextAlign.center,
                              style: GoogleFonts.cairo(color: Colors.white),
                            ),
                            tileColor: Colors.blue,
                          ),
                          ListTile(
                            onTap: () {
                              if(Res.USER==null){
//_display_dialog('Login required for this action');
                              //showCupertinoModalPopup(context: context, builder: (context)=>'good');

                                Navigator.of(context).restorablePush(_modalBuilder);
                              }else{
                                Navigator.push(
                                    ctx,
                                    MaterialPageRoute(
                                        builder: (context) => AddScreen()));
                                Navigator.pop(context);
                              }
                            },
                            title: Text(
                              'اضف إعلانك',
                              style: GoogleFonts.cairo(color: Colors.white),
                            ),
                            leading: Icon(
                              Icons.add,
                              color: Colors.white,
                            ),
                          ),
                          /*ListTile(
                            title: Text(
                              'تغيير اللغة',
                              style: GoogleFonts.cairo(color: Colors.white),
                            ),
                            leading: Icon(
                              Icons.translate,
                              color: Colors.white,
                            ),
                          ),*/
                          ListTile(
                            title: Text(
                              'شارك بنيان',
                              style: GoogleFonts.cairo(color: Colors.white),
                            ),
                            leading: Icon(
                              Icons.share,
                              color: Colors.white,
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.push(
                                  ctx,
                                  MaterialPageRoute(
                                      builder: (context) => AboutScreen()));
                              Navigator.pop(context);
                            },
                            child: ListTile(
                              title: Text(
                                'عن بنيان',
                                style: GoogleFonts.cairo(color: Colors.white),
                              ),
                              leading: Icon(
                                Icons.info_outline,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          ListTile(
                            onTap: () => launch('mailto:contact@bunyan.qa'),
                            title: Text(
                              'إتصل بنا',
                              style: GoogleFonts.cairo(color: Colors.white),
                            ),
                            leading: Icon(
                              Icons.mail_outline,
                              color: Colors.white,
                            ),
                          ),

                          /*ListTile(
                            onTap: () => launch('mailto:contact@bunyan.com'),
                            title: Text(
                              'وظائف',
                              style: GoogleFonts.cairo(color: Colors.white),
                            ),
                            leading: Icon(
                              Icons.work_outline,
                              color: Colors.white,
                            ),
                          ),*/

                          ListTile(
                            onTap: () async {
                              if(Res.USER!=null){
                                final prefs = await SharedPreferences.getInstance();
                                await prefs.remove('user');
                                Res.USER = null;
                              }
                              Navigator.pushAndRemoveUntil(
                                  Res.mainContext,
                                  MaterialPageRoute(
                                      builder: (context) => SplashScreen()), (route) => false);

                            },
                            title: Text(
                              Res.USER!=null ? 'تسجيل الخروج' : 'تسجيل الدخول',
                              style: GoogleFonts.cairo(color: Colors.white),
                            ),
                            leading: Icon(
                              Icons.power_settings_new,
                              color: Colors.white,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 40.w, vertical: 20.h),
                            child: Text(
                              'الفئات',
                              style: GoogleFonts.cairo(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 30.sp),
                              textAlign: TextAlign.start,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 10.h),
                            child: ListTile(
                              onTap: () {
                                Navigator.push(
                                    ctx,
                                    MaterialPageRoute(
                                        builder: (context) => RealEstatesScreen()));
                                Navigator.pop(context);
                              },
                              title: Text(
                                'عقارات',
                                style: GoogleFonts.cairo(color: Colors.white),
                              ),
                              tileColor: Colors.grey.withOpacity(.5),
                              leading: Container(
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Color(0xffb5af00),
                                  ),
                                  padding: EdgeInsets.all(15.sp),
                                  child: Icon(
                                    Icons.home,
                                    color: Colors.white,
                                    size: 25.sp,
                                  )),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 10.h),
                            child: ListTile(
                              onTap: () {
                                Navigator.push(
                                    ctx,
                                    MaterialPageRoute(
                                        builder: (context) => ServicesScreen()));
                                Navigator.pop(context);
                              },
                              title: Text(
                                'خدمات',
                                style: GoogleFonts.cairo(color: Colors.white),
                              ),
                              tileColor: Colors.grey.withOpacity(.5),
                              leading: Container(
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.purple,
                                  ),
                                  padding: EdgeInsets.all(15.sp),
                                  child: Icon(
                                    Icons.work,
                                    color: Colors.white,
                                    size: 25.sp,
                                  )),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 10.h),
                            child: ListTile(
                              onTap: () {
                                Navigator.push(
                                    ctx,
                                    MaterialPageRoute(
                                        builder: (context) => EnterprisesScreen()));
                                Navigator.pop(context);
                              },
                              title: Text(
                                'شركات',
                                style: GoogleFonts.cairo(color: Colors.white),
                              ),
                              tileColor: Colors.grey.withOpacity(.5),
                              leading: Container(
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.pink,
                                  ),
                                  padding: EdgeInsets.all(15.sp),
                                  child: Icon(
                                    Icons.location_city,
                                    color: Colors.white,
                                    size: 25.sp,
                                  )),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 10.h),
                            child: ListTile(
                              onTap: () {
                                Navigator.push(
                                    ctx,
                                    MaterialPageRoute(
                                        builder: (context) => NewsScreen()));
                                Navigator.pop(context);
                              },
                              title: Text(
                                'اخبار',
                                style: GoogleFonts.cairo(color: Colors.white),
                              ),
                              tileColor: Colors.grey.withOpacity(.5),
                              leading: Container(
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.deepOrange,
                                  ),
                                  padding: EdgeInsets.all(15.sp),
                                  child: Icon(
                                    Icons.map,
                                    color: Colors.white,
                                    size: 25.sp,
                                  )),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  /*_display_dialog(String text){
    return showDialog<String>(
        barrierDismissible: true, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
              title: const Text('AlertDialog Title'),
              content: Text(text),actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, 'Cancel'),
              child: const Text('Ok'),
            )
          ],
          );
        });
  }*/
  static Route<void> _modalBuilder(BuildContext context, Object arguments) {
    return CupertinoModalPopupRoute<void>(
      builder: (BuildContext context) {
        return CupertinoActionSheet(
          title: const Text(''),
          message: Text('عليك فتح حساب للاستخدام'),
          actions: <CupertinoActionSheetAction>[
            CupertinoActionSheetAction(
              child: const Text('Close'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
           /* CupertinoActionSheetAction(
              child: const Text('Action Two'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),*/
          ],
        );
      },
    );
  }
}

