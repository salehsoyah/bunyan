import 'languages.dart';

class LanguageAr extends Languages {
  @override
  String get enterLabel => "أدخل";

  @override
  String get confirm => "تأكيد";

  @override
  String get mobileNumber => "رقم الهاتف المحمول";

  @override
  String get email => "البريد الإلكتروني";

  @override
  String get userName => "اسم االمستخدم";

  @override
  String get signUp => "انشأ حساب";

  @override
  String get currence => "رق";

  @override
  String get save => "حفظ";

  @override
  String get before => "قبل";

  @override
  String get next => "التالي";

  @override
  String get send => "إرسال";

  @override
  String get non => "غير";

  @override
  String get tryAgain => "حاول مرة أخرى";

  @override
  String get loader => "جاري التحميل...";

  @override
  String get exist => "إضغط مرة اخرى للخروج";

  @override
  String get showMore => "عرض المزيد";

  @override
  String get showLess => "عرض اقل";

  @override
  String get today => "اليوم";

  @override
  String get days => "ايام";

  @override
  String get emptyValidator => "الرجاء ادخل";

  @override
  String get pickertitle => "إختر صورة او فيديو";

  @override
  String get follow => "تابع";

  @override
  String get unFollow => "إلغاء الماتبعة";

  @override
  String get buildingNumber => "رقم المبنى";

  @override
  String get streetNumber => "رقم الشارع";

  @override
  String get zone => "المنطقة";

  @override
  String get max => "بحد اقصى";

  @override
  String get word => "حرف";

  @override
  String get noAds => "لا يوجد إعلانات";

  @override
  String get makesureoftheinformation => "الرجاء التأكد من صحة المعلومات";

  @override
  String get back => "عودة";

  @override
  String get chekconnection => "الرجاء التأكد من الشبكة";

  @override
  String get ServererrorPleasetryagainlater =>
      "خطأ في الخوادم. الرجاء المحاولة لاحقا.";

  @override
  String get agreeon => "موافق";

  @override
  String get Pleaseenteravalidmail => "الرجاء إدخال بريد صحيح'";

  @override
  String get Pleaseenteravalidmobilenumber => "الرجاء إدخال رقم جوال صحيح";

  @override
  String get activateacount => "تفعيل الحساب";

  @override
  String get pleasegoemail =>
      "الرجاء التوجه للبريد الإلكتروني لإيجاد رمز الفعيل";

// **********************Filter **********************//
  @override
  String get realEstate => "العقارات";

  @override
  String get services => "الخدمات";

  @override
  String get agencies => "شركات";

  @override
  String get news => "اخبار";

  @override
  String get searchPlaceholder => "ابحث في كل الاقسام";

  @override
  String get search => "ابحث";

// ********************** Bottom Menu App **********************//
  @override
  String get menuHome => "الرئيسية";

  @override
  String get menuFavorite => "المفضلة";

  @override
  String get menuNotifications => "الإشعارات";

  @override
  String get menuProfile => "الملف الشخصي";

  // ********************** Login Page **********************//
  @override
  String get loginPassword => "كلمة المرور";

  @override
  String get loginForgetPwd => "هل نسيت كلمة السر؟";

  @override
  String get resetPassword => "إعادة تعيين كلمة المرور";

  @override
  String get loginSignIn => "تسجيل الدخول";

  @override
  String get loginAccount => "ليس لديك حساب؟";

  @override
  String get loginAsGuest => "الدخول كظيف";

  @override
  String get register => "إنشاء حساب";

  @override
  String get registerAsPerson => "شخص";

  @override
  String get registerAsCompany => "شركة";

  // ********************** Sign Up Page **********************//

  @override
  String get registerTerms => "اوافق على سياسة الخصوصية وشروط الاستخدام";

  @override
  String get createPassword => "إنشاء كلمة المرور";

  @override
  String get confirmPassword => "تأكيد كلمة المرور";

  @override
  String get registerPhoto => "اضف الصورة الشخصية";

  @override
  String get registerCRnumber => "رقم السجل التجاري";

  @override
  String get registerPasswordValidator => "كلمة السر متكونة من 6 إلى 16 احرف";

  @override
  String get registerEmployeDetails => "تفاصيل الموظف";

  @override
  String get registerEmployeName => "إسم الموظف";

  @override
  String get registerEmployePosition => "الوظيفة";

  @override
  String get registerCompanyDetails => "تفاصيل الشركة";

  @override
  String get registerCompanyName => "إسم الشركة";

  @override
  String get registerCompanyLogo => "شعار الشركة";

  @override
  String get registerEmployePhoto => "الصورة الشخصية";

  @override
  String get registerDetails => "البيانات";

  @override
  String get registerPhotos => "الصور";

  // ********************** Forget Password Page **********************//
  @override
  String get forgetTitle => "نسيت كلمة المرور";

  @override
  String get forgetWelcome =>
      "الرجاء إدخال رقم هاتفك أدناه ، ثم سنرسل رمز OTP للتحقق.";

  @override
  String get forgetSend => "إرسال";

  // ********************** OTP Page **********************//
  @override
  String get otpTitle => "التحقق من OTP";

  @override
  String get otpSubTitle => "لقد أرسلنا لك رسالة نصية قصيرة تحتوي على رمز إلى";

  @override
  String get otpNoCode => "لم تتلق رمز؟";

  @override
  String get otpResendCode => "أعد إرسال الرمز";

  // ********************** Product Details Page **********************//
  @override
  String get productDetailsFavorite => "أزل من المفضلة";

  @override
  String get productDetailsUnFavorite => "أضف للمفضلة";

  @override
  String get productDetailsShare => "شارك";

  @override
  String get productDetailsRoom => "غرف";

  @override
  String get productDetailsBathRoom => "حمامات";

  @override
  String get productDetailsFurniched => "مؤثث";

  @override
  String get productDetailsUnFurniched => "غير مؤثث";

  @override
  String get productDetailsDescrption => "وصف";

  @override
  String get productDetailsID => "معرف الإعلان";

  @override
  String get productDetailsReport => "بلغ عن الإعلان";

  @override
  String get productDetailsShowOnMap => "عرض على الخريطة";

  @override
  String get productDetailsFollowers => "متابع";

  @override
  String get productDetailsFollow => "تابع";

  @override
  String get productDetailsCall => "إتصل";

  @override
  String get productDetailsCallAdvertiser => "إتصل بالمعلن عن طريق";

  @override
  String get productDetailsCallWhats => "الواتساب";

  @override
  String get productDetailsCallPhone => "الهاتف";

  @override
  String get productDetailsCallChat => "الدردشة";

  @override
  String get productDetailsMoreAds => "المزيد من الإعلانات من نفس المعلن";

  // ********************** Profile Page **********************//
  @override
  String get labelSelectLanguage => "العربية";

  @override
  String get appLanguage => "لغة التطبيق";

  @override
  String get profileWorkOn => "موظف في:";

  @override
  String get editPhotoProfileSuccess => "تم تغيير الصورة الشخصية بنجاح";

  @override
  String get logout => "تسجيل خروج";

  // ********************** ModalBottomSheet **********************//
  @override
  String get modalBottomSheetPhotoLibrary => "مكتبة الصور";

  @override
  String get modalBottomSheetCamera => "كاميرا";

  // ********************** Add Ad Page **********************//
  @override
  String get adType => "إعلان عن";

  @override
  String get adPhoto => "الصور";

  @override
  String get adPhotoValidation => "الرجاء إضافة الصّور";

  @override
  String get adCatogory => "نوع الإعلان";

  @override
  String get adSubcategory => "الفئة الفرعية";

  @override
  String get adCity => "المدينة";

  @override
  String get adZone => "المنطقة";

  @override
  String get adFurnishing => "التأثيث";

  @override
  String get adVideo => "فيديو";

  @override
  String get adSwimming => "حوض سباحة";

  @override
  String get adWithSwimming => "مع حوض سباحة";

  @override
  String get adWithoutSwimming => "بدون حوض سباحة";

  @override
  String get adRoomNumber => "عدد الغرف";

  @override
  String get adBathRoomNumber => "عدد الحمّامات";

  @override
  String get adSpace => "المساحة (متر)";

  @override
  String get adPrice => "السعر";

  @override
  String get adLocation => "المكان";

  @override
  String get adOnLocation => "حدد على الخريطة";

  @override
  String get adOnLocationValidation => "الرجاء حدد على الخريطة";

  @override
  String get adAddress => "العنوان";

  @override
  String get adDescription => "التفاصيل";

  @override
  String get adAction => "اضف الإعلان";

  @override
  String get about => "حول";

  @override
  String get editprofile => "تعديل الملف الشخصي";

  @override
  String get profile => "الملف الشخصي";

  @override
  String get realstate => "العقارات";

  @override
  String get home => "الصفحة الرئيسية";
  @override
  String get regions=> "المناطق";
  @override
  String get advsearoption=> "خيار البحث المتقدم";
  @override
  String get cities=> "مدن";
  @override
  String get rooms=> "غرف";
  @override
  String get baths=> "الحمامات";
  @override
  String get price=> "السعر";
  @override
  String get from=> "من";
  @override
  String get to=> "إلى";
  @override
  String get furnishing=> "تأثيث";
  @override
  String get searchagence=> "ابحث عن وكالة";


  // ********************** Real State Page **********************//
  @override
  String get employeeAccomodation => "سكن عمال";

  @override
  String get apartments => "شقق سكنية";

  @override
  String get villa => "فلل سكنية";

  @override
  String get depot => "مستودعات";

  // ********************** Favorite Page **********************//
  @override
  String get noFavorite => "لا يوجد اعلان في المفضلة";

  // ********************** Chat Page **********************//
  @override
  String get noChats => "لا يوجد رسائل";

  // ********************** Redirect to Auth Page **********************//
  @override
  String get redirectToAuthMessage => "يجب عليك تسجيل الدخول للمتابعة";

  @override
  String get emptyNotifications => "لا إشعارات لك حتى الآن";

  // ********************** Enterprises Page **********************//
  String get searchForAnAgency => "ابحث عن وكالة";

}
