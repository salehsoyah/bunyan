import 'languages.dart';

class LanguageEn extends Languages {
  @override
  String get enterLabel => "Enter";
  @override
  String get confirm => "Confirm";
  @override
  String get mobileNumber => "Mobile Number";
  @override
  String get email => "Email";
  @override
  String get userName => "User Name";
  @override
  String get signUp => "Sign Up";
  @override
  String get currence => "QAR";
  @override
  String get save => "Save";
  @override
  String get before => "Before";
  @override
  String get next => "Next";
  @override
  String get send => "Send";
  @override
  String get non => "Non";
  @override
  String get tryAgain => "Try Again";
  @override
  String get loader => "Loading...";
  @override
  String get exist => "Click again to exit";
  @override
  String get showMore => "Show More";
  @override
  String get showLess => "Show Less";
  @override
  String get today => "Today";
  @override
  String get days => "Days";
  @override
  String get emptyValidator => "Please enter";
  @override
  String get pickertitle => "Choose a photo or video";
  @override
  String get follow => "Follow";
  @override
  String get unFollow => "unFollow";
  @override
  String get buildingNumber => "Building Number";
  @override
  String get streetNumber => "Street Number";
  @override
  String get zone => "Zone";
  @override
  String get max => "Maximum";
  @override
  String get word => "Word (s)";
  @override
  String get noAds => "No Ads";
  @override
  String get makesureoftheinformation => "Please make sure the information is correct";
  @override
  String get back => "Back";
  @override
  String get chekconnection => "Please check the network";
  @override
  String get ServererrorPleasetryagainlater => "Server error. Please try again later.";
  @override
  String get agreeon => "Agree On";
  @override
  String get Pleaseenteravalidmail=> "Please enter a valid mail";
  @override
  String get Pleaseenteravalidmobilenumber=> "Please enter a valid mobile number";
  @override
  String get activateacount=> "Activate the account";
  @override
  String get pleasegoemail=> "Please go to the email to find the activation code";

// **********************Filter **********************//
  @override
  String get realEstate => "REAL ESTATE";
  @override
  String get services => "SERVICES";
  @override
  String get agencies => "AGENCIES";
  @override
  String get news => "NEWS";
  @override
  String get searchPlaceholder => "search all ads";
  @override
  String get search => "Search";

  // ********************** Bottom Menu App **********************//
  @override
  String get menuHome => "Home";
  @override
  String get menuFavorite => "favorites";
  @override
  String get menuNotifications => "Notifications";
  @override
  String get menuProfile => "Profile";

  // ********************** Login Page **********************//
  @override
  String get loginPassword => "Password";
  @override
  String get loginForgetPwd => "Forget password?";
  @override
  String get resetPassword => "Reset password";
  @override
  String get loginSignIn => "Sign In";
  @override
  String get loginAccount => "Don\'t have an account?";
  @override
  String get loginAsGuest => "Login as guest";
  @override
  String get register => "Create Account";
  @override
  String get registerAsPerson => "Person";
  @override
  String get registerAsCompany => "Company";

  // ********************** Sign Up Page **********************//
  @override
  String get registerTerms => "I agree to the Privacy Policy and Terms of Use";
  @override
  String get createPassword => "Create Password";
  @override
  String get confirmPassword => "Confirm Password";
  @override
  String get registerPhoto => "Add a profile picture";
  @override
  String get registerCRnumber => "Commercial Register Number";
  @override
  String get registerPasswordValidator =>
      "The password is from 6 to 16 characters";
  @override
  String get registerEmployeDetails => "Employee details";
  @override
  String get registerEmployeName => "Employee Name";
  @override
  String get registerEmployePosition => "Position";
  @override
  String get registerCompanyDetails => "Company Details";
  @override
  String get registerCompanyName => "Company Name";
  @override
  String get registerCompanyLogo => "Company Logo";
  @override
  String get registerEmployePhoto => "Photo Profile";
  @override
  String get registerDetails => "Details";
  @override
  String get registerPhotos => "Photos";

  // ********************** Forget Password Page **********************//
  @override
  String get forgetTitle => "Forget Password";
  @override
  String get forgetWelcome =>
      "Please enter your phone number below, then we will send OTP code to verify.";
  @override
  String get forgetSend => "Send";

  // ********************** OTP Page **********************//
  @override
  String get otpTitle => "OTP Verification";
  @override
  String get otpSubTitle => "We have sent you an SMS with a code to";
  @override
  String get otpNoCode => "Didn't receive code?";
  @override
  String get otpResendCode => "Resend Code";

  // ********************** Product Details Page **********************//
  @override
  String get productDetailsFavorite => "Add To Favorite";
  @override
  String get productDetailsUnFavorite => "Remove from Favorite";
  @override
  String get productDetailsShare => "Share";
  @override
  String get productDetailsRoom => "Rooms";
  @override
  String get productDetailsBathRoom => "Bath Rooms";
  @override
  String get productDetailsFurniched => "Furniched";
  @override
  String get productDetailsUnFurniched => "UnFurniched";
  @override
  String get productDetailsDescrption => "Descrption";
  @override
  String get productDetailsID => "Prodcut Reference";
  @override
  String get productDetailsReport => "Report the ad";
  @override
  String get productDetailsShowOnMap => "Show on map";
  @override
  String get productDetailsFollowers => "Followers";
  @override
  String get productDetailsFollow => "Follow";
  @override
  String get productDetailsCall => "Call";
  @override
  String get productDetailsCallAdvertiser => "Call Adviser with";
  @override
  String get productDetailsCallWhats => "Whatsapp";
  @override
  String get productDetailsCallPhone => "Phone";
  @override
  String get productDetailsCallChat => "Chat";
  @override
  String get productDetailsMoreAds => "More ads from the same advertiser";
  @override
  String get about=> "About";
  @override
  String get editprofile=> "Edit profile";
  @override
  String get profile=> "Profile";
  @override
  String get realstate=> "Real State";
  @override
  String get add=> "Add";
  @override
  String get home=> "Home";
  @override
  String get regions=> "Regions";
  @override
  String get advsearoption=> "Advanced Search Option";
  @override
  String get cities=> "Cities";
  @override
  String get rooms=> "Rooms";
  @override
  String get baths=> "Baths";
  @override
  String get price=> "Price";
  @override
  String get from=> "From";
  @override
  String get to=> "To";
  @override
  String get furnishing=> "Furnishing";
  @override
  String get searchagence=> "Search for an agency";



  // ********************** Profile Page **********************//
  @override
  String get labelSelectLanguage => "English";
  @override
  String get appLanguage => "Application language";
  @override
  String get profileWorkOn => "Employee in:";
  @override
  String get editPhotoProfileSuccess =>
      "Photo profile has been successfully changed";
  @override
  String get logout => "Log Out";

  // ********************** ModalBottomSheet **********************//
  @override
  String get modalBottomSheetPhotoLibrary => "Photo Library";
  @override
  String get modalBottomSheetCamera => "Camera";

  // ********************** Add Ad Page **********************//
  @override
  String get adType => "Ad for";
  @override
  String get adPhoto => "Photos";
  @override
  String get adPhotoValidation => "Please add photos";
  @override
  String get adCatogory => "Ad type";
  @override
  String get adSubcategory => "Subcategory";
  @override
  String get adCity => "City";
  @override
  String get adZone => "Zone";
  @override
  String get adFurnishing => "Furnishing";
  @override
  String get adVideo => "Video";
  @override
  String get adSwimming => "Swimming pool";
  @override
  String get adWithSwimming => "With Swimming pool";
  @override
  String get adWithoutSwimming => "Without Swimming pool";
  @override
  String get adRoomNumber => "The number of rooms";
  @override
  String get adBathRoomNumber => "The number of bathrooms";
  @override
  String get adSpace => "Space (meter)";
  @override
  String get adPrice => "Price";
  @override
  String get adLocation => "Location";
  @override
  String get adOnLocation => "Select on the map";
  @override
  String get adOnLocationValidation => "Please select on the map";
  @override
  String get adAddress => "Address";
  @override
  String get adDescription => "Description";
  @override
  String get adAction => "Add Ad";

  // ********************** Real State Page **********************//
  @override
  String get employeeAccomodation => "workers Accomodation";
  @override
  String get apartments => "Apartments";
  @override
  String get villa => "Villa";
  @override
  String get depot => "Depot";

  // ********************** Favorite Page **********************//
  @override
  String get noFavorite => "No ads in favorites";

  // ********************** Chat Page **********************//
  @override
  String get noChats => "No Messages";

  // ********************** Redirect to Auth Page **********************//
  @override
  String get redirectToAuthMessage => "You Should sign In to continue";

  // ********************** Notifications Page **********************//
  @override
  String get emptyNotifications => "No notifications for you so far";

  // ********************** Enterprises Page **********************//
  String get searchForAnAgency => "Search for an agency";
}
